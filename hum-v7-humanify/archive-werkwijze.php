<?php
/**
 * The template for archive page - werkwijze
 *
 * @package hum-v7-humanify
 */

get_header();
?>

<div class="wrap-main">

	<section id="primary" class="content-area">

		<main id="main" class="site-main">

			<?php
			get_template_part( 'template-parts/pages/page/header', 'page__archive');
			?>

			<div class="page-content">

				<?php
				if ( have_rows( 'archive_werkwijze_grp', 'option' ) ) {
				  while ( have_rows( 'archive_werkwijze_grp', 'option') ) {
						the_row();
						include(locate_template( 'template-parts/acf/rows/row--text-text-wysi.php' ));
					}
				}
				?>

				<?php
				get_template_part( 'template-parts/acf/rows/row--archive', 'werkwijze');
				?>

			</div>


		</main>

	</section>

</div>
<?php

get_footer();
