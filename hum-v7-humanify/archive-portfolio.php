<?php
/**
 * The template for displaying post archive page for portfolio
 *
 * @package hum-v7-humanify
 */

get_header();
$archive_title = get_field( 'archive_title_portfolio', 'option' );
?>

<div class="wrap-main">

	<section id="primary" class="content-area">

		<main id="main" class="site-main">

      <?php
			get_template_part( 'template-parts/pages/page/header', 'page__archive');

      if ( have_posts() ) {

				?>
				<div class="page-content">

					<section class="row row--portfolio style-1">

						<div class="wrap">

							<div class="grid--previews grid--50 grid--portfolio">

								<?php
								while ( have_posts() ) {

									the_post();
									include( locate_template( 'template-parts/singles/portfolio/preview-portfolio.php' ));

								}
								wp_reset_postdata();
								?>

							</div>

						</div>

					</section>

				</div>
        <?php
				hum_archive_page_nav();

      }
			?>

		</main>

	</section>

	<?php
	get_template_part( 'template-parts/singles/portfolio/query', 'portfolio__mini' );
	?>

</div>
<?php

get_footer();
