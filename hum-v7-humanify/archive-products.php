<?php
/**
 * The template for post archive page for products
 *
 * @package hum-v7-humanify
 */

get_header();
?>

<div class="wrap-main">

	<section id="primary" class="content-area">

		<main id="main" class="site-main">

			<?php
			get_template_part( 'template-parts/pages/page/header', 'page__archive');
			?>

			<div class="page-content">

        <section class="row row--previews row--products">

					<div class="block-body wrap">

						<?php
						get_template_part( 'template-parts/singles/products/query-links', 'products__per-cat');
						?>

					</div>

				</section>

			</div>


		</main>

	</section>

</div>
<?php

get_footer();
