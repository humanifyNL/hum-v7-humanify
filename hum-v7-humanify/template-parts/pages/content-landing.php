<?php
/**
 * Landing content
 *
 * @package hum-v7-core
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/pages/page/header', 'page__landing' );
	?>

	<div class="post-content">

		<?php
		if ( get_field('enable_page_parent') ) {

   		// is parent
			get_template_part( 'template-parts/acf/rows/row', '-index' );
			get_template_part( 'template-parts/acf/rows/row', '-page--index' );

		} else {
			// is child
			get_template_part( 'template-parts/acf/rows/row', '-page--side' );

		}

		get_template_part( 'template-parts/acf/flex', 'landing' );
		?>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
