<?php
/**
 * Contact content
 *
 * @package hum-v7-humanify
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <?php
  // page header
  get_template_part( 'template-parts/pages/page/header', 'page__landing' );
  ?>

  <div class="page-content">

    <?php
    if ( have_rows( 'contact_row' ) ) {

      while ( have_rows( 'contact_row' ) ) {

        the_row();

        ?>
        <section class="row row--contact">

          <div class="block-body wrap">

            <div class="grid grid--60">

              <div class="block block--left block--contact section-body">

                <?php
                include( locate_template( 'template-parts/acf/partials/text__wysi.php') );
                ?>

              </div>

              <div class="block block--right block--contact">

                <div class="block__frame">

                  <?php
                  include( locate_template( 'template-parts/pages/contact/contact-list-buttons.php' ));
                  include( locate_template( 'template-parts/pages/contact/contact-list-location.php' ));
                  include( locate_template( 'template-parts/pages/contact/contact-list-company.php' ));
                  ?>

                </div>

              </div>

            </div>

          </div>

        </section>
        <?php
      }
    }


    if ( have_rows( 'contact_form_grp' ) ) {
      while ( have_rows( 'contact_form_grp' ) ) {

        the_row();

        get_template_part( 'template-parts/acf/rows/row', '-form' );
      }
    }
    ?>

    <section class="row row--contact map">

    <div class="block block--map">

    <?php
    include( locate_template( 'template-parts/acf/partials/map.php' ) );
    ?>

    </div>
    
    </section>

    <?php
    get_template_part( 'template-parts/pages/page/footer', 'page-about' );
    ?>

  </div>

</article><!-- #post-<?php the_ID(); ?> -->
