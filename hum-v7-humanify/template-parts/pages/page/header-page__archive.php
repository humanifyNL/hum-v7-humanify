<?php
/**
 * Template part for page header of archive pages
 *
 * @package hum-v7-humanify
 */
?>

<header class="page-header page-header--archive style-1">

  <div class="wrap">

    <div class="grid grid--60">

      <div class="block block--title">

        <?php
        $learn_title = 'Alle artikelen';

        // title
        if ( is_post_type_archive( 'post_learn' ) ) {

          echo '<h1 class="page-title">'.$learn_title.'</h1>';

        } elseif( is_post_type_archive( 'portfolio' ) ) {

          echo '<h1 class="page-title">'; post_type_archive_title(); echo '</h1>';
          echo '<div class="block__text block__text--intro"><p class="undertext">Een selectie recente projecten</p></div>';

        } else {

          echo '<h1 class="page-title">'; post_type_archive_title(); echo '</h1>';

        }

        // descr
        if( !empty($term_descr) ) {

          echo 'div class="block__text">'.$term_descr;

        }
        ?>

      </div>

      <div class="block block--meta"></div>

    </div>

  </div>

</header>
