<?php
/**
 * Template part used for displaying page sibling links.
 *
 * @package hum-v7-humanify
 */

$parent_id = get_field( 'page_landing_main', 'option' );

if ( $post->post_parent == $parent_id ) {
	// is child of services

	$child_pages = get_pages( array(

		'child_of' => $post->post_parent,
		'exclude' => array( get_the_id() ),
		'sort_column' => 'menu_order',

	) );

	$child_pages[] = get_post($parent_id);


} elseif ( $post->post_parent ) {
	// is child but not of services

	$child_pages = get_pages( array(

		'child_of' => $parent_id,
		'exclude' => array( get_the_id() ),
		'sort_column' => 'menu_order',

	) );

	//array_unshift( $child_pages, get_post($parent_id) );
	$child_pages[] = get_post($parent_id);


} else {
	// is parent

	$child_pages = get_pages( array(

		'parent'    => $parent_id,
		'sort_column' => 'menu_order',

	) );
}



if ( !empty($child_pages) ) {

	$block_title = get_field( 'services_title', 'option' );
	if ( $block_title ) { echo '<h3 class="block__title">'.$block_title.'</h3>'; }


	?><!-- query-links-page-siblings -->
	<ul class="block__list list--links">

		<?php
	  foreach ($child_pages as $page) {

		  setup_postdata( $page );
			$page_link = get_page_link( $page->ID );
			$page_title = get_field( 'post_title_short' , $page->ID );
			if ( !$page_title ) {
				$page_title = $page->post_title;
			}

			echo '<li>';

				echo '<a class="block__link block__link--icon" href="'.$page_link.'">'.$page_title.'</a>';

	    echo '</li>';

  	}
		wp_reset_postdata();
		?>

	</ul>
	<?php
}
