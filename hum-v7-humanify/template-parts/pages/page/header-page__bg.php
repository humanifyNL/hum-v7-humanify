<?php
/**
 * Template part for post-header with text on background image
 *
 * @package hum-v7-core
 */

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'featured' );
?>

<header class="page-header page-header--bg" <?php if( $thumb ) {?> style="background-position: center; background-size: cover; background-image: url(<?php echo $thumb[0]; ?>);" <?php }?>>

  <?php
  if ( have_rows( 'page_intro_group' ) ) {
    while ( have_rows( 'page_intro_group' ) ) {

      the_row();
      get_template_part( 'template-parts/acf/rows/row--text-wysi' );

    }
  }
  // breadcrumbs
  // get_template_part( 'template-parts/site/yoast', 'breadcrumbs' );

  ?>

</header>
