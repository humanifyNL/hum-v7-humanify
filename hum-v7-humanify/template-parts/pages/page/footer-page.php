<?php
/**
 * Template part for page-footer
 *
 * @package hum-v7-humanify
 */
?>

<section class="row page-footer style-1">

  <div class="wrap">

    <div class="grid grid--50">

      <div class="block block--archive">

        <?php
        echo '<h3 class="block__title">Meer over Humanify</h3>';

        get_template_part( 'template-parts/pages/page/query-links', 'page-siblings' );
        ?>

      </div>

      <div class="block block--archive">

        <?php
        get_template_part( 'template-parts/pages/page/query-links', 'page-siblings-service');
        ?>

      </div>


    </div>

  </div>

</section>
