<?php
/**
 * Template part for header of landing template
 *
 * @package hum-v7-humanify
 */
?>

<header class="page-header page-header--landing style-1">

  <div class="wrap block-body">

    <div class="grid grid--60">

      <div class="block block--title">

        <?php
        // title
        hum_post_type();

        the_title( '<h1 class="page-title">', '</h1>' );
        echo '<link href="'; the_permalink(); echo'"/>';

        $page_subtitle = get_field( 'page_subtitle' );
        if ( !empty($page_subtitle) ) {

          echo '<h3 class="page-subtitle">'.$page_subtitle.'</h3>';
        }

        if ( have_rows( 'page_intro_group' ) ) {
          while ( have_rows( 'page_intro_group' ) ) {

            the_row();
            include( locate_template( 'template-parts/acf/partials/text.php') );
            include( locate_template( 'template-parts/acf/partials/link__repeater.php') );

          }
        } else {
          echo '<div class="block__text">';
            the_content();
          echo '</div>';
        }
        ?>

      </div>

      <?php
      //get_template_part( 'template-parts/site/blocks/block', 'humanify' );
      ?>
      <div class="block"></div>

    </div>

  </div>

</header>
