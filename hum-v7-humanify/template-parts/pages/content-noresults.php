<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @package hum-v7-core
 */
?>

<section class="row no-results not-found">

	<div class="page-content wrap">

		<h2 class="row__title"><?php esc_html_e( 'Niks gevonden', 'hum-base' ); ?></h1>
		<?php

		if ( is_search() ) {

			echo '<p>'; esc_html_e( 'Probeer eens een andere zoekterm', 'hum-base' ); echo '</p>';

		}	else {

			echo '<p>'; esc_html_e( 'Niks gevonden, probeer te zoeken', 'hum-base' ); echo '</p>';
		}
		?>

	</div>

</section><!-- .no-results -->
