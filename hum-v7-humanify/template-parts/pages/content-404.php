<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package hum-v7-humanify
 */
?>

<section class="error-404 not-found">

  <?php
	get_template_part( 'template-parts/pages/404/header', '404' );
	?>

  <div class="page-content">

    <section class="row">

      <div class="wrap">

        <div class="grid grid--50 block-body">

          <div class="block block--left">

            <h3 class="block__title"><?php esc_html_e( 'Categorieën', 'hum-base' ); ?></h3>

            <?php
            $args_404_c = array(
              'taxonomy' => 'category',
              'post_type'=> array( 'post'),
              'orderby' => 'name',
              'order' => 'ASC',
            );
            $cats_404 = get_terms( $args_404_c );

            echo '<ul class="block__list">';

              foreach ( $cats_404 as $cat ) {
                $cat_link = get_tag_link( $cat->term_id );

                echo '<li><a href="'.$cat_link.'" class="'.$cat->slug.' tag" rel="nofollow">'.$cat->name.'<span class="tag__amount"> &#40;'.$cat->count.'&#41;</span></a></li>';
              }

            echo '</ul>';
            ?>

          </div>

          <div class="block block--right">

            <h3 class="block__title"><?php esc_html_e( 'Tags', 'hum-base' ); ?></h3>

            <?php
            $args_404_t = array(
              'taxonomy' => 'post_tag',
              'post_type'=> array( 'post'),
              'orderby' => 'name',
              'order' => 'ASC',
            );
            $tags_404 = get_terms( $args_404_t );

            echo '<ul class="block__list">';

              foreach ( $tags_404 as $tag ) {
                $tag_link = get_tag_link( $tag->term_id );

                echo '<li><a href="'.$tag_link.'" class="'.$tag->slug.' tag" rel="nofollow">'.$tag->name.'<span class="tag__amount"> &#40;'.$tag->count.'&#41;</span></a></li>';
              }

            echo '</ul>';
            ?>

          </div>

        </div>

      </div>

    </section>

    <?php
    get_template_part( 'template-parts/singles/products/footer', 'products' );
    ?>

  </div>

</section><!-- .error-404 -->
