<?php
/**
 * Query page (landing) siblings
 *
 * @package hum-v7-core
 */

$main_id = get_the_id();

$child_pages = get_pages(
	array(
		'child_of' => $post->post_parent,
		'exclude' => array( $main_id ),
		'sort_column' => 'menu_order',
	)
);


if ( !empty($child_pages) ) {

	?>
	<div class="grid--previews <?php echo hum_grid_preview();?>">

		<?php
	  foreach ($child_pages as $page) {

		  setup_postdata( $page );

			if ( $preview_type ) {

        include( locate_template( 'template-parts/pages/page/preview-page__icon.php' ) );

      } else {

        include( locate_template( 'template-parts/pages/page/preview-page.php' ) );
      }

  	}
		wp_reset_postdata();
		?>

	</div>
	<?php
}
