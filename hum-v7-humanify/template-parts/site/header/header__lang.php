<?php
/**
 * Header language menu
 *
 * @package hum-v7-core
 */

$enable_lang = get_field( 'enable_language_select', 'option' );

if ( $enable_lang ) {

  echo '<div class="header__links">';

    echo '<ul class="list--hor list--right lang">';

      $url_nl = get_site_url();
      $url_en = get_site_url() . '/en';
      $url_de = get_site_url() . '/de';

      echo '<li class="lang-nl"><a href="'.$url_nl.'">NL</a></li>';
      echo '<li class="lang-en"><a href="'.$url_en.'">EN</a></li>';
      echo '<li class="lang-de"><a href="'.$url_de.'">DE</a></li>';

    echo '</ul>';

  echo '</div>';
}
