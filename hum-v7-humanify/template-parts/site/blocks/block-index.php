<?php
/**
  * Block for post content index
  * needs /js/post-index.js
  *
  * @package hum-v7-humanify
  */

if ( wp_script_is( 'hum_post-index' ) ) {

  $index_title = get_field( 'post_index_title' );
  ?>
  <div class="block block--index block--index--top js-index">

    <?php
    if ( $index_title ) {
      echo '<h3 class="block__title">'.$index_title.'</h3>';
    }
    ?>

    <ul class="block__list list--links">
    </ul>

  </div>
  <?php
}
?>
