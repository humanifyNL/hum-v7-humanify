<?php
/**
  * Block for category
  * needs a term object ($cat_focus)
  *
  * @package hum-v7-humanify
  */
?>

<div class="clickable preview preview--post has-label">

  <?php
  $focus_id = $cat_focus->term_id;
  $focus_title = $cat_focus->name;
  $focus_slug = $cat_focus->slug;
  $focus_subtitle = get_field( 'cat_subtitle', $cat_focus);
  $focus_intro = get_field( 'cat_intro_text', $cat_focus);
  $focus_url = get_term_link( $cat_focus );
  $focus_url_title = 'Over '.$focus_slug;


  // title
  echo '<h3 class="block__title block__title--icon pad">'.$focus_title.'</h3>';

  // text
  echo '<div class="block__text pad">'.$focus_intro.'</div>';

  // link
  echo '<div class="block__footer label">';
  echo '<a href="'.$focus_url.'" class="click block__link block__link--label">Bekijk services</a>';
  echo '</div>';
  ?>


</div>
