<?php
/**
  * Block for post content index
  * needs /js/post-index.js
  *
  * @package hum-v7-humanify
  */

if ( wp_script_is( 'hum_post-index' ) ) {
  ?>
  <div class="block block--index block--side js-index">

    <div class="block__sticky">

      <div class="block__frame">

        <ul class="block__list list--links">
        </ul>

      </div>

    </div>

  </div>
  <?php
}
?>
