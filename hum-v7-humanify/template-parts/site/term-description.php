<?php
/**
 * Template part for acf term description of taxonomy page
 *
 *
 * @package hum-v7-humanify
 */

$term_obj = get_queried_object();
$term_descr_2 = get_field( 'cat_more_text', $term_obj );
$term_img = get_field( 'cat_more_img' , $term_obj );
$term_title = get_field( 'cat_more_title' , $term_obj );
$term_link_objects = get_field( 'cat_more_link' , $term_obj );

if ( !empty($term_descr_2) ) {

  ?>
  <section class="row row--section row--term style-1">

    <div class="section-body wrap">

      <div class="grid grid--60">

        <div class="block block--text">

          <?php
          if ( $term_title ) {
            echo '<h2 class="row__title">'.$term_title.'</h2>';
          }

          echo '<div class="block__text">'.$term_descr_2.'</div>';

          if ( $term_link_objects ) {

            echo '<div class="button-group button-group--vert fill">';

            foreach ( $term_link_objects as $term_link_obj ) {

              $t_link = get_post_permalink($term_link_obj);
              $t_link_t = $term_link_obj->post_title;
              $t_link_title = '<span class="button-label">Aan de slag </span>'.$t_link_t;

              echo '<a href="'.$t_link.'" class="btn button--w">'.$t_link_title.'</a>';
            }

            echo '</div>';

          }

          ?>

        </div>

        <?php
        if ( !empty($term_img) ) {

          echo '<div class="block block--image has-img">';

            // vars
            $url = $term_img['url'];
            $img_id = $term_img['id'];
            $caption = $term_img['caption'];
            $size = 'medium';

            echo '<a class="block__img">';
            echo wp_get_attachment_image( $img_id, $size, '', array( 'class' => 'section-img') );
              if ( $caption ) {
                echo '<div class="wp-caption">';
                echo '<p class="wp-caption-text">'.$caption.'</p>';
                echo '</div>';
              }
            echo '</a>';

          echo '</div>';

        }
        ?>

      </div>

    </div>

  </section>
  <?php

}
