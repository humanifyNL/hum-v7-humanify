<?php
/**
 * Flex footer module
 *
 * @package hum-v7-core
 */

if ( have_rows( 'contact_location_rep', 'option' ) ) {
  while ( have_rows( 'contact_location_rep', 'option' ) ) {

    the_row();

    $loc_name = get_sub_field ( 'location_name', 'option' );
    $loc_street = get_sub_field ( 'location_street', 'option' );
    $loc_area = get_sub_field ( 'location_area', 'option' );
    $loc_country = get_sub_field ( 'location_country', 'option' );
  }
}

if ( have_rows( 'contact_group', 'option' ) ) {
  while ( have_rows( 'contact_group', 'option' ) ) {

    the_row();

    $email = get_sub_field ( 'contact_email', 'option' );
    $phone = get_sub_field ( 'contact_phone', 'option' );
    $phone_title = get_sub_field ( 'contact_phone_txt', 'option' );
    $linkedin = get_sub_field ( 'contact_linkedin', 'option' );
    $linkedin_title = get_sub_field ( 'contact_linkedin_txt', 'option' );
    $facebook = get_sub_field ( 'contact_facebook', 'option' );
    $facebook_title = get_sub_field ( 'contact_facebook_txt', 'option' );
  }
}

if ( have_rows( 'footer_group_flex', 'option' )) {

  ?>
  <div class="footer footer--flex">

    <div class="wrap wrap--footer">

      <div class="grid grid--footer">

      <?php
      while ( have_rows( 'footer_group_flex', 'option'  )) {

        the_row();

        if ( get_row_layout() == 'footer_list' ) {

          $title = get_sub_field( 'footer_title' );
          $text = get_sub_field( 'footer_text' );

          echo '<div class="block block--footer text">';

            if( $title ) { echo '<h4 class="block__title foot">'.$title.'</h4>'; }
            if( $text ) { echo '<div class="block__text foot">'.$text.'</div>'; }

          echo '</div>';


        } elseif ( get_row_layout() == 'footer_logo' ) {

          $enable_logo = get_sub_field( 'enable_footer_sitelogo' );
          $logo_address = get_sub_field( 'footer_logo_add_address' );

          echo '<div class="block block--footer logo">';

            $custom_img = get_sub_field( 'footer_logo_custom' );

            if ( $enable_logo ) {

              the_custom_logo();

            } else {

              $url_home = get_site_url();
              echo '<a href="'.$url_home.'" title="Back to home">';
                echo wp_get_attachment_image( $custom_img, 'small' );
              echo '</a>';
            }

            if ( $logo_address ) {

              if ( have_rows( 'contact_location_rep', 'option' ) ) {

                echo '<ul class="block__list">';
                  if ( $loc_street ) { echo '<li class="location__street">'.$loc_street.'</li>'; }
                  if ( $loc_area ) { echo '<li class="location__area">'.$loc_area.'</li>'; }
                  if ( $loc_country ) { echo '<li class="location__country">'.$loc_country.'</li>'; }
                echo '</ul>';
              }
            }

          echo '</div>';


        } elseif ( get_row_layout() == 'footer_address' ) {

            $loc_title = get_sub_field ( 'location_title' );

            echo '<div class="block block--footer address">';

              if ( $loc_title ) { echo '<h4 class="block__title">'.$loc_title.'</h4>'; }

              if ( have_rows( 'contact_location_rep', 'option' ) ) {

                echo '<ul class="block__list">';
                  if ( $loc_name ) { echo '<li class="location__name">'.$loc_name.'</li>'; }
                  if ( $loc_street ) { echo '<li class="location__street">'.$loc_street.'</li>'; }
                  if ( $loc_area ) { echo '<li class="location__area">'.$loc_area.'</li>'; }
                  if ( $loc_country ) { echo '<li class="location__area">'.$loc_country.'</li>'; }
                echo '</ul>';
              }

            echo '</div>';


        } elseif ( get_row_layout() == 'footer_contact' ) {

            $contact_title = get_sub_field( 'contact_title' );
            $footer_links = get_sub_field( 'footer_contact_check' );

            echo '<div class="block block--footer contact">';

              if ( $contact_title ) { echo '<h4 class="block__title">'.$contact_title.'</h4>'; }

              echo '<ul class="block__list list--icon">';
              if ( $footer_links && in_array( 'email', $footer_links ) ) {

                if ( !empty($email) ) { echo '<li><a class="email" href="mailto:'.$email.'" rel="nofollow" target="_blank">'.$email.'</a></li>'; }
              }
              if ( $footer_links && in_array( 'phone', $footer_links ) ) {

                if ( !empty($phone) ) { echo '<li><a class="phone" href="tel:'.$phone.'" rel="nofollow" target="_blank">'.$phone_title.'</a></li>'; }
              }
              if ( $footer_links && in_array( 'linkedin', $footer_links ) ) {

                if ( !empty($linkedin) ) { echo '<li><a class="linkedin" href="'.$linkedin.'" rel="nofollow" target="_blank">'.$linkedin_title.'</a></li>'; }
              }
              if ( $footer_links && in_array( 'facebook', $footer_links ) ) {

                if ( !empty($facebook) ) { echo '<li><a class="facebook" href="'.$facebook.'" rel="nofollow" target="_blank">'.$facebook_title.'</a></li>'; }
              }
              echo '</ul>';

            echo '</div>';

        } elseif ( get_row_layout() == 'footer_links' ) {

          $footer_links_title = get_sub_field( 'footer_links_title' );

          if ( have_rows( 'footer_links_repeater', 'option' ) ) {

            echo '<div class="block block--footer links">';

              if ( $footer_links_title ) { echo '<h4 class="block__title">'.$footer_links_title.'</h4>'; }

              echo '<ul class="block__list list--icon">';

                while ( have_rows( 'footer_links_repeater', 'option' ) ) {

                  the_row();
                  $footer_link = get_sub_field( 'footer_links_url', 'option' );
                  $link_url = $footer_link['url'];
                  $link_title = $footer_link['title'];

                    if ( $footer_link ) {
                      echo '<li><a class="pagelink" href="'.$link_url.'" rel="nofollow">'.$link_title.'</a></li>';
                    }
                }

              echo '</ul>';

            echo '</div>';
          }

        }
      }
      ?>

    </div>

    </div>

  </div>
  <?php

}
