<?php
/**
 * Post content WP tinyMCE - more
 *
 * @package hum-v7-core
 */

if ( !empty( get_the_content() ) ) {

  ?>
  <section class="row row--content">

    <div class="post-body wrap">

      <?php
      hum_post_more();
      hum_page_nav();
      ?>

    </div>

  </section>
  <?php
}
