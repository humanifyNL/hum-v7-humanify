<?php
/**
 * Main menu
 *
 * @package hum-v7-humanify
 */
?>

<nav id="site-navigation-left" class="nav--main nav--main--left" aria-label="<?php esc_attr_e( 'Primary Menu', 'hum-base' ); ?>" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">

  <?php
  wp_nav_menu( array(
    'theme_location' => 'secondary',
    'menu_id'        => 'secondary-menu',
    'menu_class'     => 'menu menu--main menu--main--left',
    'depth'          => 2,
    'container_class'=> 'wrap-menu wrap-menu--left',
  ) );
  ?>

</nav>
