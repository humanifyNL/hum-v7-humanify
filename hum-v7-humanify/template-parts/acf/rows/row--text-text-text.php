<?php
/**
 * section with 3 text blocks
 *
 * ACF field: group_60019a19b6ea1
 *
 * @package hum-v7-core
 */

$center_align = get_sub_field( 'layout_text_center' );
?>

<section class="row row--section text_text_text <?php echo hum_row_style();?>" <?php hum_row_img(); ?>>

  <div class="block-body wrap">

    <div class="grid--previews grid--33">

      <?php
      if ( have_rows( 'text_group_1' ) ) {
        while ( have_rows( 'text_group_1' ) ) {

          the_row();
          $link = get_sub_field( 'link_page_post' );

          echo '<div class="block block--text'; if ( $link ) { echo ' preview block--panel has-label clickable'; } if ( $center_align ) { echo ' center'; } echo '">';

            //include( locate_template( 'template-parts/acf/partials/image__icon.php') );
            include( locate_template( 'template-parts/acf/partials/title__icon.php') );
            include( locate_template( 'template-parts/acf/partials/text__pad.php') );
            include( locate_template( 'template-parts/acf/partials/link__label.php') );

          echo '</div>';

        }
      }

      if ( have_rows( 'text_group_2' ) ) {
        while ( have_rows( 'text_group_2' ) ) {

          the_row();
          $link = get_sub_field( 'link_page_post' );

          echo '<div class="block block--text'; if ( $link ) { echo ' preview block--panel has-label clickable'; } if ( $center_align ) { echo ' center'; } echo '">';

            //include( locate_template( 'template-parts/acf/partials/image__icon.php') );
            include( locate_template( 'template-parts/acf/partials/title__icon.php') );
            include( locate_template( 'template-parts/acf/partials/text__pad.php') );
            include( locate_template( 'template-parts/acf/partials/link__label.php') );

          echo '</div>';

        }
      }

      if ( have_rows( 'text_group_3' ) ) {
        while ( have_rows( 'text_group_3' ) ) {

          the_row();
          $link = get_sub_field( 'link_page_post' );

          echo '<div class="block block--text'; if ( $link ) { echo ' preview block--panel has-label clickable'; } if ( $center_align ) { echo ' center'; } echo '">';

            //include( locate_template( 'template-parts/acf/partials/image__icon.php') );
            include( locate_template( 'template-parts/acf/partials/title__icon.php') );
            include( locate_template( 'template-parts/acf/partials/text__pad.php') );
            include( locate_template( 'template-parts/acf/partials/link__label.php') );

          echo '</div>';

        }
      }
      ?>

    </div>

  </div>

</section>
