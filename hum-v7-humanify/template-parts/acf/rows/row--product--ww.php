<?php
/**
  * ACF product werkwijze tekst
  *
  * @package hum-v7-humanify
  */

?>
<section class="row row--product row--product__ww">

  <div class="wrap">

    <div class="grid grid--60">

      <div class="block block--text section-body">

        <?php
        include( locate_template( 'template-parts/acf/partials/title__row.php') );

        include( locate_template( 'template-parts/acf/partials/text__wysi.php') );
        ?>

      </div>

      <?php
      $uitvoering = get_field( 'enable_prod_cost' ); // uitvoering aan = uitvoering product!
      ?>

      <div class="block block--products <?php if( !$uitvoering ){echo 'nomob';}?>">

        <?php
        get_template_part( 'template-parts/singles/products/query-links', 'products__all');
        ?>

      </div>

    </div>

  </div>

</section>
