<?php
/**
  * ACF product werkwijze & kosten
  *
  * @package hum-v7-humanify
  */

$row_title_def = get_field( 'prod_cost_row_title', 'option' );

?>
<section class="row row--product row--product__cost style-1">

  <div class="wrap">

    <?php
    if ( $row_title_def ) {
      echo '<h2 class="row__title">'.$row_title_def.'</h2>';
    }
    ?>

    <div class="grid grid--60">

      <div class="block">

        <?php
        get_template_part( 'template-parts/singles/products/repeater', 'product-cost' );
        ?>

      </div>

      <div class="block block--details">

        <div class="grid grid--details">

          <div class="block block--docs">

            <?php
            get_template_part( 'template-parts/singles/post_docs/query-docs__link' );
            ?>

          </div>

          <div class="block block--price">

            <?php
            echo '<h4 class="block__title">Uurtarief</h4>';

            $hum_hourly = get_field ( 'humanify_hourly_rate', 'option' );
            echo '<p class="block__price">'.$hum_hourly.',-</p>';
            ?>

          </div>


        </div>

      </div>


    </div>

  </div>

</section>
