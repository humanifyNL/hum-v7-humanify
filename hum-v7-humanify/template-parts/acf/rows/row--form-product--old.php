<?php
/**
  * Contact form CF7
  *
  * ACF field:
  *
  * @package hum-base-v6
  */

$form = get_field( 'form_prod', 'option' );
$form_title = get_field( 'form_title_prod', 'option' );
$form_subtitle = get_field( 'form_subtitle_prod', 'option' );
$form_text = get_field( 'form_text_prod', 'option' );

$theme = get_sub_field( 'row_theme' );

if ( !empty($form) ) {

  ?>
  <section class="row row--form form_product <?php if( $theme ) { echo 'style-1 ';}?>">

    <div class="block-body wrap">

      <div class="grid">

        <div class="block block--form">

            <?php
            if ( $form_title) { echo '<h2 class="row__title">'.$form_title.'</h2>'; }
            if ( $form_subtitle) { echo '<h3 class="row__subtitle">'.$form_subtitle.'</h3>'; }
            if ( $form_text ) { echo '<div class="form__text">'.$form_text.'</div>'; }

            echo do_shortcode( $form );
            ?>

        </div>

      </div>

    </div>

  </section>
  <?php
}
?>
