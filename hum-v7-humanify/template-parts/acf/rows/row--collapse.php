<?php
/**
  * Collapsible list row
  *
  * ACF field: group_5f15771f6c489
  *
  * @package hum-v7-core
  */
?>

<section class="row row--section collapse <?php echo hum_row_style(); ?>">

  <div class="wrap section-body">

    <div class="grid">

      <div class="block block--text">

        <?php
        include( locate_template( 'template-parts/acf/partials/text__wysi.php' ) );
        ?>

      </div>

      <div class="block block--collapse">

        <?php
        include( locate_template( 'template-parts/acf/partials/collapse.php') );
        ?>

      </div>

    </div>

  </div>

</section>
