<?php
/**
  * Text (list) & uitvoering
  *
  * @package hum-v7-humanify
  */
?>

<section class="row row--product">

  <div class="wrap">

    <div class="grid">

      <div class="block">

        <?php
        $uitvoering_title = get_sub_field( 'row_title_uitvoering' );
        echo '<h2 class="row__title">Werkwijze</h2>';
        get_template_part( 'template-parts/singles/products/repeater', 'product-cost__home' );
        ?>

      </div>

    </div>

  </div>

</section>
