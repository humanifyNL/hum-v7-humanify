<?php
/**
  * Review row for portfolio
  *
  * ACF field:
  *
  * @package hum-v7-humanify
  */

if ( have_rows ( 'portfolio_reviews' ) ) {
  while ( have_rows ( 'portfolio_reviews' ) ) {

    the_row();
    $row_title = get_sub_field( 'row_title' );

    if ( have_rows ( 'review_repeater' ) ) {

      ?>
      <section class="row row--review">

        <div class="block-body wrap">

          <?php
          if ( $row_title ) {
            echo '<h2 class="row__title">'.$row_title.'</h2>';
          }
          ?>

          <div class="grid grid--50">

            <?php
            while ( have_rows ( 'review_repeater' ) ) {

              the_row();

              $r_review = get_sub_field( 'review_text' );
              $r_user = get_sub_field( 'review_user_name' );
              $r_user_link = get_sub_field( 'review_user_link' );
              $r_user_info = get_sub_field( 'review_user_info' );
              $r_enable_case_link = get_sub_field( 'enable_review_case_link' );
              $r_case_link_text = get_sub_field( 'review_case_link_text' );
              $r_case_link = get_sub_field( 'review_case_link' );

              if ( $r_review ) {

                ?>
                <div class="block block--review">

                  <?php
                  if ( $r_review ) {
                    echo '<div class="block__text">'.$r_review.'</div>';
                  }
                  ?>

                  <div class="block__footer">

                    <div class="review__user-info <?php if ( $r_enable_case_link ) { echo ' case-link'; }?>">

                      <?php
                      // username and optional link
                      if ( $r_user ) {

                        if ( $r_user_link ) {
                        echo '<a class="block__link block__link--review" href="'.$r_user_link.'">';
                        echo $r_user;
                        echo '</a>';
                        }

                        // info
                        if ( $r_user_info ) {
                        echo '<p>'.$r_user_info.'</p>';
                        }
                      }
                      ?>

                     </div>

                     <?php
                     if ( $r_enable_case_link ) {

                       echo '<div class="review__case">';

                         echo '<a href="'.$r_case_link.'" class="btn button--alt">';

                         if ( $r_case_link_text ) { echo $r_case_link_text;
                         } else { echo 'Bekijk case';
                         }

                         echo '</a>';

                       echo '</div>';

                     }
                     ?>

                  </div>

                </div>
                <?php
              }
            }
            ?>

        </div>

      </section>

      <?php
    }

  }
}
