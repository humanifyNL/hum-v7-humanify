<?php
/**
 * Postlinks - double repeater row
 *
 * ACF field:
 *
 * @package hum-base-v6
 */

$bg_img_url = get_sub_field('postlinks_img_url');
?>

<section class="row row--pages postlinks style-1 has-bg" <?php if( $bg_img_url ) { echo 'style="background-position: top; background-size: cover; background-image: url('.$bg_img_url.')"'; }?>>

  <div class="wrap">

    <div class="grid grid--50">

      <?php
      if ( have_rows( 'postlinks_left_grp' ) ) {
        while ( have_rows( 'postlinks_left_grp' ) ) {
          the_row();

          ?>
          <div class="block block--left">

            <?php
            include( locate_template( 'template-parts/acf/partials/title__row.php') );

            include( locate_template( 'template-parts/acf/partials/post__repeater.php' ) );
            ?>

          </div>
          <?php
        }
      }

      if ( have_rows( 'postlinks_right_grp' ) ) {
        while ( have_rows( 'postlinks_right_grp' ) ) {
          the_row();

          ?>
          <div class="block block--right">

            <?php
            include( locate_template( 'template-parts/acf/partials/title__row.php') );

            include( locate_template( 'template-parts/acf/partials/post__repeater.php' ) );
            ?>

          </div>
          <?php
        }
      }
      ?>

    </div>

  </div>

</section>
