<?php
/**
 * Text section
 *
 * ACF field: group_5f08831a932a6
 *
 * @package hum-v7-core
 */

$glue = get_sub_field( 'text_section_connect' );
?>

<section class="row row--section text-text <?php echo hum_row_style(); if( $glue ){ echo ' connect '; }?>">

  <div class="section-body wrap">

    <?php
    include( locate_template( 'template-parts/acf/partials/title__row.php') );
    ?>

    <div class="grid <?php echo hum_grid_section(); ?>">

      <div class="block block--text <?php echo hum_block_style();?>">

        <?php
        include( locate_template( 'template-parts/acf/partials/text__wysi.php') );

        include( locate_template( 'template-parts/acf/partials/link__repeater.php') );
        ?>

      </div>

      <div class="block block--text <?php echo hum_block_style();?>">

        <?php
        include( locate_template( 'template-parts/acf/partials/text__wysi-r.php') );

        include( locate_template( 'template-parts/acf/partials/link__repeater-r.php') );
        ?>

      </div>

    </div>

  </div>

</section>
