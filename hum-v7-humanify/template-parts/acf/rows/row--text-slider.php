<?php
/**
 * Text section with image slider
 *
 * ACF field: group_5f0b0e81748e3
 *
 * @package hum-v7-core
 */

$glue = get_sub_field( 'text_section_connect' );
?>

<section class="row row--section text_slider <?php echo hum_row_style(); if( $glue ){ echo ' connect '; }?>">

  <div class="section-body wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <div class="block block--text">

        <?php
        include( locate_template( 'template-parts/acf/partials/text__wysi.php') );

        include( locate_template( 'template-parts/acf/partials/link__repeater.php') );
        ?>

      </div>

      <div class="block block--slider">

        <?php
        include( locate_template( 'template-parts/acf/partials/slider.php') );
        ?>

      </div>

    </div>

  </div>

</section>
