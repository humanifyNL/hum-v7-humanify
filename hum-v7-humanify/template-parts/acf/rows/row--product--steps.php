<?php
/**
  * ACF product - stappen
  *
  * @package hum-v7-humanify
  */

if ( have_rows( 'steps_repeater') ) {

  ?>
  <section class="row row--product row--product__steps">

    <div class="wrap">

      <?php
      include( locate_template( 'template-parts/acf/partials/title__row.php') );
      include( locate_template( 'template-parts/acf/partials/text__row.php') );

      get_template_part( 'template-parts/singles/products/repeater', 'product-steps' );
      ?>

    </div>

  </section>
  <?php
}
