<?php
/**
 * Portfolio row with client info & review
 *
 * @package hum-v7-humanify
 */

?>
<section class="row row--portfolio__client">

  <div class="wrap">

    <div class="grid grid--60">

      <div class="block block--text section-body">

        <?php

        if ( !empty( get_the_content() ) ) {
          echo '<div class="block__text">';
            the_content();
          echo '</div>';
        }
        ?>

      </div>


      <div class="block block--details">

        <?php
        //get_template_part( 'template-parts/singles/portfolio/portfolio', 'products');
        ?>

      </div>

    </div>

  </div>

</section>
