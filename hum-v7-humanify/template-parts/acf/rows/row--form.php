<?php
/**
  * Contact form
  *
  * ACF field: group_5f1580869f2d2
  *
  * @package hum-v7-core
  */

$form = get_sub_field( 'form_form' );

if ( !empty($form) ) {

  ?>
  <section class="row row--form <?php echo hum_row_style(); ?>">

    <div class="block-body wrap">

      <div class="grid">

        <div class="block block--form">

            <?php
            include( locate_template( 'template-parts/acf/partials/title__row.php' ) );
            include( locate_template( 'template-parts/acf/partials/text__row.php' ) );

            include( locate_template( 'template-parts/acf/partials/form.php' ) );
            ?>

        </div>

      </div>

    </div>

  </section>
  <?php
}
?>
