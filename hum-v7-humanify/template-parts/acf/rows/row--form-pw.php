<?php
/**
  * PW protected form
  *
  * @package hum-v7-core
  */
?>

<section class="row row--form row--form--pw">

  <div class="block-body wrap">

    <div class="grid">

      <div class="block">

          <?php
          echo get_the_password_form( $post );
          ?>

      </div>

    </div>

  </div>

</section>
