<?php
/**
  * ACF product werkwijze & kosten
  *
  * @package hum-v7-humanify
  */

?>
<section class="row row--product row--product__cost style-1">

  <div class="wrap">

    <?php
    include( locate_template( 'template-parts/acf/partials/title__row.php') );
    ?>

    <div class="grid grid--60">

      <div class="block block--text section-body">

        <?php
        include( locate_template( 'template-parts/acf/partials/text__wysi.php') );
        ?>

      </div>

      <div class="block block--details">

        <div class="grid grid--details">

          <div class="block block--price">

            <?php
            echo '<h4 class="block__title">Uurtarief</h4>';

            $hum_hourly = get_field ( 'humanify_hourly_rate', 'option' );
            echo '<p class="block__price">'.$hum_hourly.',-</p>';
            ?>

          </div>

          <div class="block block--docs">

            <?php
            get_template_part( 'template-parts/singles/post_docs/query-docs__link' );
            ?>

          </div>

        </div>

      </div>


    </div>

  </div>

</section>
