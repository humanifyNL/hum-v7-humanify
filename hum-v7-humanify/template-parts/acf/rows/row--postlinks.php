<?php
/**
 * Postlinks - repeater row
 *
 * ACF field: group_5f144e174f531
 *
 * @package hum-v7-core
 */
?>

<section class="row row--section row--posts <?php echo hum_row_style(); ?>">

  <div class="section-body wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <div class="block block--text">

        <?php
        include( locate_template( 'template-parts/acf/partials/text__wysi.php') );
        ?>

      </div>

      <div class="block block--links">

        <?php
        include( locate_template( 'template-parts/acf/partials/postlinks.php' ) );
        ?>

      </div>

    </div>

  </div>

</section>
