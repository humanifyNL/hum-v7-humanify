<?php
/**
 * Text partial
 *
 * @package hum-v7-core
 */

$text = get_sub_field( 'row_text' );

if ( $text ) {

  echo '<div class="row__text">';

    echo $text;

  echo '</div>';

}
