<?php
/**
 * Title-block partial with icon before
 *
 * ACF fields: group_5f11bd1934c8f
 *
 * @package hum-v7-humanify
 */

$block_title = get_sub_field( 'block_title' );

if ( $block_title ) {

  echo '<h3 class="block__title block__title--icon pad">';

    echo $block_title;

  echo '</h3>';
}
