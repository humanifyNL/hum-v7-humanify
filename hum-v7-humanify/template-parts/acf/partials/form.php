<?php
/**
 * Form
 *
 * ACF field:
 *
 * @package hum-v7-core
 */

echo '<div class="block__form">';

  $form = get_sub_field( 'form_form' );
  echo do_shortcode( $form );

echo '</div>';
