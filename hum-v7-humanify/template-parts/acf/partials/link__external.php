<?php
/**
 * Link partial
 *
 * ACF field: group_5f087f17ba886
 *
 * @package hum-v7-core
 */

$link_ext = get_sub_field( 'link_ext_arr' );
$link_type = get_sub_field( 'link_type' );


if ( $link_ext ) {

  echo '<a class="'.$link_type.'"';

  // url
  echo 'href="'. esc_url($linkext['url']).'"';

  // target
  if ( $linkext['target']) {
    echo 'target="'.esc_attr($linkext['target']).'"';
  }

  echo '>';

  // title
  if ( $linkext['title'] ) {

    echo esc_html($linkext['title']) ;

  } else {
    echo "Lees meer";
  }

  echo '</a>';
}
