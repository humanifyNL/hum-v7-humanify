<?php
/**
 * Map partial
 *
 * ACF field: group_5f09c4c20072d
 *
 * @package hum-v7-core
 */

if ( have_rows( 'contact_location_rep', 'option' ) ) {

  echo '<div class="block__map">';

    while ( have_rows( 'contact_location_rep', 'option' ) ) {

      the_row();

      $location = get_sub_field( 'location_map', 'option' );
      $loc_name = get_sub_field ( 'location_name', 'option' );

      echo '<div class="marker" data-lat="'.$location['lat'].'" data-lng="'.$location['lng'].'">';
        echo $loc_name;
      echo '</div>';
    }

  echo '</div>';

}
