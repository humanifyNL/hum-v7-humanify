<?php
/**
 * Slider with swiper.js
 *
 * ACF field: group_5f0b0e81748e3
 *
 * @package hum-v7-core
 */

if ( have_rows( 'swiper_slides' ) ) {

  ?>
  <div class="block__img block__img--slider">

    <div class="swiper-wrapper">

      <?php
      while ( have_rows( 'swiper_slides' ) ) {

        the_row();

        $slide_size = 'large';
        $slide_id = get_sub_field( 'swiper_slide_id' );

        echo '<div class="swiper-slide">';

          echo wp_get_attachment_image( $slide_id, $slide_size );

        echo '</div>';

      }
      ?>

    </div>

  </div>
  <?php
}
