<?php
/**
 * Title-block partial
 *
 * ACF fields: group_5f11bd1934c8f
 *
 * @package hum-v7-core
 */

$block_title = get_sub_field( 'block_title' );

if ( $block_title ) {

  echo '<h3 class="block__title">';

    echo $block_title;

  echo '</h3>';
}
