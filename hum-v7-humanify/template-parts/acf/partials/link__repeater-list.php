<?php
/**
 * Link partial
 *
 * @package hum-v7-core
 */

if ( have_rows( 'link_repeater_list' ) ) {

  ?>
  <ul class="block__list list--links">

  <?php
    while ( have_rows( 'link_repeater_list' ) ) {

      the_row();

      $link = get_sub_field( 'link_page_post' );
      $link_title = get_sub_field( 'link_title' );
      $link_type = get_sub_field( 'link_type' );

      if ( $link_type == 'block__link' ) {
        $link_class = 'block__link block__link--icon';
      } else {
        $link_class = 'btn block__btn '.$link_type;
      }

      echo '<li>';
        echo '<a class="'.$link_class.'" href="'.esc_url($link).'">'.esc_html($link_title).'</a>';
      echo '</li>';

    }
    ?>

  </ul>
  <?php

}
