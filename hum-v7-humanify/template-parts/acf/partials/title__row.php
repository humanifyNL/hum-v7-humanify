<?php
/**
 * Title-row partial
 *
 * ACF field: group_5f11bc851c651
 *
 * @package hum-v7-core
 */

$row_title = get_sub_field( 'row_title' );
$row_subtitle = get_sub_field( 'row_subtitle' );
$enable_sub = get_sub_field( 'enable_subtitle' );
$row_theme = get_sub_field( 'row_title_theme' );

if ( $row_title ) {
  echo '<h2 class="row__title'; if ( $row_theme ) { echo ' row__title--theme'; } echo ' ">'.$row_title.'</h2>';
}
if ( $row_subtitle && $enable_sub ) {
  echo '<h3 class="row__subtitle">'.$row_subtitle.'</h3>';
}
