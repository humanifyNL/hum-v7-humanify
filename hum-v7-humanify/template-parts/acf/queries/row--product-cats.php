<?php
/**
 * Template part for query of product cats (cat_focus)
 *
 * @package hum-v7-humanify
 */

$bg_img_url = get_sub_field( 'home_bg_url' );
$bg_img_url = get_field( 'hum_bg_url', 'option' );
?>

<section class="row row--product-cats" <?php if( $bg_img_url ) { echo 'style="background-position: top; background-size: cover; background-image: url('.$bg_img_url.')"'; }?>>

  <div class="block-body wrap">

    <?php
    include( locate_template( 'template-parts/singles/products/query-product-cats.php' ) );
    ?>

  </div>

</section>
