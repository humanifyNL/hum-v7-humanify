<?php
/**
 * Query row - docs
 *
 * @package hum-v7-humanify
 */
?>

<section class="row row--docs style-1">

  <div class="block-body wrap">

    <?php
    include( locate_template( 'template-parts/singles/post_docs/query-docs.php' ) );
    ?>

  </div>

</section>
