<?php
/**
 * Query row - posts - custom select
 *
 * @package hum-v7-core
 */
?>

<section class="row row--previews row--previews__select <?php echo hum_row_style();?>">

  <div class="wrap block-body">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <div class="block block--image">

        <?php
        // post image
        include( locate_template( 'template-parts/singles/post/query-posts__lastpost--img.php' ) );
        ?>

      </div>

      <div class="block block--previews">

        <?php
        include( locate_template( 'template-parts/acf/partials/title__row.php' ) );

        include( locate_template( 'template-parts/singles/post/query-posts__lastpost.php' ) );
        ?>

      </div>


    </div>

  </div>

</section>
