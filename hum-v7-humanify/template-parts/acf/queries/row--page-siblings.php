<?php
/**
 * Query row - page siblings (related)
 *
 * @package hum-v7-core
 */

?>
<section class="row row--pages siblings">

	<div class="block-body wrap">

		<?php
		$page_sib_title = get_field( 'page_siblings_title', 'option');
		if ( $page_sib_title ) { echo '<h2 class="row__title">'.$page_sib_title.'</h2>'; }

		include( locate_template( 'template-parts/pages/page/query-page-siblings.php' ) );
		?>

	</div>

</section>
