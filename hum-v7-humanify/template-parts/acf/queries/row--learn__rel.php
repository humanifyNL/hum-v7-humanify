<?php
/**
 * Template part for query of post_learn per focus
 *
 * @package hum-v7-humanify
 */
?>

<section class="row row--previews row--previews--rel">

  <div class="block-body wrap">

    <?php
    include( locate_template( 'template-parts/singles/post_learn/query-learn__rel.php' ) );
    ?>

  </div>

</section>
