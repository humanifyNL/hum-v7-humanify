<?php
/**
 * Query row - landing pages
 *
 * @package hum-v7-core
 */

$preview_type = get_field( 'landing_preview_type', 'option' ); // yes is icon, no is block
$landing_title = get_field( 'landing_siblings_title', 'option' );

?>
<section class="row row--pages siblings landing">

	<div class="block-body wrap">

		<?php
		if ( $landing_title ) {
			echo '<h2 class="row__title">'.$landing_title.'</h2>';
		}

		include( locate_template( 'template-parts/pages/landing/query-landing__siblings.php' ) );
		?>

	</div>

</section>
