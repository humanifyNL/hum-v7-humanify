<?php
/**
 * Template part for query of products per focus
 *
 * @package hum-v7-humanify
 */

$bg_img_url = get_field('hum_bg_url', 'options');
?>

<section class="row row--previews row--previews--products" <?php if( $bg_img_url ) { echo 'style="background-position: top; background-size: cover; background-image: url('.$bg_img_url.')"'; }?>>

  <div class="block-body wrap">

    <?php
    include( locate_template( 'template-parts/singles/products/query-products__focus.php' ) );
    ?>

  </div>

</section>
