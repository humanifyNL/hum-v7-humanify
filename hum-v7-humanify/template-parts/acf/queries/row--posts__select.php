<?php
/**
 * Query row - posts - custom select
 *
 * @package hum-v7-core
 */

?>
<section class="row row--previews select">

  <div class="block-body">

  	<?php
		include( locate_template( 'template-parts/singles/post/query-posts__select.php' ) );
		?>

  </div>

</section>
