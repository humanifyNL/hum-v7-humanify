<?php
/**
 * Query row - page children
 *
 * @package hum-v7-core
 */
?>

<section class="row row--pages children">

	<div class="block-body wrap">

		<?php
		include( locate_template( 'template-parts/pages/page/query-page-children.php' ) );
		?>

	</div>

</section>
