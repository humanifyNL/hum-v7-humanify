<?php
/**
 * Template part for displaying Author bio
 *
 * @package hum-v7-core
 */
?>

<div class="block block--author">

	<div class="author__avatar">

		<?php
		/* Filter the author bio avatar size.
		* @param int $size The avatar height and width size in pixels.
		*/
		$author_bio_avatar_size = apply_filters( 'hum_base_author_bio_avatar_size', 48 );
		echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
		?>

	</div>

	<div class="block__body">

		<?php
		if ( is_single() ) {

			echo '<h3 class="block__title">';

				echo get_the_author();

			echo '</h3>';
		}
		?>

		<div class="block__text">

			<?php
			if ( get_the_author_meta( 'description' ) ) {

				echo '<p class="author__bio">';
					the_author_meta( 'description' );
				echo '</p>';
			}
			?>

		</div>

		<?php
		// Display link to author archive only in posts.
		if ( is_single() ) {

			$author_link = get_field( 'author_link', 'option');
			$author_link_text = get_field( 'author_link_text', 'option');

			echo '<div class="block__footer">';

				echo '<a class="author__link" href="'.$author_link.'" rel="author">';
				  if ( $author_link_text ) {
						echo $author_link_text;
					} else {
						echo 'Auteur archief';
					}
				echo '</a>';

			echo '</div>';
		}
		?>

	</div>

</div>
