<?php
/**
 * Template part for query of product cats (cat_focus)
 *
 * @package hum-v7-humanify
 */

$query_per_foc = new WP_Query;

$cats_focus = get_terms( array(
    'taxonomy' => 'cat_focus',
    'hide_empty' => false,
    'meta_key' => 'cat_order',
    'orderby' => 'meta_value_num',
) );

?><!--query-product-cats-->

<div class="grid--previews grid--33">

  <?php
  foreach ( $cats_focus as $cat_focus ) {

    include( locate_template( 'template-parts/site/blocks/block-cat.php' ) );

  }
  ?>

</div>
