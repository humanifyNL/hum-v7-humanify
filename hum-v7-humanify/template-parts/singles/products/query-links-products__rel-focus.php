<?php
/**
 * Template part for query of post_learn posts
 *
 * @package hum-v7-humanify
 */

$focus_terms = get_the_terms( get_the_id(), 'cat_focus' );

if ( !empty( $focus_terms ) ) {
  $focus_ids = wp_list_pluck( $focus_terms, 'term_id' );
  $focus_names = wp_list_pluck( $focus_terms, 'name' );
  $n_focus = count($focus_ids);
}
?>

<!--query__links-products__rel-focus-->
<?php
if ( $focus_ids ) {

  $args = array(
    'post_type' => array( 'products' ),
    'meta_key' => 'acf_post_order',
    'orderby' => 'meta_value_num',
    'order' => 'DESC',
    'tag__not_in' => array( 95 ),
    'tax_query' => array(
      'relation' => 'AND',
      array(
        'taxonomy' => 'cat_focus',
        'field' => 'id',
        'terms' => $focus_ids,
        'operator' => 'IN'
      ),
    ),
  );

  $query_prods = new WP_query ( $args );

  if ( $query_prods->have_posts() ) {

    echo '<ul class="block__list list--links">';

      while ( $query_prods->have_posts() ) {

        $query_prods->the_post();
        $prod_title = the_title( '', '', false );
        $prod_url = get_permalink();

        echo '<li>';

          echo '<a class="block__link block__link--icon product" href="'.$prod_url.'">'.$prod_title.'</a>';

        echo '</li>';

      }

      wp_reset_postdata();

    echo '</ul>';
  }
}
?>
