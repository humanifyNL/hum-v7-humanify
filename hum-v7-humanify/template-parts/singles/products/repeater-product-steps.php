<?php
/**
 * Repeater for product steps
 *
 * @package hum-v7-humanify
 */
?>

<div class="grid grid--steps">

  <?php
  if ( have_rows( 'steps_repeater') ) {

    while ( have_rows( 'steps_repeater' ) ) {

      the_row();

      $step_title = get_sub_field( 'step_title' );
      $step_text = get_sub_field( 'step_descr' );
      $step_time = get_sub_field( 'step_hours' );

      echo '<div class="block block--step">';

        echo '<h4 class="block__title">'.$step_title.'</h4>';

        echo '<div class="block__frame">';
          echo '<div class="block__time nomob">'.$step_time.'&#37;</div>';
          echo '<div class="block__time yesmob">&#40;'.$step_time.'&#37;&#41;</div>';
          echo '<div class="block__text">'.$step_text.'</div>';
        echo '</div>';

      echo '</div>';

    }
  }
  ?>

</div>
