<?php
/**
 * Query focus archive links
 *
 * @package hum-v7-humanify
 */


$query_per_foc = new WP_Query;

$focus_cats = get_terms( array(
    'taxonomy' => 'cat_focus',
    'hide_empty' => false,
    'meta_key' => 'cat_order',
    'orderby' => 'meta_value_num',
) );

?><!--query__links__products__cat_focus-->

<ul class="block__list list--links">

  <?php
  foreach ( $focus_cats as $focus_cat ) {

    $focus_id = $focus_cat->term_id;
    $focus_title = get_field( 'cat_subtitle', $focus_cat);
    $focus_url = get_term_link( $focus_cat );
    $focus_url_title = 'Naar deze service';

    echo '<li>';

      echo '<a class="block__link block__link--icon product" href="'.$focus_url.'">'.$focus_title.'</a>';

    echo '</li>';
  }
  ?>

</ul>
