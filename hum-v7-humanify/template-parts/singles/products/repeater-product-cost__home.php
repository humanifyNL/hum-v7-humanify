<?php
/**
 * Repeater for product cost options (options)
 *
 * @package hum-v7-humanify
 */
?>

<div class="grid--previews grid--50">

  <?php
  if ( have_rows( 'prod_cost_repeater' , 'option') ) {

    while ( have_rows( 'prod_cost_repeater' , 'option') ) {

      the_row();

      $block_title = get_sub_field( 'prod_cost_title' ,'option');
      $block_text = get_sub_field( 'prod_cost_text', 'option' );
      $block_url = get_sub_field( 'prod_cost_link', 'option' );
      $block_url_title = get_sub_field( 'prod_cost_linktitle', 'option' );


      echo '<div class="clickable preview preview--post has-label">';

        echo '<h3 class="block__title block__title--icon">'.$block_title.'</h3>';

        echo '<div class="block__text is-excerpt pad">'.$block_text.'</div>';

        echo '<div class="block__footer label">';
          echo '<a href="'.$block_url.'" class="click block__link--label">'.$block_url_title.'</a>';
        echo '</div>';

      echo '</div>';

    }
  }
  ?>

</div>
