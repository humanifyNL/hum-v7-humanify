<?php
/**
 * Template part for header - post
 *
 * @package hum-v7-humanify
 */
?>

<header class="page-header post-header post-header--products style-1">

  <div class="wrap">

    <div class="grid grid--60">

      <div class="block block--title">

        <?php
        // title

        the_title( '<h1 class="page-title">', '</h1>' );
        echo '<link href="'; the_permalink(); echo'"/>';

        hum_cats_cp_rollout('category');

        echo '<div class="block__text">';
          the_content();
        echo '</div>';
        ?>

      </div>

      <div class="block block--meta">

        <?php

        echo '<div class="block__meta block__meta--tax">';

          hum_cats_cp_rollout_r('cat_focus');

        echo '</div>';

        echo '<div class="block__meta block__meta--pub">';

          hum_meta_published();

        echo '</div>';


        if ( has_tag( 'uitvoering' ) ) {

          $intro_link_object = get_field( 'prod_intro_link' );

          if ( $intro_link_object ) {

            $post = $intro_link_object;
            setup_postdata($post);
            $i_title = the_title( '', '', false );
            $i_link = get_permalink();

            wp_reset_postdata();

            echo '<div class="button-group right">';

            echo '<a class="btn button--w" href="'.$i_link.'"><span class="button-label">'.$i_title.'</span></a>';

            echo '</div>';
          }

        } else {

          echo '<div class="block__meta block__meta--tax">';

            hum_meta_cp();

          echo '</div>';

        }
        ?>

      </div>

    </div>

  </div>

</header>
