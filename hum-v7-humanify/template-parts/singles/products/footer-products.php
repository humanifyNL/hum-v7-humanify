<?php
/**
 * Template part for page-footer with product categories
 *
 * @package hum-v7-humanify
 */
?>

<section class="row page-footer page-footer--products style-1">

  <div class="wrap">

    <?php
    get_template_part( 'template-parts/singles/products/query-links', 'products__per-focus');
    ?>

  </div>

</section>
