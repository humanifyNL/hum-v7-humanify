<?php
/**
 * Template part for query of product links sorted by cat_focus
 *
 * @package hum-v7-humanify
 */
?>
<!--query__links__products__all-->

<?php
$current_postid = get_the_id();

$query_per_foc = new WP_Query;
$focus_cats = get_terms( array(
  'taxonomy' => 'cat_focus',
  'hide_empty' => true,
  'meta_key' => 'cat_order',
  'orderby' => 'meta_value_num',
));

echo '<h2 class="row__title">Diensten</h2>';

foreach ( $focus_cats as $focus_cat ) {

  $focus_id = $focus_cat->term_id;

  $query_per_foc->query( array(
    'post_type' => array( 'products' ),
    'meta_key' => 'acf_post_order',
    'orderby' => 'meta_value_num',
    'order' => 'DESC',
    'tag__not_in' => array( 95 ),
    'tax_query' => array(
      'relation' => 'AND',
      array(
        'taxonomy' => 'cat_focus',
        'field' => 'id',
        'terms' => array( $focus_id ),
        'operator' => 'IN'
      ),
    ),
  ));

  if ( $query_per_foc->have_posts() ) {

    echo '<div class="block__frame">';

      $row_title = $focus_cat->name;
      if ( $row_title ) { echo '<h3 class="block__title">'.$row_title.'</h3>'; }

      echo '<ul class="block__list list--links">';

        while ( $query_per_foc->have_posts() ) {

          $query_per_foc->the_post();
          $prod_title = the_title( '', '', false );
          $prod_url = get_permalink();
          $prod_id = $post->ID;

          echo '<li>';

            echo '<a class="block__link product'; if ( $prod_id == $current_postid ) { echo ' is-active';} echo '" href="'.$prod_url.'">'.$prod_title.'</a>';

          echo '</li>';

        }

        wp_reset_postdata();

      echo '</ul>';

    echo '</div>';
  }
}
?>
