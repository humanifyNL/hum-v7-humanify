<?php
/**
 * Query products per focus tag
 *
 * @package hum-v7-humanify
 */

$focus_terms = get_the_terms( get_the_id(), 'cat_focus' );
if ( !empty( $focus_terms ) ) {
  $focus_ids = wp_list_pluck( $focus_terms, 'term_id' );
}

if ( $focus_ids ) {

  $args = array(
    'post_type' => array( 'products' ),
    'meta_key' => 'acf_post_order',
    'orderby' => 'meta_value_num',
    'order' => 'DESC',
    'tag__not_in' => array( 95 ),
    'tax_query' => array(
      'relation' => 'AND',
      array(
        'taxonomy' => 'cat_focus',
        'field' => 'id',
        'terms' => $focus_ids,
        'operator' => 'IN'
      ),
    ),
  );

  $query_prods = new WP_query ( $args );

  if ( $query_prods->have_posts() ) {

    ?><!--query__products__focus-->

    <div class="grid--previews grid--33">

      <?php
      while ( $query_prods->have_posts() ) {

        $query_prods->the_post();
        include( locate_template( 'template-parts/singles/products/preview-product.php' ) );

      }

      wp_reset_postdata();
      ?>

    </div>

    <?php
  }
}
