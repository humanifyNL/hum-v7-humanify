<?php
/**
 * Template part for query of product links sorted by cat_focus
 *
 * @package hum-v7-humanify
 */
?>
<!--query__links__products__per-cat-->

<div class="grid grid--50">

  <?php
  $query_per_cat = new WP_Query;
  $queried_cats = get_terms( array(
    'taxonomy' => 'category',
    'hide_empty' => true,
    'meta_key' => 'cat_order',
    'orderby' => 'meta_value_num',
  ));

  foreach ( $queried_cats as $cat ) {

    $cat_id = $cat->term_id;

    $query_per_cat->query( array(
      'post_type' => array( 'products' ),
      'meta_key' => 'acf_post_order',
      'orderby' => 'meta_value_num',
      'order' => 'DESC',
      'tag__not_in' => array( 95 ),
      'tax_query' => array(
        'relation' => 'AND',
        array(
          'taxonomy' => 'category',
          'field' => 'id',
          'terms' => array( $cat_id ),
          'operator' => 'IN'
        ),
      ),
    ));

    if ( $query_per_cat->have_posts() ) {
    ?>

      <div class="block block--archive">

        <?php
        $row_title = $cat->name;
        if ( $row_title ) { echo '<h3 class="block__title">'.$row_title.'</h3>'; }
        ?>

        <ul class="block__list list--links">

          <?php
          while ( $query_per_cat->have_posts() ) {

            $query_per_cat->the_post();
            $prod_title = the_title( '', '', false );
            $prod_url = get_permalink();

            echo '<li>';

              echo '<a class="block__link block__link--icon product" href="'.$prod_url.'">'.$prod_title.'</a>';

            echo '</li>';

          }

          wp_reset_postdata();
          ?>

        </ul>

      </div>
      <?php

    }
  }
  ?>

</div>
