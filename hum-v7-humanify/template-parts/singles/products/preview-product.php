<?php
/**
 * Template part used for displaying post frames in loop
 *
 * @package hum-v7-humanify
 */
?>

<article id="post-<?php the_id();?>" class="clickable preview preview--post has-label preview--product<?php if ( !empty($enable_iso) ) { hum_iso_class('post_tag'); }?>">

  <?php
  // thumbnail
  if ( has_post_thumbnail( $post->ID ) ) {

    echo '<div class="block__thumb">';

      the_post_thumbnail('medium');

    echo '</div>';
  }

  // title
  echo '<h3 class="block__title block__title--icon pad">'; the_title(); echo '</h3>';

  hum_excerpt( 'block__text is-excerpt pad' );

  $link_title = get_field( 'post_links_title' , 'option');

  echo '<div class="block__footer label">';
    echo '<a href="'; the_permalink(); echo '" class="'. hum_button_class( 'post' ). '">'.$link_title.'</a>';
  echo '</div>';
  ?>

</article>
