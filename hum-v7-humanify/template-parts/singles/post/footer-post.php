<?php
/**
 * Template part for post-footer
 *
 * @package hum-v7-humanify
 */
?>

<section class="row page-footer page-footer--post style-1">

  <div class="wrap">

    <div class="grid grid--60">

      <div class="block block--left">

        <?php
        echo '<h2 class="row__title">Ook werken met mij?</h2>';

        get_template_part( 'template-parts/singles/products/query-links', 'products__rel-focus');
        ?>

      </div>

      <div class="block block--right">

        <?php
        echo '<h2 class="row__title">Gerelateerd</h2>';

        get_template_part( 'template-parts/singles/post_learn/query-links', 'learn__tax');
        ?>

      </div>

    </div>

  </div>

</section>
