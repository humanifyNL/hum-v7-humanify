<?php
/**
 * Query posts - custom select (acf post objects)
 *
 * @package hum-v7-core
 */

$select_ids = get_sub_field( 'posts_select' );

if( !empty($select_ids) ) {

  // if custom selected
  $args_sel = array(
    'post_type' => array( 'post' ),
    'post__in' => $select_ids,
    'orderby' => 'date',
    'order' => 'DESC',
  );

  $query_posts_sel = new WP_query ( $args_sel );

  if ( $query_posts_sel->have_posts() ) {

    ?>
    <div class="grid--previews select <?php echo hum_grid_preview();?>">

      <?php
      while ( $query_posts_sel->have_posts() ) {

        $query_posts_sel->the_post();
        include( locate_template( 'template-parts/singles/post/preview-post.php' ) );

      }

      wp_reset_postdata();
      ?>

    </div>

    <?php
  }
}
