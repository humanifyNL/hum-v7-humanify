<?php
/**
 * Query posts
 *
 * @package hum-v7-core
 */

$main_id = get_the_id(); // page id for template parts
$args = array(
  'post_type' => array( 'post' ),
  'orderby' => 'date',
  'order' => 'ASC',
);

$query_posts = new WP_query ( $args );

if ( $query_posts->have_posts() ) {

  ?>
  <div class="grid--previews <?php echo hum_grid_preview();?>">

  <?php
  while ( $query_posts->have_posts() ) {

    $query_posts->the_post();
    include( locate_template( 'template-parts/singles/post/preview-post.php' ) );

  }

  wp_reset_postdata();

}
