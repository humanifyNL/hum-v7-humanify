<?php
/**
 * Meta tags bottom
 *
 * ACF blog post options
 *
 * @package hum-v7-core
 */

if ( have_rows( 'post_meta_bot' , 'option' )) {

  ?>
  <div class="block block--meta block--meta--bottom">

    <?php
    while ( have_rows( 'post_meta_bot' , 'option' )) {

      the_row();


      if ( get_row_layout() == 'meta_tax' ) {

        echo '<div class="block__meta">';

          $tax_select = get_sub_field( 'select_meta_terms', 'option');
          foreach ( $tax_select as $tax ) {

            switch ( $tax ) {

              case 'post_tag':
              hum_meta_cp( $tax, 'button' );
              break;

              case 'category':
              hum_meta_cp( $tax, 'button--alt');
              break;

              /*
              case 'custom_post':
              hum_meta_cp( $tax, 'button--wired');
              break;
              */

              default:
              hum_meta_cp( $tax );
            }

          }

        echo '</div>';
      }


      if ( get_row_layout() == 'meta_author' ) {

        if ( !is_search() ) {

          if ( get_sub_field( 'enable_author_block', 'option' ) ) {

            include( locate_template( 'template-parts/singles/author/block--author.php') );

          } else {

            echo '<div class="block__meta block__meta--author">';
            hum_meta_author();
            echo '</div>';
          }
        }

      }


      if ( get_row_layout() == 'meta_date' ) {

        echo '<div class="block__meta block__meta--pub">';

          if ( get_sub_field( 'enable_pub_update', 'option' ) ) {
            hum_meta_published_update();
          } else {
            hum_meta_published();
          }

        echo '</div>';
      }


      if ( get_row_layout() == 'meta_comments' ) {

        echo '<div class="block__meta block__meta--comments">';

          hum_meta_comments();

        echo '</div>';
      }

    }
    ?>

  </div>
  <?php

}
