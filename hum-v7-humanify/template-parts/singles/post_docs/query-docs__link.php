<?php
/**
 * Template part for query of document posts
 *
 * @package hum-v7-humanify
 */

$args_docs = array(
  'post_type' => array( 'post_docs' ),
  'posts_per_page' => 3,
  'meta_key' => 'acf_post_order',
  'orderby' => 'meta_value_num',
  'order' => 'DESC',
);


$query_docs = new WP_query ( $args_docs );

if ( $query_docs->have_posts() ) {

  ?><!-- query-docs__links -->


  <ul class="block__list list--links list--docs">

    <?php
    while ( $query_docs->have_posts() ) {

      $query_docs->the_post();

      $short_title = get_field( 'post_title_short' );

      echo '<li><a href="'; the_permalink(); echo '" class="block__link block__link--icon docs">'.$short_title.'</a></li>';

    }

    wp_reset_postdata();
    ?>

  </ul>

  <?php

}
