<?php
/**
 * Template part for query of document posts - related
 *
 * @package hum-v7-humanify
 */

$main_id = get_the_id();

$args_pf = array(
  'post_type' => array( 'post_docs' ),
  'post__not_in' => array( $main_id ),
  'meta_key' => 'acf_post_order',
  'orderby' => 'meta_value_num',
  'order' => 'DESC',
);

$query_pf = new WP_query ( $args_pf );

if ( $query_pf->have_posts() ) {

  ?>
  <!--query-docs__rel-->
  <h3 class="row__title">Lees verder</h3>

  <ul class="block__list list--links list--docs related">

    <?php
    while ( $query_pf->have_posts() ) {

      $query_pf->the_post();
      $short_title = get_field( 'post_title_short' );

      echo '<li><a href="'; the_permalink(); echo '" class="block__link block__link--icon docs">'.$short_title.'</a></li>';

    }

    wp_reset_postdata();
    ?>

  </ul>

  <?php

}
