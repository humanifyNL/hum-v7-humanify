<?php
/**
 * Template part used for displaying post frames in loop
 *
 * @package hum-v7-humanify
 */

?>
<article id="post-<?php the_id();?>" class="clickable preview preview preview--doc <?php if ( !empty($enable_iso) ) { hum_base_iso_block_class('category'); }?>">

  <?php
  echo '<h3 class="block__title">'; the_title(); echo '</h3>';

  // echo '<p class="rp-date">'.$date.'</p>';
  hum_excerpt();

  // link
  $link_title = get_field( 'post_links_title' , 'option');

  echo '<div class="block__footer">';
    echo '<a href="'; the_permalink(); echo '" class="'. hum_button_class( 'post' ). '">'.$link_title.'</a>';
  echo '</div>';
  ?>

</article>
