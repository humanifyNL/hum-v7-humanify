<?php
/**
* Template part for query of werkwijze links - focus cat
 *
 * @package hum-v7-humanify
 */

$query_per_cat = new WP_Query;
$query_per_cat->query( array(
  'post_type' => array( 'werkwijze' ),
  'meta_key' => 'acf_post_order',
  'orderby' => 'meta_value_num',
  'order' => 'ASC',
  'tax_query' => array(
    'relation' => 'AND',
    array(
      'taxonomy' => 'category',
      'field' => 'slug',
      'terms' => array( 'focus' ),
      'operator' => 'IN'
    ),
  ),
));

if ( $query_per_cat->have_posts() ) {

  $row_title = 'Over website doelen'; //$cat->name;
  if ( $row_title ) { echo '<h3 class="block__title">'.$row_title.'</h3>'; }
  ?>

  <!--query__links__werkwijze__focus-->
  <ul class="block__list list--links">

    <?php
    while ( $query_per_cat->have_posts() ) {

      $query_per_cat->the_post();
      //$prod_title = the_title( '', '', false );
      $prod_title = get_field( 'post_title_short' );
      $prod_url = get_permalink();

      echo '<li>';
        echo '<a class="block__link block__link--icon product" href="'.$prod_url.'">'.$prod_title.'</a>';
      echo '</li>';

    }

    wp_reset_postdata();
    ?>

  </ul>
  <?php
}
?>
