<?php
/**
 * Template part for product page
 *
 * @package hum-v7-humanify
 */
?>

<article id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/singles/products/header', 'products' );
	?>

	<section class="post-content">

		<?php
		get_template_part( 'template-parts/acf/rows/row', '-index' );
		?>

		<section class="row">

			<div class="wrap">

				<div class="grid grid--60">

					<div class="block block--post">

						<?php
						if ( have_rows( 'flex_first_row' ) ) {
							while ( have_rows( 'flex_first_row' ) ) {

								the_row();
								include( locate_template( 'template-parts/acf/rows/row--text-wysi.php') );

							}
						}
						?>

					</div>

					<div class="block"></div>

					<div class="block block--post">

						<?php
						get_template_part( 'template-parts/acf/flex-post' );
						?>

					</div>

					<?php
					get_template_part( 'template-parts/site/blocks/block-index--side' );
					?>

				</div>

			</div>

		</section>

		<?php
		get_template_part( 'template-parts/acf/flex-landing' );
		?>

		<?php
		if ( have_rows( 'product_ww_grp' ) ) {
			while ( have_rows( 'product_ww_grp' ) ) {

				the_row();
				get_template_part( 'template-parts/acf/rows/row', '-product--ww');
			}
		}

		$uitvoering = get_field( 'enable_prod_cost' ); // uitvoering aan = uitvoering product!

		if ( have_rows( 'product_cost_grp' ) ) {
			while ( have_rows( 'product_cost_grp' ) ) {

				the_row();

				if ( !$uitvoering ) {
					get_template_part( 'template-parts/acf/rows/row', '-product--cost' );
				} else {
					get_template_part( 'template-parts/acf/rows/row', '-product--cost--uit' );
				}
			}
		}

		/*
		if ( have_rows( 'product_steps_grp' ) && !$uitvoering  ) {
			while ( have_rows( 'product_steps_grp' ) ) {

				the_row();
				get_template_part( 'template-parts/acf/rows/row', '-product--steps' );
			}
		}
		*/



		$form_enabled = get_field( 'enable_form_prod' );
		if ( $form_enabled ) {
			get_template_part( 'template-parts/acf/rows/row', '-form-product' );
		}
		?>

	</section>

</article><!-- #post-<?php the_ID(); ?> -->

<?php
//get_template_part( 'template-parts/singles/products/footer', 'products');
?>
