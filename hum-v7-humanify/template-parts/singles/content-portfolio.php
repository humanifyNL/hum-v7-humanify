<?php
/**
 * Template part for displaying portfolio content
 *
 * @package hum-v7-humanify
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <?php
  get_template_part( 'template-parts/singles/portfolio/header', 'portfolio' );
  ?>

  <div class="post-content">

    <?php
    get_template_part( 'template-parts/acf/rows/row', '-portfolio__images' );
    ?>

    <section class="row row--postnav style-1">
      <?php
      hum_post_nav_blank();
      ?>
    </section>


    <?php
    get_template_part( 'template-parts/acf/rows/row', '-portfolio__client' );
    get_template_part( 'template-parts/acf/rows/row', '-reviews' );
    get_template_part( 'template-parts/acf/rows/row', '-gallery__post');
    ?>

    <?php
    get_template_part( 'template-parts/acf/flex-page' );
    ?>

  </div>


</article>

<section class="row row--postnav row--postnav__bot style-1">

  <?php
  hum_post_nav_blank();
  ?>

</section>
