<?php
/**
 * The default template for displaying post content
 * Used for single only
 *
 * @package hum-v7-humanify
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <?php
  get_template_part( 'template-parts/singles/post/header', 'post' );
  ?>

  <section class="post-content">

    <div class="wrap">

      <div class="grid grid--60">

        <div class="block block--left">

          <?php
          // content
          if ( have_rows( 'flex_content_post' ) ) {

            get_template_part( 'template-parts/acf/flex-post' );

          } else {

            get_template_part( 'template-parts/site/the-content' );
          }
          ?>

        </div>

        <div class="block block--right">

          <?php
          echo '<div class="entry-meta__tax side" style="text-align: right;">';

            echo '<div class="tax__descr">';
              echo 'Gerelateerde service';
            echo '</div>';
            hum_meta_cp('cat_focus', 'websites');
          echo '</div>';
          ?>

        </div>

      </div>

    </div>

    <?php

    // post nav
    // hum_base_post_nav_thumb();
		?>

	</section>

	<?php
	get_template_part( 'template-parts/singles/post/meta', 'post__bot' );
  ?>

</article><!-- #post-<?php the_ID(); ?> -->

<?php

include( locate_template('template-parts/acf/queries/row--posts__rel.php') );
