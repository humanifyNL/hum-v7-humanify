<?php
/**
 * Query posts_learn
 *
 * @package hum-v7-humanify
 */

$args_l = array(
  'post_type' => array( 'post_learn' ),
  'posts_per_page' => -1,
  'meta_key' => 'acf_post_order',
  'orderby' => 'meta_value_num date',
  'order' => 'DESC',
);

$query_l = new WP_query ( $args_l );

if ( $query_l->have_posts() ) {

  ?><!-- query-learn__iso -->

  <?php
  // set title
  $title_posts = get_field( 'posts_title', 'option' );
  if ( $title_posts ) { echo '<div class="wrap"><h3 class="row__title">'.$title_posts.'</h3></div>'; }


  $enable_iso = true;
  echo '<p class="button-group-label">Sorteer op onderwerp</p>';
  hum_get_query_buttons_iso( $wp_query, 'tag_onderwerp' );
  ?>

  <div class="grid--iso grid--previews">

    <?php
    while ( $query_l->have_posts() ) {

      $query_l->the_post();
      include( locate_template( 'template-parts/singles/post_learn/preview-learn.php' ));

    }
    wp_reset_postdata();
    ?>

  </div>

  <?php
}
