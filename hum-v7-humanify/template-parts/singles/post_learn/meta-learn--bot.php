<?php
/**
 * Meta tags top - post_learn
 *
 * @package hum-v7-core
 */
?>

<div class="block block--meta block--meta--bot">

  <?php
  echo '<h3 class="block__title">Auteur</h3>';

  echo '<div class="block__meta block__meta--author">';
    get_template_part( 'template-parts/site/blocks/block', 'humanify');
  echo '</div>';

  echo '<h3 class="block__title">Publicatie</h3>';

  echo '<div class="block__meta block__meta--pub">';
    hum_meta_published_update();
  echo '</div>';
  ?>

</div>
