<?php
/**
 * Template part for related products on portfolio page
 *
 * @package hum-v7-humanify
 */

$method = get_field( 'portfolio_method', $post->ID );

if ( $method ) {

  echo '<div class="tax tax--prod">';

    $m_name = $method->post_title;
    $m_title = 'Ga naar '. $m_name;
    $m_link = get_permalink($method);
    $m_slug = $method->post_name;

    echo '<a class="tax__link tax__link--method '.$m_slug.'" href="'.$m_link.'" rel="nofollow" title="'.$m_title.'">'.$m_name.'</a>';

  echo '</div>';
}
