<?php
/**
 * Template part for header - portfolio
 *
 * @package hum-v7-humanify
 */
?>

<header class="page-header post-header post-header--portfolio style-1">

  <div class="wrap wrap--post">

    <div class="grid grid--60">

      <div class="block block--title">

        <?php
        // title
        the_title( '<h1 class="page-title">', '</h1>' );
        echo '<link href="'; the_permalink(); echo'"/>';

        hum_case_type( 'tag_types' );

        hum_portfolio_link('btn btn-flat button--alt', 'Naar deze website');
        ?>

      </div>

      <div class="block block--meta block--meta--top">

        <?php
        echo '<div class="block__meta block__meta--tax">';
          hum_type_rollout();
        echo '</div>';

        echo '<div class="block__meta block__meta--pub">';
          hum_meta_updated();
        echo '</div>';
        ?>

      </div>

    </div>

  </div>

</header>
