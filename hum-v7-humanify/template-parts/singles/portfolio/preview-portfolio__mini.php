<?php
/**
 * Template part used for displaying post frames in loop
 *
 * @package hum-v7-humanify
 */

$pf_method = get_field( 'portfolio_method' );
?>

<article id="portfolio-<?php the_id();?>" class="preview preview--portfolio preview--portfolio__mini">

  <div class="block__body">

    <h3 class="block__title"><?php the_title(); ?></h3>

    <?php
    hum_excerpt();
    ?>

    <?php
    echo '<div class="clickable block__footer">';

      hum_portfolio_link('click btn btn-flat button--alt', 'Naar website');

    echo '</div>';
    ?>

  </div>

</article>
