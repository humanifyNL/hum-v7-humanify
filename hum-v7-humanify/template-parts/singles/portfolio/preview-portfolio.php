<?php
/**
 * Template part used for displaying portfolio previews
 *
 * @package hum-v7-humanify
 */
?>

<article id="portfolio-<?php the_id();?>" class="preview preview--portfolio">

  <?php
  // title
  // echo '<h3 class="block__title">'; the_title(); echo '</h3>';

  // thumbnail
  if ( has_post_thumbnail( $post->ID ) ) {

    echo '<div class="block__img">';

      echo '<a href="'; the_permalink(); echo '">';

      the_post_thumbnail('medium');

      echo '</a>';

    echo '</div>';
  }
  ?>

  <div class="block__body">

    <?php
    // link
    echo '<div class="block__footer">';

      hum_portfolio_link('btn btn-flat button--w', 'Naar website');
      echo '<a class="block__btn btn btn-flat button--alt" href="'; the_permalink(); echo '">Bekijk case</a>';

    echo '</div>';

    if ( is_post_type_archive( 'portfolio' ) ) {
      // excerpt
      hum_excerpt();
    }
    ?>

  </div>

</article>
