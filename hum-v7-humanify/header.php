<?php
/**
 * The header for our theme
 * <head> section and everything up until content
 *
 * @package hum-v7-humanify
 */
?>
<!doctype html>

<html <?php language_attributes(); ?> class="no-js">
<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="profile" href="http://microformats.org/profile/specs" />
	<link rel="profile" href="http://microformats.org/profile/hatom" />
		<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-96453961-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-96453961-1');
	</script>

	<?php wp_head(); ?>

</head>

<body <?php body_class();?>>

	<span class="skiplink"><a class="screen-reader-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'hum-base' ); ?>"><?php esc_html_e( 'Skip to content', 'hum-base' ); ?></a></span>

	<noscript>
	  <div id="no-javascript">
	    <?php esc_html_e( 'No JavaScript, no working website. Turn it on, thank you!', 'hum-base' ); ?>
	  </div>
	</noscript>


	<div class="site-container">

	<?php
	include( locate_template( 'template-parts/site/header/header.php' ) );
