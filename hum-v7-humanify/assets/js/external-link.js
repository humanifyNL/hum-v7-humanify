/**
 * Mark links that open in a new window.
 *
 * @package hum-v7-core
 */

jQuery(function($) { 'use strict';

	/* Accessibility enhancements
	 *
	 * Mark links that open in a new window.
	 */
	$( '.post-content a[target="_blank"]' ).append( '<span class="icon-webfont fa-external-link" aria-hidden="true"></span>' );

	// Remove title attributes when the title attribute is the same as the link text
    $('*').each( function () {
        var self = $(this);
        var theTitle = $.trim( self.attr( 'title' ) );
        var theText = $.trim( self.text() );
        if ( theTitle === theText ) {
            self.removeAttr( 'title' );
        }
    } );

} );
