/**
 * Share buttons for social media
 *
 * uses webshare API to share on mobile - with fallback for non-mobile
 * https://css-tricks.com/how-to-use-the-web-share-api/
 *
 * @package hum-v7-core
 */

jQuery(document).ready(function($) {


  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

  const shareButton = document.querySelector('.share-button');
  const shareDialog = document.querySelector('.share-dialog');
  const closeButton = document.querySelector('.dialog__close');

  const title = document.title;
  const url = document.querySelector('link[rel=canonical]') ? document.querySelector('link[rel=canonical]').href : document.location.href;

  shareButton.addEventListener('click', event => {

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if ( /iPad|iPhone|iPod/.test(userAgent) && navigator.share ) {
      navigator.share({
        title: title,
        url: url,
      })
      .catch(console.error);

    } else {
      shareDialog.classList.add('is-open');
      closeButton.classList.add('is-open');
    }

  });

  closeButton.addEventListener('click', event => {
    shareDialog.classList.remove('is-open');
    closeButton.classList.remove('is-open');

  });

});

// bottom
jQuery(document).ready(function($) {

  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

  const shareButtonBot = document.querySelector('.share-button.bottom');
  const shareDialogBot = document.querySelector('.share-dialog.bottom');
  const closeButtonBot = document.querySelector('.dialog__close.bottom');

  const title = document.title;
  const url = document.querySelector('link[rel=canonical]') ? document.querySelector('link[rel=canonical]').href : document.location.href;

  shareButtonBot.addEventListener('click', event => {

    if ( /iPad|iPhone|iPod/.test(userAgent) && !window.MSStream && navigator.share ) {
      navigator.share({
        title: title,
        url: url,
      })
      .catch(console.error);
    } else {
      shareDialogBot.classList.add('is-open');
      closeButtonBot.classList.add('is-open');
    }

  });

  closeButtonBot.addEventListener('click', event => {
    shareDialogBot.classList.remove('is-open');
    closeButtonBot.classList.remove('is-open');

  });

});



/* Set fallback share links (for non mobile)
 *
 * adds links to share button (class= .share + .platform)
 * https://css-tricks.com/simple-social-sharing-links/
 * https://codepen.io/adamcoti/pen/jVGWdG
 *
 */

jQuery(document).ready(function($) {

  setShareLinks();

  function socialWindow(url) {
    var left = (screen.width - 570) / 2;
    var top = (screen.height - 570) / 2;
    var params = "menubar=no,toolbar=no,status=no,width=570,height=570,top=" + top + ",left=" + left;
    // Setting 'params' to an empty string will launch
    // content in a new tab or window rather than a pop-up.
    // params = "";
    window.open(url,"NewWindow",params);
  }

  function setShareLinks() {
    var pageUrl = encodeURIComponent(document.URL);
    var title = encodeURIComponent(document.title);
    var subject = "subject=" + title;
    var tweet = encodeURIComponent($("meta[property='og:description']").attr("content"));
    var body = "body=Bekijk%20dit%20artikel%20" + pageUrl;

    $(".share.facebook").on("click", function() {
      url = "https://www.facebook.com/sharer.php?u=" + pageUrl;
      socialWindow(url);
    });

    $(".share.twitter").on("click", function() {
      url = "https://twitter.com/intent/tweet?url=" + pageUrl + "&text=" + tweet;
      socialWindow(url);
    });

    $(".share.linkedin").on("click", function() {
      url = "https://www.linkedin.com/sharing/share-offsite/?url=" + document.URL;
      socialWindow(url);
    })

    $(".share.email").on("click", function() {
      url = "mailto:?" + subject + "&" + body;
      socialWindow(url);
    })
  }

});
