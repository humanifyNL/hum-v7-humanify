/**
 * Copy link to clipboard
 *
 * https://codepen.io/andrea89/pen/xYJLxG
 *
 * @package hum-v7-core
 */

jQuery(document).ready(function($) {

  var $temp = $("<input>");
  var $url = $(location).attr('href');

  $('.dialog__btn').on('click', function() {

    $("body").append($temp);

    $temp.val($url).select();

    document.execCommand("copy");

    $temp.remove();

    $(".dialog__btn").text("Copied!");

  })

});
