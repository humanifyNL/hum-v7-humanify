<?php
/**
 * The template for displaying taxonomy page for cat_focus
 *
 * @package hum-base-v5
 */

get_header();
?>

<div class="wrap-main">

	<section id="primary" class="content-area">

		<main id="main" class="site-main">

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="page-content">

					<?php
					// header
					get_template_part( 'template-parts/pages/page/header', 'page__tax' );

					// products query
					get_template_part( 'template-parts/acf/queries/row', '-products__focus' );

					// term descr
					get_template_part( 'template-parts/site/term', 'description' );

					// posts
					get_template_part( 'template-parts/acf/queries/row', '-learn__focus' );
					?>

				</div>

			</article><!-- #post-<?php the_ID(); ?> -->

		</main>

	</section>

</div>
<?php

get_footer();
