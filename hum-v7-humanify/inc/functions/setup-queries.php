<?php
/**
 * Query functions
 *
 * @package hum-v7-core
 */

// index post
function hum_core_loop( $query ) {

  if ( $query->is_main_query() ) {

    // home
    if( !is_admin() && is_home() && !is_front_page() ) {

      $query->set( 'order', 'ASC' );
      $query->set( 'orderby', 'date' );
    }

  }

}
add_action( 'pre_get_posts', 'hum_core_loop');


// taxonomy
function hum_tax_queries( $query ) {

  if ( $query->is_main_query() ) {

    if ( is_category() ) {
      $query->set( 'post_type', array('werkwijze', 'post') );
      $query->set( 'order', 'DESC' );
      $query->set( 'meta_key', 'acf_post_order' );
      $query->set( 'orderby', 'meta_value_num date' );
    }

    if ( is_tag() ) {

      $query->set( 'post_type', array('post_learn') );
      $query->set( 'order', 'DESC' );
      $query->set( 'meta_key', 'acf_post_order' );
      $query->set( 'orderby', 'meta_value_num date' );
    }

    if ( is_tax( '', 'cat_focus' ) ) {
      $query->set( 'post_type', array('post_learn') );
      $query->set( 'order', 'DESC' );
      $query->set( 'meta_key', 'acf_post_order' );
      $query->set( 'orderby', 'meta_value_num date' );
    }
  }
}
add_action( 'pre_get_posts', 'hum_tax_queries');


// portfolio
function hum_portfolio_query( $query ) {

  if ( $query->is_main_query() ) {

    // home
    if ( !is_admin() && is_post_type_archive( 'portfolio' ) ) {

      $tax_query = array(
        'relation' => 'AND',
        array(
          'taxonomy' => 'tag_types',
          'field' => 'slug',
          'terms' => array( 'mini' ),
          'operator' => 'NOT IN',
        ),
      );

      $query->set( 'tax_query' , $tax_query );
    }

  }

}
add_action( 'pre_get_posts', 'hum_portfolio_query');
