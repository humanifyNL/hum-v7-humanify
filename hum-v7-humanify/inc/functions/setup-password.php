<?php
/**
  * Custom PW protected form
  *
  * @package hum-v7-core
  */

add_filter( 'the_password_form', 'hum_password_form' );
function hum_password_form() {
    global $post;
    $label = 'pwbox-' . ( empty( $post->ID ) ? rand() : $post->ID );
    $output = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form" method="post">
    <p>' . esc_html__( 'Deze pagina is afgeschermd, vul het wachtwoord in.', 'hum-core' ) . '</p>
    <p>
    <input class="pw-field pw-form" type="password" name="post_password" id="' . $label . '" type="password" size="20"/><input type="submit" name="Submit" class="button pw-form" value="' . esc_attr__( 'Bevestig', 'text-domain' ) . '" />
    </p>
    </form>
    ';
    return $output;
}
