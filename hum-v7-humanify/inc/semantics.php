<?php
/**
 * Hum Base websemantics.
 *
 * Some functions to add backwards compatibility to older WordPress versions.
 * Adds some awesome websemantics like microformats(2) and microdata.
 *
 * @link https://schema.org
 * @link http://indiewebcamp.com
 *
 * @package hum-v7-core
 */

/**
 * Table of Contents:
 *
 * - Websemantics implementation catalog.
 * - Adds custom classes to the array of body classes.
 * - Additional body classes - add category (or any other taxonomy) class for single posts.
 * - Additional body classes - add Page slug class.
 * - Additional post classes. Adds custom classes to the array of post classes.
 * - Add semantics. Automatically define the Schema.org type you want to use for the type of the content.
 * - Echos the semantic classes added via the "hum_core_semantics" filters.
 * - Allow Schema.org attributes to be added to HTML tags in the editor (but not for comments).
 * - Add rel-prev attribute to previous_image_link.
 * - Add rel-next attribute to next_image_link.
 * - Adds custom classes to the array of comment classes.
 *
 * ----------------------------------------------------------------------------
 */

/**
 * Schema.org microdata format
 *   @link https://schema.org
 *   @link https://html.spec.whatwg.org/multipage/microdata.html
 *
 * itemtype="https://schema.org/Blog" (in inc/semantics.php only)
 * itemtype="https://schema.org/BlogPosting" (in inc/semantics.php only)
 * itemtype="https://schema.org/UserComments" (in comments.php only)
 * itemtype="https://schema.org/WebPage" (in inc/semantics.php only)
 * itemtype="https://schema.org/Person" (in inc/template-tags.php and partial-templates/author-bio.php)
 * itemtype="https://schema.org/CollectionPage"
 * itemtype="https://schema.org/SearchResultsPage" (in inc/semantics.php only)
 * itemtype="https://schema.org/AboutPage" (in inc/semantics.php only)
 * itemtype="https://schema.org/ContactPage" (in inc/semantics.php only)
 * itemtype="https://schema.org/ProfilePage" (in inc/semantics.php only)
 * itemtype="https://schema.org/ImageObject" (in inc/semantics.php only)
 * itemtype="https://schema.org/CreativeWork" (in inc/semantics.php only)
 *
 *
 * - itemprop="image" (in inc/semantics.php only)
 *
 */


/**
 * Adds custom classes to the array of body classes.
 *
 * param array $classes Existing class values.
 *
 * @return array Filtered class values.
 */

if ( ! function_exists( 'hum_core_body_class' ) ) {

	function hum_core_body_class( $classes ) {

		// Sidebar in use
		if ( is_active_sidebar( 'sidebar-main') || is_active_sidebar( 'sidebar-static')) {
			$classes[] = 'sidebar-yes';
		} else { // Sidebar not in use
			$classes[] = 'sidebar-no';
		}

		// Index, archive views.
		if ( is_archive() || is_search() || is_home() ) {
			$classes[] = 'list-view';
		}

		// Single views. is_singular applies when one of the following returns true: is_single(), is_page() or is_attachment().
		if ( is_singular() && ! is_front_page() ) {
			$classes[] = 'singular';
		}

		// Has top header
		$enable_lang = get_field( 'enable_language_select', 'option' );
		$enable_links = get_field( 'enable_header_links', 'option' );
		if ( $enable_lang || $enable_links ) {
			$classes[] = 'header-top';
		}

		return $classes;
	}

add_filter( 'body_class', 'hum_core_body_class' );
}



/**
 * Additional body classes - add category (or any other taxonomy) class for single posts.
 * Controlled with conditionals, nicenames, by Brian Krogsgard.
 *
 * @link http://www.organizedthemes.com/body-class-tricks-for-wordpress-sites/#comment-315
 */

function hum_core_tax_body_class( $classes ) {
	if ( is_singular('post') ) {

		global $post;
		$terms = get_the_terms( $post->ID, 'category' );

		if ( $terms && ! is_wp_error( $terms ) ) {

			foreach ( $terms as $term ) {
				// assign body class for the website categories
				$classes[] =$term->slug;
			}
		}
	}

	if ( is_singular('products') ) {

		global $post;
		$terms = get_the_terms( $post->ID, 'post_tag' );

		if ( $terms && ! is_wp_error( $terms ) ) {

			foreach ( $terms as $term ) {
				// assign body class for the website categories
				$classes[] = 'tax-'.$term->slug;
			}
		}
	}

	return $classes;
}
add_filter( 'body_class', 'hum_core_tax_body_class' );



/**
 * Additional body classes - add Page slug class.
 *
 * @link http://www.wpbeginner.com/wp-themes/how-to-add-page-slug-in-body-class-of-your-wordpress-themes/
 */

function hum_core_page_slug_body_class( $classes ) {

	if ( is_singular() ) {

		global $post;

		if ( isset( $post ) ) {
			$classes[] = $post->post_type . '-' . $post->post_name;
		}
	}

	return $classes;
}
add_filter( 'body_class', 'hum_core_page_slug_body_class' );



/**
 * Add semantics.
 *
 * @param string $id the class identifier
 * @return array
 */

if ( ! function_exists( 'hum_core_get_semantics' ) ) {

	function hum_core_get_semantics( $id = null ) {

		$classes = array();

		// Add default values.
		switch ( $id ) {

			case 'body':

				if ( ! is_singular() ) {
					$classes['itemscope'] = array( '' );
					$classes['itemtype']  = array( 'https://schema.org/CollectionPage' );

					if ( is_front_page() ) {

						// Is posts index page.
						if( is_home() ) {
							// For personal blog you might use https://schema.org/Blog instead.
							$classes['itemtype']  = array( 'https://schema.org/CollectionPage' );
						}
						// If static front page, see bellow: is_single.

					} elseif ( is_archive() ) {

						// Is author page.
						if ( is_author() ) {
							// Also could be https://schema.org/CollectionPage
							$classes['itemtype']  = array( 'https://schema.org/ProfilePage' );

							// Is category.
						} elseif ( is_category() ) {
							$classes['itemtype']  = array( 'https://schema.org/CollectionPage' );

							// Is specific category. E.g. category for our blog.
							// See other conditional options: https://developer.wordpress.org/reference/functions/is_category/
							// If website is blog, you could also use:
							// } elseif ( is_home() || ( is_archive() && !is_author() ) {
						} elseif ( is_category( 'blog' ) ) {
							$classes['itemtype']  = array( 'https://schema.org/Blog' );

							// Other archive types.
						} else {
							$classes['itemtype']  = array( 'https://schema.org/CollectionPage' );
						}

						// Is search results page.
					} elseif ( is_search() ) {
						$classes['itemtype']  = array( 'https://schema.org/SearchResultsPage' );
					}

					// Is single post.
				} elseif ( is_single() ) {
					$classes['itemscope'] = array( '' );
					$classes['itemtype']  = array( 'https://schema.org/WebPage' );
					// For personal blog you might use instead:
					// $classes['itemtype']  = array( 'https://schema.org/BlogPosting' );

					// Is single page.
				} elseif ( is_page() ) {
					$classes['itemscope'] = array( '' );
					$classes['itemtype']  = array( 'https://schema.org/WebPage' );

					// Specific pages. In this case we specify only about us/me and contact pages.
					// For your FAQ page you would use itemtype QAPage
					// Check other specific WebPage types: https://schema.org/WebPage
					// You could also use is_page( 1 ) to select page by its ID (in this example ID=1).
					// See other conditional options: https://developer.wordpress.org/reference/functions/is_page/
					if ( is_page_template ( 'page-templates/about.php' ) ) {
						$classes['itemtype']  = array( 'https://schema.org/AboutPage' );

						// Contacts page.
					} elseif ( is_page_template ( 'page-templates/contact.php' ) ) {
						$classes['itemtype']  = array( 'https://schema.org/ContactPage' );
					}

					// More custom examples:
					// Is of movie post type.

					/*
					elseif ( is_singular('movies') ) {
						$classes['itemtype']  = array( 'https://schema.org/ItemPage' );
					}


					// OR Add several custom post types where each one describes a single item.


					elseif ( is_singular( array(
						'book',
						'movie',
					) ) ) {
						$classes['itemtype']  = array( 'https://schema.org/ItemPage' );
					}
					*/
				}

			break; // case body


		case 'site-title':

			if ( ! is_singular() ) {
				$classes['itemprop']  = array( 'name' );
				// .site-title class is required as it is used in style.css, inc/customizer.php and .js files.
				$classes['class']     = array( 'site-title' );

				// I'm not sure if we need to use Site title in singular posts/pages. For post title we use "headline", so "name" is available to use.
				// For singular pages display only required CSS class.
			} else {
				// .site-title class is required as it is used in style.css, inc/customizer.php and .js files.
				$classes['class']     = array( 'site-title' );
			}

		break; // case site title


		case 'site-description':

			if ( ! is_singular() ) {
				$classes['itemprop']  = array( 'description' );
				// .site-description class is required as it is used in style.css and other files.
				$classes['class']     = array( 'site-description' );

				// For singular pages display only required CSS class.
			} else {
				// .site-description class is required as it is used in style.css and other files.
				$classes['class']     = array( 'site-description' );
			}

		break; // case site-description


		case 'site-url':

			if ( ! is_singular() ) {
				$classes['itemprop']  = array( 'url' );
				$classes['class']     = array( 'url' );
			}

		break; // case site-url


		case 'post':

			if ( ! is_singular() ) {
				// Any property to describe Article???
				$classes['itemscope'] = array( '' );
				$classes['itemtype']  = array( 'https://schema.org/Article' );
				// For personal blog you might use:
				// $classes['itemtype']  = array( 'https://schema.org/BlogPosting' );
				// $classes['itemprop']  = array( 'blogPost' );

			} else {
				// Post is an image attachment.
				if ( wp_attachment_is_image() ) {
					$classes['itemscope'] = array( '' );
					$classes['itemtype']  = array( 'https://schema.org/ImageObject' );

					// Post is of Image post format.
				} elseif ( has_post_format( 'image' ) ) {
					$classes['itemscope'] = array( '' );
					$classes['itemtype']  = array( 'https://schema.org/ImageObject' );

					// Post is of Quote post format.
				} elseif ( has_post_format( 'quote' ) ) {
					$classes['itemscope'] = array( '' );
					$classes['itemtype']  = array( 'https://schema.org/CreativeWork' );

					// Post is of Status post format.
				} elseif ( has_post_format( 'status' ) ) {
					$classes['itemscope'] = array( '' );
					$classes['itemtype']  = array( 'https://schema.org/BlogPosting' );

				} else {
					// Looks that if mainEntityOfPage is specified (link element with article permalink bellow the title in content.php),
					// there is no need to specify mainEntity for the Article container:
					// @link http://www.seoskeptic.com/how-to-use-schema-org-v2-0s-mainentityofpage-property/
					// More resources: @link https://stackoverflow.com/questions/34466028/how-to-implement-mainentityofpage-to-this-specific-site
					// @link https://webmasters.stackexchange.com/questions/87940/new-required-mainentityofpage-for-article-structured-data

					// $classes['itemprop']  = array( 'mainEntity' );
					$classes['itemscope'] = array( '' );
					// For personal blog you might use https://schema.org/BlogPosting
					$classes['itemtype']  = array( 'https://schema.org/Article' );
					// This ID (should it be #post-thumbnail ?) could be used for the itemref in the article element (in inc/semantics.php) to point to featured image (see header.php) for the Google AMP Articles Rich Snippets "Article image" validation.
					// $classes['itemref']  = array( 'featured-image' );
				}

			}

		break; // case post

		} // end switch

		$classes = apply_filters( 'hum_core_semantics', $classes, $id );
		$classes = apply_filters( "hum_core_semantics_{$id}", $classes, $id );
		// ^ Double quotes above are for a reason: https://wordpress.org/support/topic/hum_core_semantics_id-hook-firing-for-each-type-of-element/

		return $classes;
	}
}



/**
 * Echos the semantic classes added via the "hum_core_semantics" filters.
 *
 * @param string $id the class identifier
 */
function hum_core_semantics( $id ) {

	$classes = hum_core_get_semantics( $id );

	if ( ! $classes ) {
		return;
	}

	foreach ( $classes as $key => $value ) {
		echo ' ' . esc_attr( $key ) . '="' . esc_attr( join( ' ', $value ) ) . '"';
	}
}


if ( ! function_exists( 'hum_core_allow_schema_markup' ) ) {

	// Allow Schema.org attributes to be added to HTML tags in the editor (but not for comments).
	function hum_core_allow_schema_markup() {

		global $allowedposttags;

		foreach( $allowedposttags as $tag => $attr ) {
			$attr[ 'itemscope' ] = array();
			$attr[ 'itemtype' ] = array();
			$attr[ 'itemprop' ] = array();
			$allowedposttags[ $tag ] = $attr;
		}

		return $allowedposttags;
	}
	add_action( 'init', 'hum_core_allow_schema_markup' );
}


/**
 * Add rel-prev attribute to previous_image_link.
 *
 * @param string a-tag
 * @return string
 */
function hum_core_semantic_previous_image_link( $link ) {

	return preg_replace( '/<a/i', '<a rel="prev"', $link );
}
add_filter( 'previous_image_link', 'hum_core_semantic_previous_image_link' );


/**
 * Add rel-next attribute to next_image_link.
 *
 * @param string a-tag
 * @return string
 */
function hum_core_semantic_next_image_link( $link ) {

	return preg_replace( '/<a/i', '<a rel="next"', $link );
}
add_filter( 'next_image_link', 'hum_core_semantic_next_image_link' );


/**
 * Add Schema.org support to search forms.
 *
 */
function hum_core_get_search_form( $form ) {
	$form = preg_replace( '/<form/i', '<form itemprop="potentialAction" itemscope itemtype="http://schema.org/SearchAction"', $form );
	$form = preg_replace( '/<\/form>/i', '<meta itemprop="target" content="' . site_url( '/?s={search} ' ) . '"/></form>', $form );
	$form = preg_replace( '/<input type="search"/i', '<input type="search" itemprop="query-input"', $form );

	return $form;
}
add_filter( 'get_search_form', 'hum_core_get_search_form' );
