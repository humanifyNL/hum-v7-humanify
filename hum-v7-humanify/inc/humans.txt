/* We are people, not machines. Read more at: http://humanstxt.org */

/* SITE BY */
	Name: Robbert vanHooij, https://Humanify.nl

/* TEAM */
	Role: Web developer, Web designer, SEO strategist
	Name: Robbert van Hooij
	URI: https://Humanify.nl
	Contact: robbert [at] Humanify.nl
	LinkedIn: linkedin.com/in/Robbertvanhooij
	Twitter: @Robberthooij	Location: Arnhem, GLD, NL
	From: Arnhem, Netherlands

/* SPECIAL THANKS */
	Name: Tomas Mackevicius
	URI: http://mtomas.com/1/
	Name: Team Automattic
	URI: https://automattic.com
	Name: Elliot Condon
	URI: https://www.advancedcustomfields.com/

/* SITE */
	Launch date: 2018-01-01
	Last update: 2021-02-01
	Language: Netherlands Doctype: HTML5
	CMS: WordPress
	Theme: Hum-v6-core

/* TECHNOLOGY COLOPHON */
	Standards: HTML5, CSS3, BEM
	Components: SASS, HTML5 Shiv, ACF
	Software: Notepad++, ATOM, XAMPP, Chrome, NPM
