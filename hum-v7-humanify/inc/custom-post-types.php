<?php
/**
 * Custom post functions
 *
 */

function create_post_type() {

  register_post_type( 'post_learn',
    array(
      'labels' => array(
        'name'              => __( 'Posts Learn' ),
        'singular_name'     => __( 'Post Learn' ),
  			'all_items'         => __( 'All Learn Posts'),
  			'view_item'  	      => __( 'View Learn Post'),
  			'add_new_item'      => __( 'Add New Learn Post'),
  			'add_new'           => __('Add New'),
  			'edit_item'         => __('Edit Learn Post'),
  			'update_item'       => __('Update Learn Post'),
  			'search_item'       => __('Search Learn Post'),
      ),
      'public'              => true,
      'has_archive'         => true,
			'hierarchical'        => false,
			'publicly_queryable'  => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
	 	 	'show_in_nav_menus'   => true,
	 		'show_in_admin_bar'   => true,
			'supports' => array('title','thumbnail','editor','hum_core_postexcerpt','revisions'),
			'taxonomies'  => array( 'post_tag', 'category' ),
			'rewrite' => array('slug' => 'learn'),
		)
  );

  register_post_type( 'werkwijze',
    array(
      'labels' => array(
        'name'              => __( 'Werkwijze' ),
        'singular_name'     => __( 'Werkwijze' ),
        'all_items'         => __( 'Alle Posts'),
        'view_item'  	      => __( 'View Post'),
        'add_new_item'      => __( 'Add Werkwijze Post'),
        'add_new'           => __('Add Post'),
        'edit_item'         => __('Edit Post'),
        'update_item'       => __('Update Post'),
        'search_item'       => __('Search Posts'),
      ),
      'public'              => true,
      'has_archive'         => true,
      'hierarchical'        => false,
      'publicly_queryable'  => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'supports' => array('title','thumbnail','editor','hum_core_postexcerpt','revisions'),
      'taxonomies'  => array( 'post_tag', 'category' ),
      'rewrite' => array('slug' => 'werkwijze'),
    )
  );

  register_post_type( 'portfolio',
    array(
      'labels' => array(
        'name'              => __( 'Portfolio' ),
        'singular_name'     => __( 'Portfolio' ),
  			'all_items'         => __( 'All Portfolio Cases'),
  			'view_item'         => __( 'View Portfolio Case'),
  			'add_new_item'      => __( 'Add New Portfolio Case'),
  			'add_new'           => __('Add New Case'),
  			'edit_item'         => __('Edit Portfolio Case'),
  			'update_item'       => __('Update Portfolio Case'),
  			'search_item'       => __('Search Portfolio Case'),
      ),
      'public'              => true,
      'has_archive'   	    => true,
			'hierarchical'        => false,
			'publicly_queryable'  => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
	 	 	'show_in_nav_menus'   => true,
	 		'show_in_admin_bar'   => true,
			'supports' => array('title','thumbnail','editor','hum_core_postexcerpt','revisions'),
			'taxonomies'  => array( 'tag_types' ),
			'rewrite' => array('slug' => 'portfolio'),
		)
  );

  register_post_type( 'post_support',
    array(
      'labels' => array(
        'name'              => __( 'Posts Support' ),
        'singular_name'     => __( 'Post Support' ),
        'all_items'         => __( 'All Support Posts'),
        'view_item'         => __( 'View Support Post'),
        'add_new_item'      => __( 'Add New Support Post'),
        'add_new'           => __('Add New'),
        'edit_item'         => __('Edit Support Post'),
        'update_item'       => __('Update Support Post'),
        'search_item'       => __('Search Support Post'),
      ),
      'public'              => true,
      'has_archive'         => true,
      'hierarchical'        => false,
      'publicly_queryable'  => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'supports' => array('title','editor','hum_core_postexcerpt','revisions'),
      //'taxonomies'  => array( 'post_tag' ),
      'rewrite' => array('slug' => 'support'),
    )
  );

  register_post_type( 'post_docs',
    array(
      'labels' => array(
        'name'              => __( 'Documents' ),
        'singular_name'     => __( 'Documents' ),
        'all_items'         => __( 'All Documents'),
        'view_item'         => __( 'View Document'),
        'add_new_item'      => __( 'Add New Document'),
        'add_new'           => __('Add New'),
        'edit_item'         => __('Edit Document'),
        'update_item'       => __('Update Document'),
        'search_item'       => __('Search Document'),
      ),
      'public'              => true,
      'has_archive'         => true,
      'hierarchical'        => false,
      'publicly_queryable'  => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'supports' => array('title','editor','hum_core_postexcerpt'),
      //'taxonomies'  => array( 'post_tag' ),
      'rewrite' => array('slug' => 'docs'),
    )
  );

}
add_action( 'init', 'create_post_type' );


function create_learn_taxonomies() {

  $labels_pf = array(
    'name'              => _x( 'Type', 'taxonomy general name' ),
    'singular_name'     => _x( 'Type', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Type' ),
    'all_items'         => __( 'All Types' ),
    'parent_item'       => __( 'Parent Type' ),
    'parent_item_colon' => __( 'Parent Type:' ),
    'edit_item'         => __( 'Edit Type' ),
    'update_item'       => __( 'Update Type' ),
    'add_new_item'      => __( 'Add New Type' ),
    'new_item_name'     => __( 'New Type Name' ),
    'menu_name'         => __( 'Type' ),
  );

  $args_pf = array(
    'hierarchical'      => false,
    'labels'            => $labels_pf,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'portfolio-type' ),
  );

  register_taxonomy( 'tag_types', array( 'post_portfolio' ), $args_pf );

  $labels_ow = array(
    'name'              => _x( 'Onderwerp', 'taxonomy general name' ),
    'singular_name'     => _x( 'Onderwerp', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Onderwerp' ),
    'all_items'         => __( 'All Onderwerp' ),
    'parent_item'       => __( 'Parent Onderwerp' ),
    'parent_item_colon' => __( 'Parent Onderwerp:' ),
    'edit_item'         => __( 'Edit Onderwerp' ),
    'update_item'       => __( 'Update Onderwerp' ),
    'add_new_item'      => __( 'Add New Onderwerp' ),
    'new_item_name'     => __( 'New Type Onderwerp' ),
    'menu_name'         => __( 'Onderwerp' ),
  );

  $args_ow = array(
    'hierarchical'      => true,
    'labels'            => $labels_ow,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'onderwerp' ),
  );

  register_taxonomy( 'tag_onderwerp', array( 'post_learn' ), $args_ow );
}

add_action( 'init', 'create_learn_taxonomies', 0 );
