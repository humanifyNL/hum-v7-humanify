<?php
/**
 * Hum Base buttons for post/page blocks
 *
 *
 * @package hum-v7-core
 */

 if ( !function_exists( 'hum_button' ) ) {


  function hum_button_class( $preview_type ) {

    // determine source of post block
    if ( $preview_type == 'post' ) {

      $button_type = get_field( 'post_links', 'option' );

    } elseif ( $preview_type == 'post_rel' ) {

      $button_type = get_field( 'rel_links', 'option' );

    } elseif ( $preview_type = 'page' ) {

      $button_type = get_field( 'page_links', 'option' );

    } else {

      $button_type = 'link';
    }


    // switch
    switch ( $button_type ) {

      case 'button':
        $button_class = 'click block__btn btn button';
        break;

      case 'button--alt':
        $button_class = 'click block__btn btn button--alt';
        break;

      case 'button--w':
        $button_class = 'click block__btn btn button--w';
        break;

      case 'button--wired':
        $button_class = 'click block__btn btn button--wired';
        break;

      case 'button--wired--alt':
        $button_class = 'click block__btn btn button--wired--alt';
        break;

      case 'block__link':
        $button_class = 'click block__link block__link--label';
        break;

      default:
        $button_class = 'click block__link';
    }

    return $button_class;

  }

}


/* ACF populate select field
 *
 * https://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 * fieldname = postlinks
 */

function acf_load_button_field_choices( $field ) {

    // reset choices
    $field['choices'] = array(
      'button' => 'Button',
      'button--wired' => 'Wired button',
      'button--w' => 'White button',
      'block__link' => 'Default link',
    );

    // return the field
    return $field;

}

// preload choices for these fields:
add_filter('acf/load_field/name=post_links', 'acf_load_button_field_choices');
add_filter('acf/load_field/name=page_links', 'acf_load_button_field_choices');
add_filter('acf/load_field/name=rel_links', 'acf_load_button_field_choices');
add_filter('acf/load_field/name=link_type', 'acf_load_button_field_choices');



/* ACF populate select field
 *
 * https://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 * fieldname = postlinks
 */

function acf_load_button_field_all_choices( $field ) {

    // reset choices
    $field['choices'] = array(
      'button' => 'Primary button',
      'button--alt' => 'Secondary button',
      'button--wired' => 'Wired Primary button',
      'button--wired-alt' => 'Wired Secondary button',
      'button--w' => 'White button',
      'block__link' => 'Default link',
    );

    // return the field
    return $field;

}

// preload choices for these fields:
add_filter('acf/load_field/name=cat_link_select', 'acf_load_button_field_all_choices');
add_filter('acf/load_field/name=tag_link_select', 'acf_load_button_field_all_choices');
