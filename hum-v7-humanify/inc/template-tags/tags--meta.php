<?php
/**
 * Custom template tags
 * Meta-tags on top & bottom of article
 *
 * @package hum-v7-core
 */

 /**
 * Content meta information below the post title
 * Create your own hum_base_entry_meta_top() to override in a child theme's funcions.php file.
 */
if ( ! function_exists( 'hum_meta_author' ) ) {

  function hum_meta_author() {

    echo '<div class="post-author">'. get_the_author().'</div>';
  }
}

// published
if ( ! function_exists( 'hum_meta_published' ) ) {

  function hum_meta_published() {

    // published on
    if ( is_single() ) {

      $time_string = '<time class="post-date published" datetime="%1$s">%2$s</time>';
    } else {

      $time_string = '<time class="post-date published" datetime="%1$s">%2$s</time>';
    }

    $time_string = sprintf( $time_string,
      esc_attr( get_the_date( 'c' ) ),
      get_the_date()
    );

    echo '<div class="date">'.$time_string.'</div>';

  }
}

// updated
if ( ! function_exists( 'hum_meta_updated' ) ) {

  function hum_meta_updated() {

    if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {

      $time_string = '<time class="post-date updated" datetime="%3$s">%4$s</time>';

    } else {

      $time_string = '<time class="post-date updated" datetime="%1$s">%2$s</time>';

    }

    $time_string = sprintf( $time_string,
      esc_attr( get_the_date( 'c' ) ),
      get_the_date(),
      esc_attr( get_the_modified_date( 'c' ) ),
      get_the_modified_date()
    );

    echo '<div class="date">'.$time_string.'</div>';
  }
}

// published & updated
if ( ! function_exists( 'hum_meta_published_update' ) ) {

  function hum_meta_published_update() {

    if ( is_single() ) {

      if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {

        $time_string = '<time class="post-date published" datetime="%1$s">%2$s</time><time class="post-date updated" datetime="%3$s">Laatste update: %4$s</time>';

      } else {
        $time_string = '<time class="post-date published" datetime="%1$s">%2$s</time>';
      }

    } else {

      if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {

        $time_string = '<time class="post-date published" datetime="%3$s">%4$s</time>';
      } else {

        $time_string = '<time class="post-date published" datetime="%1$s">%2$s</time>';
      }
    }

    $time_string = sprintf( $time_string,
      esc_attr( get_the_date( 'c' ) ),
      get_the_date(),
      esc_attr( get_the_modified_date( 'c' ) ),
      get_the_modified_date()
    );

    echo '<div class="date">'.$time_string.'</div>';

  }
}

// categories custom tax
if ( ! function_exists( 'hum_meta_cp' ) ) {

  function hum_meta_cp( $tax = 'post_tag', $class = 'button' ) {

    $get_post_terms = get_the_terms( get_the_id(), $tax );

    if ( !empty( $get_post_terms ) && !is_wp_error( $get_post_terms ) ) {

      echo '<div class="tax">';

        foreach ( $get_post_terms as $post_term ) {

          $term_name = $post_term->name;
          $term_slug = $post_term->slug;
          $term_title = 'Alles over '.$term_name;
          $term_link = get_term_link( $post_term, $tax );

          echo '<a class="'.$class.' tax__link tax__link--'.$term_slug.'"';
          echo ' href="'.$term_link.'" rel="nofollow" title="'.$term_title.'">'.$term_name.'</a>';
        }

      echo '</div>';
    }
  }
}

// comments
if ( ! function_exists( 'hum_meta_comments' ) ) {

  function hum_meta_comments() {

  // comments
  	if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) {

  	echo '<span class="comments-link">';
  		comments_popup_link(
  			sprintf(
  				wp_kses(
  					/* translators: %s: post title. */
  					__( 'Reageer<span class="screen-reader-text"> op %s</span>', 'hum-base' ),
  					array(
  						'span' => array(
  							'class' => array(),
  						)
  					)
  				),
  				get_the_title()
  			)
  		);
  		echo '</span>';
  	}
  }
}

// auto-populate select field with taxonomy terms
function hum_select_meta_terms( $field ) {

  // get array of taxonomy term names
  $new_arr = hum_current_tax();
  // reset field choices
  $field['choices'] = $new_arr;
  // return the field
  return $field;
}
add_filter('acf/load_field/name=select_meta_terms', 'hum_select_meta_terms');
