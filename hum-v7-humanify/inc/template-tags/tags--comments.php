<?php
/**
 * Custom template tags
 * Post Comments
 *
 * @package hum-v7-core
 */

if ( ! function_exists( 'hum_comment' ) ) {

/**
* Template for comments and pingbacks
* To override this walker in a child theme without modifying the comments template
* simply create your own hum_comment() in a child theme's funcions.php file, and that function will be used instead.
*
* Used as a callback by wp_list_comments() for displaying the comments.
*/

  function hum_comment( $comment, $args, $depth ) {

    $GLOBALS['comment'] = $comment;

    switch ( $comment->comment_type ) {
    	case 'pingback' :
    	case 'trackback' :
    	// Display trackbacks differently than normal comments

      ?>
      <li id="comments__list-ping-<?php comment_ID(); ?>" <?php comment_class(); ?>>
    	<p><?php esc_html_e( 'Pingback:', 'hum-base' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( esc_html__( '(Edit)', 'hum-base' ), '<span class="edit-link">', '</span>' ); ?></p>

      <?php
      break;
    	default :
    	// Proceed with normal comments
    	global $post;
      ?>

      <li class="comments__list-item n<?php comment_ID(); ?>">

      	<article id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?> itemprop="comment" itemscope="itemscope" itemtype="https://schema.org/Comment">

      		<header class="comment__header">

      			<?php
    				echo get_avatar( $comment, 48 );
            echo '<div class="comment__meta">';

      				printf( '<cite class="comment__cite"><span class="comment__cite__author p-author vcard hcard h-card" itemprop="creator" itemscope="itemscope" itemtype="http://schema.org/Person"><span class="fn p-name" itemprop="name">%1$s</span></span>%2$s</cite>',
      					get_comment_author_link(),
      					// If current post author is also comment author, make it known visually
      					( $comment->user_id === $post->post_author ) ? '<span class="post-author-label">' . esc_html__( 'Post author', 'hum-base' ) . '</span>' : ''
      				);
      				printf( '<time class="comment__date" datetime="%1$s" itemprop="datePublished dateModified dateCreated">%2$s</time>',
      					get_comment_time( 'c' ),
      					/* translators: 1: date, 2: time. */
      					sprintf( esc_html__( '%1$s at %2$s', 'hum-base' ), get_comment_date(), get_comment_time() )
      				);

            echo '</div>';
      			?>

      		</header><!-- .comment__header -->

      		<?php
          if ( '0' == $comment->comment_approved ) {
            ?>
            <p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'hum-base' ); ?></p>
            <?php
          }
          ?>

      		<section class="comment-content p-summary p-name" itemprop="text name description">

      			<?php
            comment_text(); ?>

      			<div class="comment-content__links">

      			  <div class="reply">
      					<?php comment_reply_link( array_merge( $args, array(
      						'reply_text' => esc_html__( 'Reply', 'hum-base' ),
      						'depth'      => $depth,
      						'max_depth'  => $args['max_depth'],
      						) ) );
      					?>
      				</div>

      			  <?php
              edit_comment_link( esc_html__( 'Edit', 'hum-base' ), '<div class="edit">', '</div>' ); ?>

      		  </div>

      		</section>

        </article><!-- #comment-## -->
      <?php

    break;
    }
  }
}

/**
* HTML5 compatibility. Adds the new HTML5 input types to the comment-form.
*
* @param string $form
* @return string
*/

function hum_comment_autocomplete( $fields ) {
	$fields['author'] = preg_replace( '/<input/', '<input autocomplete="nickname name" ', $fields['author'] );
  $fields['email']  = preg_replace( '/<input/', '<input autocomplete="email" ', $fields['email'] );
	$fields['url']    = preg_replace( '/<input/', '<input autocomplete="url" ', $fields['url'] );

	return $fields;
}
add_filter( 'comment_form_default_fields', 'hum_comment_autocomplete' );
