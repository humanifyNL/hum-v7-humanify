<?php
/**
 * Custom template tags
 *
 * @package hum-v7-humanify
 */


if ( ! function_exists( 'hum_cats_cp_rollout_r' ) ) {

  function hum_cats_cp_rollout_r( $tax = 'category' ) {

    $get_post_cats = get_the_terms( get_the_id(), $tax );

    if ( !empty( $get_post_cats ) && !is_wp_error( $get_post_cats ) ) {

      foreach ( $get_post_cats as $post_cat ) {

        //$cat_name = rtrim($post_cat->name, "s");
        $cat_name = $post_cat->name;
        $cat_title = 'Alles over '.$cat_name;
        $cat_link = get_term_link( $post_cat, $tax );

        echo '<a class="tax__link rollout is-right" href="'.$cat_link.'" rel="nofollow">';
        echo '<span class="rollout-visible">'.$cat_name.'</span>';
          echo '<span class="rollout-hidden">bekijk alles</span>';
        echo '</a>';
      }
    }
  }
}

if ( ! function_exists( 'hum_cats_cp_rollout' ) ) {

  function hum_cats_cp_rollout( $tax = 'category' ) {

    $get_post_cats = get_the_terms( get_the_id(), $tax );

    if ( !empty( $get_post_cats ) && !is_wp_error( $get_post_cats ) ) {

      foreach ( $get_post_cats as $post_cat ) {

        //$cat_name = rtrim($post_cat->name, "s");
        $cat_name = $post_cat->name;
        $cat_title = 'Alles over '.$cat_name;
        $cat_link = get_term_link( $post_cat, $tax );

        echo '<a class="tax__link rollout" href="'.$cat_link.'" rel="nofollow">';
        echo '<span class="rollout-visible">'.$cat_name.'</span>';
          echo '<span class="rollout-hidden">bekijk alles</span>';
        echo '</a>';
      }
    }
  }
}

if ( ! function_exists( 'hum_post_type' ) ) {

  function hum_post_type( $link = false ) {

    $get_post_type = get_post_type();
    if ( is_front_page() ) {
      return;
    }

    if ( $get_post_type == 'page' ) {
      // page - show parent name
      $has_parent = wp_get_post_parent_id( get_the_id() );
      $obj = get_post();

      if ( $has_parent ) {
        $obj_parent = get_post( $has_parent );
        $page_parent_link = get_permalink( $obj_parent );

        //echo '<div class="tax__label">'.$obj_parent->post_name.'</div>';
        echo '<a class="tax__link tax__label button--w" href="'.$page_parent_link.'" rel="nofollow">'.$obj_parent->post_name.'</a>';
      }


    } else {
      // no page - show post type
      if ( !empty( $get_post_type ) && !is_wp_error( $get_post_type ) ) {

        $obj = get_post_type_object( $get_post_type );
        $post_type_name = $obj->labels->singular_name;

        $post_arch_link = get_post_type_archive_link( $get_post_type );

        if ( $link ) {
          echo '<a class="tax__link button--w" href="'.$post_arch_link.'" rel="nofollow">';
          echo $post_type_name;
          echo '</a>';
        } else {
          echo '<div class="tax__label">'.$post_type_name.'</div>';
        }

      }

    }

  }
}

if ( ! function_exists( 'hum_type_rollout' ) ) {

  function hum_type_rollout() {

    $get_post_type = get_post_type();

    if ( !empty( $get_post_type ) && !is_wp_error( $get_post_type ) ) {

      $obj = get_post_type_object( $get_post_type );
      $post_type_name = $obj->labels->singular_name;

      $post_arch_link = get_post_type_archive_link( $get_post_type );

      echo '<a class="tax__link rollout" href="'.$post_arch_link.'" rel="nofollow">';
      echo '<span class="rollout-visible">'.$post_type_name.'</span>';
      echo '<span class="rollout-hidden">bekijk alles</span>';
      echo '</a>';

    }
  }
}
