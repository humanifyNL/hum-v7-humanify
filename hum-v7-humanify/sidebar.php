<?php
/**
 * The sidebar containing the main widget area
 *
 * @package hum-v7-core
 */

if ( is_active_sidebar( 'sidebar-main' ) ) {

  // Accessibility. Aria labelledby adds relationship between the sidebar and its heading.
  ?>
  <aside id="secondary" class="sidebar widget-area" aria-labelledby="sidebar-header" itemscope="itemscope" itemtype="https://schema.org/WPSideBar">

    <h2 class="screen-reader-text" id="sidebar-header"><?php esc_html_e( 'Main Sidebar', 'hum-base' ); ?></h2>

		<?php
 		dynamic_sidebar( 'sidebar-main' ); ?>

  </aside>
  <?php
}
