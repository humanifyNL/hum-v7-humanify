<?php
/**
 * The template for displaying post archive page for jobs
 *
 * @package hum-v7-humanify
 */

get_header();
?>

<div class="wrap-main">

	<section id="primary" class="content-area">

		<main id="main" class="site-main">

			<?php
			get_template_part( 'template-parts/pages/page/header', 'page__archive');

      if ( have_posts() ) {

				?>
        <section class="row row--previews row--previews__rel">

					<div class="block-body wrap wrap--posts">

						<div class="grid--previews grid--docs">

	            <?php

	            while ( have_posts() ) {

	              the_post();
								include( locate_template( 'template-parts/singles/post/preview-post__rel.php' ));

	            }
							wp_reset_postdata();
	            ?>

	          </div>

					</div>

				</section>
        <?php

      }
			?>

		</main>

	</section>

</div>
<?php

get_footer();
