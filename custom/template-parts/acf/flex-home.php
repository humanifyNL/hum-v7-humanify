<?php
/**
 * Flex content - page
 *
 * @package hum-base-v6
 */

if ( have_rows( 'flex_content_home' )) {

  while ( have_rows( 'flex_content_home' )) {

    the_row();


    if ( get_row_layout() == 'form' ) {

      get_template_part( 'template-parts/acf/rows/row--form' );
    }

    if ( get_row_layout() == 'text' ) {

      get_template_part( 'template-parts/acf/rows/row--text-wysi' );
    }

    if ( get_row_layout() == 'text_image' ) {

      get_template_part( 'template-parts/acf/rows/row--text-image' );
    }

    if ( get_row_layout() == 'text_image_slider' ) {

      get_template_part( 'template-parts/acf/rows/row--text-slider' );
    }

    if ( get_row_layout() == 'text_gallery' ) {

      get_template_part( 'template-parts/acf/rows/row--text-gallery' );
    }

    if ( get_row_layout() == 'link_pages' ) {

      get_template_part( 'template-parts/acf/rows/row--pagelinks' );
    }

    if ( get_row_layout() == 'link_posts' ) {

      get_template_part( 'template-parts/acf/rows/row--postlinks' );
    }

    if ( get_row_layout() == 'link_landing' ) {

      get_template_part( 'template-parts/acf/rows/row--pagelinks-landing' );
    }

    if ( get_row_layout() == 'text_uitvoering' ) {

      get_template_part( 'template-parts/acf/rows/row', '-text-uitvoering');
    }

    if ( get_row_layout() == 'text_list' ) {

      get_template_part( 'template-parts/acf/rows/row', '-text-list');
    }

    if ( get_row_layout() == 'products_uitvoering' ) {

      get_template_part( 'template-parts/acf/rows/row', '-product--uit');
    }

    if ( get_row_layout() == 'landing_pages' ) {

      get_template_part( 'template-parts/acf/queries/row', '-product-cats');
    }

    if ( get_row_layout() == 'portfolio_previews' ) {

      get_template_part( 'template-parts/acf/queries/row', '-portfolio');
    }

    if ( get_row_layout() == 'link_posts_two' ) {

      get_template_part( 'template-parts/acf/rows/row--postlinks-two' );
    }
  }

}
