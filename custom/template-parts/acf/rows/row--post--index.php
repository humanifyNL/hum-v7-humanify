<?php
/**
 * Template part for index row
 *
 * @package hum-v7-humanify
 */
?>

<section class="row row--post--side">

  <div class="wrap">

    <div class="grid grid--60">

      <div class="block block--post">

        <?php
        if ( have_rows( 'flex_first_row' ) ) {
          while ( have_rows( 'flex_first_row' ) ) {

            the_row();
            include( locate_template( 'template-parts/acf/rows/row--text-wysi.php') );

          }
        }
        ?>

      </div>

      <div class="block"></div>

      <div class="block block--post">

        <?php
        get_template_part( 'template-parts/acf/flex-post' );
        ?>

      </div>

      <?php
      get_template_part( 'template-parts/site/blocks/block-index--side' );
      ?>

    </div>

  </div>

</section>
