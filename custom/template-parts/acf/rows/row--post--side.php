<?php
/**
 * Template part for index row
 *
 * @package hum-v7-humanify
 */
?>

<section class="row row--post--side">

  <div class="wrap">

    <div class="grid grid--60">

      <div class="block block--post">

        <?php
        get_template_part( 'template-parts/acf/flex-post' );
        ?>

      </div>

      <div class="block block--side block--index <?php if ( has_term( 'wordpress', 'category' ) ) { echo 'reverse-h'; } ?>">

        <?php
        get_template_part( 'template-parts/singles/werkwijze/query-links', 'werkwijze__side' );
        ?>

      </div>

    </div>

  </div>

</section>
