<?php
/**
 * Template part for index row
 *
 * @package hum-v7-humanify
 */
?>

<section class="row row--page--side">

  <div class="wrap">

    <div class="grid grid--60">

      <div class="block block--post">

        <?php
        get_template_part( 'template-parts/acf/flex-page' );
        ?>

      </div>

      <div class="block"></div>

    </div>

  </div>

</section>
