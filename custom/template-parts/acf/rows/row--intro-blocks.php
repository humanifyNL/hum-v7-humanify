<?php
/**
 * Row with three blocks
 *
 * ACF field:
 *
 * @package hum-v7-humanify
 */

$bg_img_url = get_field('blocks_3_img_url');

if ( have_rows( 'about_repeater' ) ) {

  ?>
  <section class="row row--blocks-3 has-bg" <?php if( $bg_img_url ) {?> style="background-position: top; background-size: cover; background-image: url(<?php the_field('blocks_3_img_url'); ?>);" <?php }?>>

    <div class="block-body wrap">

      <div class="grid--previews grid--33">

        <?php

        while ( have_rows( 'about_repeater' ) ) {

          the_row();
          $link = get_sub_field( 'link_page_post' );

          ?>
          <div class="block preview block--panel has-label <?php if( $link ){ echo 'clickable'; }?>">

            <?php
            include( locate_template( 'template-parts/acf/partials/title__icon.php') );

            include( locate_template( 'template-parts/acf/partials/text__pad.php') );

            include( locate_template( 'template-parts/acf/partials/link__label.php') );
            ?>

          </div>
          <?php
        }
        ?>

      </div>

    </div>

  </section>
  <?php
}
