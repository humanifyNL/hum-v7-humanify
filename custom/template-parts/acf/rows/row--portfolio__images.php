<?php
/**
 * Portfolio row with images
 *
 * @package hum-v7-humanify
 */
?>

<section class="row row--images row--portfolio__images">

  <div class="wrap">

    <div class="grid grid--60">

      <?php
      $image_mob = get_field( 'header_img_url' );
      ?>

      <div class="block image-desktop entry-body <?php if ( empty($image_mob) ) { echo 'nomob';} ?>">

        <?php
        // post image
        if ( has_post_thumbnail() ) {
          echo '<div class="post-featured-image">'; the_post_thumbnail( 'large' ); echo '</div>';
        }
        ?>

      </div>

      <div class="block image-mob block-body">

        <?php
        if( !empty( $image_mob ) ) {

          echo '<div class="block__img">';

            // vars
            $img_id = $image_mob['id'];
            $url = $image_mob['url'];
            $size = 'full';

            echo '<a class="block__img--link" href="'.$url.'">';
              echo wp_get_attachment_image( $img_id, $size, '', array( 'class' => 'section-img') );
            echo '</a>';

          echo '</div>';

        }
        ?>

      </div>

    </div>

  </div>

</section>
