<?php
/**
  * Gallery row for post
  *
  * ACF field: group_5f12cd6d417bc
  *
  * @package hum-v7-humanify
  */

$theme = get_sub_field( 'row_theme' );
$images = get_sub_field( 'gallery_post' );
$images = get_field( 'gallery_post' );

if ( $images ) {

  ?>
  <section class="row row--gallery <?php if( $theme ) { echo 'style-1 ';}?>">

    <div class="section-body wrap">

      <?php
      include( locate_template( 'template-parts/acf/partials/gallery__post.php' ) );
      ?>

    </div>

  </section>
  <?php

}
