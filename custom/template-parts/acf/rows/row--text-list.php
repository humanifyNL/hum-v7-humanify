<?php
/**
 * Text section with image
 *
 * ACF field: group_5f088cda4bbf9
 *
 * @package hum-v7-humanify
 */

 $glue = get_sub_field( 'text_section_connect' );
 ?>

 <section class="row row--section text-list <?php echo hum_row_style(); if( $glue ){ echo ' connect '; }?>" <?php hum_row_img(); ?>>

   <div class="wrap">

     <div class="grid <?php echo hum_grid_section(); ?>">

       <div class="block block--text section-body">

         <?php
         include( locate_template( 'template-parts/acf/partials/text__wysi.php') );

         include( locate_template( 'template-parts/acf/partials/link__repeater.php') );
         ?>

       </div>

       <div class="block block--list">

         <?php
         include( locate_template( 'template-parts/acf/partials/title.php') );

         include( locate_template( 'template-parts/acf/partials/list__repeater.php') );

         include( locate_template( 'template-parts/acf/partials/link__repeater-r.php') );

         ?>

       </div>

     </div>

   </div>

 </section>
