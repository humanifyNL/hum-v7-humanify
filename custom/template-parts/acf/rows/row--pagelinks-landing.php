<?php
/**
 * Postlinks - repeater row
 *
 * ACF field: group_5f144e174f531
 *
 * @package hum-v7-core
 */
?>

<section class="row row--previews row--previews--landing products <?php echo hum_row_style(); ?>" <?php hum_row_img(); ?>>

  <div class="wrap">

    <?php
    include( locate_template( 'template-parts/acf/partials/title__row.php') );

    include( locate_template( 'template-parts/acf/partials/pagelinks.php' ) );
    ?>

  </div>

</section>
