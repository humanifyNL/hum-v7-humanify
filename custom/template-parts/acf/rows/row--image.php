<?php
/**
 * Text section with image
 * FIX ROW STYLE
 * ACF field: group_5f088cda4bbf9
 *
 * @package hum-v7-humanify
 */

$glue = get_sub_field( 'text_section_connect' );
$reverse = get_sub_field( 'section_grid_reverse' );
$theme = get_sub_field( 'row_theme' );
$image = get_sub_field( 'image_array' );
?>

<section class="row row--section <?php if( $theme ){ echo ' style-1'; } if( $glue ){ echo ' connect '; } if( isset($row_name) ){ echo $row_name; }?>">

  <div class="section-body wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <div class="block block--text">

        <?php
        include( locate_template( 'template-parts/acf/partials/text__wysi.php') );

        include( locate_template( 'template-parts/acf/partials/link__repeater.php') );
        ?>

      </div>

      <div class="block block--image">

        <?php
        if ( $image ) {

          include( locate_template( 'template-parts/acf/partials/image.php') );
        }
        ?>

      </div>

    <?php
    echo '</div>';
    ?>

  </div>

</section>
