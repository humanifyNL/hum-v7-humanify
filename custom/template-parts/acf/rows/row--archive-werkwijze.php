<?php
/**
 * The template for archive row - werkwijze
 *
 * @package hum-v7-humanify
 */
?>

<section class="row row--previews <?php echo hum_row_style();?>" <?php echo hum_row_img();?>>

  <div class="block-body wrap">

    <div class="grid grid--50">

      <div class="block block--archive">

        <?php
        if ( have_rows( 'services_grp', 'option' ) ) {
          while ( have_rows( 'services_grp', 'option') ) {
            the_row();
            include(locate_template( 'template-parts/acf/partials/title.php' ));
            include(locate_template( 'template-parts/acf/partials/link__repeater-list.php' ));
          }
        }
        ?>

      </div>

      <div class="block block--archive">

        <?php
        get_template_part( 'template-parts/singles/werkwijze/query-links', 'werkwijze__focus');
        ?>

      </div>


      <?php
      $enable_cat_wp = get_field( 'enable_cat_wp', 'option' );
      if ( $enable_cat_wp ) {
        ?>
        <div class="block block--archive">
          <?php
          get_template_part( 'template-parts/singles/werkwijze/query-links', 'werkwijze__wp');
          ?>
        </div>
        <?php
      }
      ?>

    </div>

  </div>

</section>
