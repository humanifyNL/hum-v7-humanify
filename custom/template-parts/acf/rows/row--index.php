<?php
/**
 * Template part for index row
 *
 * @package hum-v7-humanify
 */
?>

<section class="row row--index compact has-bg">

  <div class="wrap">

    <div class="grid grid--60">

      <?php
      get_template_part( 'template-parts/site/blocks/block-index' );
      get_template_part( 'template-parts/site/blocks/block-humanify' );
      ?>

    </div>

  </div>

</section>
