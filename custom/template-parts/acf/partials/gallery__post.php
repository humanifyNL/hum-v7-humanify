<?php
/**
 * Gallery for post
 *
 * ACF field:
 *
 * @package hum-v7-humanify
 */

$images = get_sub_field( 'gallery_post' );
$images = get_field( 'gallery_post' );
$count_img = count($images);
$n = 1;

if ( $images ) {

  ?>
  <div class="block__gallery">

    <div class="grid grid--gallery nowrap <?php echo 'grid-items-'.$count_img; ?>">

      <?php
      // show each image
      foreach ( $images as $image ) {

        echo '<div class="gallery__frame n'. $n++ .'">';

          echo '<a href="'.$image['url'].'">';

            echo '<img class="gallery__img" src="'.$image['sizes']['medium'].'" alt="'.$image['alt'].'" />';

          echo '</a>';

          if ( $image['caption'] ) { echo '<div class="wp-caption">';

            echo '<p class="wp-caption-text">'.$image['caption'].'</p></div>';

          }

        echo '</div>';

      } // endforeach
      ?>

    </div>

  </div>
  <?php
}
