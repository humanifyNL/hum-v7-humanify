<?php
/**
 * Text Tiny MCE
 *
 * ACF field: group_5f087a5128e8a
 *
 * @package hum-v7-core
 */

$text_wysi = get_sub_field( 'text_wysi' );

if ( $text_wysi ) {

  echo '<div class="block__text block__text--intro">';

    echo $text_wysi;

  echo '</div>';

}
