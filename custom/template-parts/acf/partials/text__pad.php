<?php
/**
 * Text partial with padding
 *
 * @package hum-v7-humanify
 */

$text = get_sub_field( 'text' );

if ( $text ) {

  echo '<div class="block__text pad">';

    echo $text;

  echo '</div>';

}
