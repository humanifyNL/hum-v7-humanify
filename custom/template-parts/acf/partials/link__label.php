<?php
/**
 * Link partial
 *
 * ACF field: group_5f087f17ba886
 *
 * @package hum-v7-humanify
 */

$link = get_sub_field( 'link_page_post' );
$link_title = get_sub_field( 'link_title' );

if ( $link ) {

  echo '<div class="block__footer label">';

    echo '<a class="click block__link block__link--label" href="'.esc_url($link).'">';

    if ( $link_title ) {

      echo esc_html($link_title);

    } else {

      echo "Lees meer";

    }

    echo '</a>';

  echo '</div>';
}
