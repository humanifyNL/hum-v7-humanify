<?php
/**
 * Post repeater partial
 *
 * @package hum-v7-humanify
 */

if ( have_rows( 'postlinks_repeater') ) {

  echo '<ul class="block__list list--links">';

    while ( have_rows( 'postlinks_repeater') ) {

      the_row();

      // custom title / descr
      $custom_title = get_sub_field( 'postlinks_custom_title' );

      // setup post type
      $post = get_sub_field( 'postlinks_post_object' );
      setup_postdata( $post );

      if ( !empty($custom_title) ) {
        $post_title = $custom_title;
      } else {
        $post_title = $post->post_title;
      }

      // build link
      echo '<li>';

        echo '<a class="block__link block__link--arrow" href="'; the_permalink(); echo '">'.$post_title.'</a>';

      echo '</li>';

      wp_reset_postdata();
    }


  echo '</ul>';

}
