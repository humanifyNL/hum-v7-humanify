<?php
/**
 * Template part for query of post_learn per focus
 *
 * @package hum-v7-humanify
 */
?>

<section class="row row--previews row--previews--learn">

  <div class="block-body wrap wrap--posts">

    <?php
    include( locate_template( 'template-parts/singles/post_learn/query-learn__focus.php' ) );
    ?>

  </div>

</section>
