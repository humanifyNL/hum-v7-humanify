<?php
/**
 * Query row - portfolio
 *
 * @package hum-v7-humanify
 */
?>
<section class="row row--portfolio style-1">

  <div class="block-body wrap">

    <?php
    include( locate_template( 'template-parts/singles/portfolio/query-portfolio.php' ) );
    ?>

  </div>

</section>
