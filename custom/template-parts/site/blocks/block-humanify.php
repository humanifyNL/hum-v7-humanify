<?php
/**
  * Block humanify (about me)
  *
  * @package hum-v7-humanify
  */
?>

<div class="block block--meta block--humanify">

  <?php
  // vars
  $hum_image = get_field( 'humanify_img_obj', 'option' );
  $hum_link_1 = get_field( 'humanify_link_1', 'option' );
  $hum_linktext_1 = get_field( 'humanify_linktext_1', 'option' );
  $hum_roll_1 = get_field( 'humanify_rollout_1', 'option' );
  $hum_link_2 = get_field( 'humanify_link_2', 'option' );
  $hum_linktext_2 = get_field( 'humanify_linktext_2', 'option' );
  $hum_roll_2 = get_field( 'humanify_rollout_2', 'option' );

  if( !empty( $hum_image ) ) {

    echo '<div class="block__img block__img--circle">';

      $img_id = $hum_image['id'];
      $size = 'thumbnail';
      echo wp_get_attachment_image( $img_id, $size, '', array( 'class' => 'portrait-img') );

    echo '</div>';
  }

  if ( $hum_linktext_1 || $hum_linktext_2 ) {

    echo '<div class="block__author">';

      echo '<p class="block__title name">Robbert van Hooij';
      echo '<a href="mailto:robbert@humanify.nl" class="block__link email">E-mail</a>';
      echo '</p>';

      if ( $hum_linktext_1 && !$hum_link_1 ) {

        echo '<p>'.$hum_linktext_1.'</p>';

      } elseif ( $hum_linktext_1 && $hum_link_1 ) {

        echo '<a class="block__link block__link--white rollout" href="'.$hum_link_1.'">';
        echo '<span class="rollout-visible">'.$hum_linktext_1.'</span>';
        if ( $hum_roll_1 ) {
          echo '<span class="rollout-hidden">'.$hum_roll_1.'</span>';
        }
        echo '</a>';

      }

      if ( $hum_linktext_2 && !$hum_link_2 ) {

        echo '<p>'.$hum_linktext_2.'</p>';

      } elseif ( $hum_linktext_2 && $hum_link_2 ) {

        echo '<a class="block__link block__link--white rollout" href="'.$hum_link_2.'">';
        echo '<span class="rollout-visible">'.$hum_linktext_2.'</span>';
        if ( $hum_roll_2 ) {
          echo '<span class="rollout-hidden">'.$hum_roll_2.'</span>';
        } elseif ( $hum_roll_1 ) {
          echo '<span class="rollout-hidden">'.$hum_roll_1.'</span>';
        }
        echo '</a>';

      }

    echo '</div>';
  }
  ?>

</div>
