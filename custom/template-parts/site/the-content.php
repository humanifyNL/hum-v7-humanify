<?php
/**
 * Page content WP tinyMCE
 *
 * @package hum-v7-humanify
 */

if ( !empty( get_the_content() ) ) {

  ?>
  <section class="row row--content">

    <div class="wrap">

      <?php
      if ( get_post_type( get_the_ID() ) == 'post_learn' || get_post_type( get_the_ID() ) == 'post' ) {
        get_template_part( 'template-parts/site/share-dialog');
      }
      ?>

      <div class="post-body">

        <?php
        the_content();
        ?>

      </div>

      <?php
      if ( get_post_type( get_the_ID() ) == 'post_docs' ) {
        $download_url = get_field( 'doc_pdf' );
        echo '<a href="'.$download_url.'" class="btn button--wired click download">Download PDF</a>';
      }

      if ( get_post_type( get_the_ID() ) == 'post_learn' || get_post_type( get_the_ID() ) == 'post' ) {
        get_template_part( 'template-parts/site/share-dialog-bot');
      }

      ?>

    </div>


  </section>

  <?php
}
