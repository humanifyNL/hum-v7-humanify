<?php
/**
 * Footer float bar
 *
 * @package hum-v7-core
 */
?>

<div class="footer footer--float">

  <div class="wrap wrap--footer">

    <div class="grid">

      <div class="block block--float">

        <a class="btn btn-flat button icon--float" id="totop">top</a>

      </div>

    </div>

  </div>

</div>
