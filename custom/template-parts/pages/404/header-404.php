<?php
/**
 * Template part for standard header
 *
 * @package hum-v7-humanify
 */
?>

<header class="page-header">

  <div class="wrap">

    <div class="grid grid--60">

      <div class="block block--title">

        <?php
        // title
        echo '<h1 class="page-title">'; esc_html_e( 'Deze pagina URL bestaat niet.', 'hum-base' ); echo '</h1>';

        echo '<div class="block__intro"><p>';

          esc_html_e( 'Dit is natuurlijk niet de bedoeling, maar het kan een keer gebeuren. Het kan zijn dat de pagina verwijderd is, of dat de link is veranderd. Hier wat opties om je verder te helpen.', 'hum-base' );

        echo '</p></div>';

        get_search_form();
        ?>

      </div>

      <div class="block"></div>

    </div>

  </div>

</header>
