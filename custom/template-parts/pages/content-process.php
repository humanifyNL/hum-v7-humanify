<?php
/**
 * Page-learn content
 *
 * @package hum-v7-humanify
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/pages/page/header', 'page' );
	?>

	<div class="page-content">

		<?php
		get_template_part( 'template-parts/acf/flex-page' );

		if ( get_field( 'enable_pagelinks_siblings', 'option') ) {
			get_template_part( 'template-parts/acf/queries/row--page-siblings');
		}
		?>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
