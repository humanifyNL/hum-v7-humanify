<?php
/**
 * Template part for standard header
 *
 * @package hum-v7-humanify
 */
?>

<header class="page-header">

  <div class="wrap">

    <div class="grid grid--60">

      <div class="block block--title">

        <?php
        // title
        echo '<h1 class="page-title">';
         printf( esc_html__( 'Gezocht naar %s', 'hum-base' ), '<span>"' . get_search_query() . '"</span>' );
        echo '</h1>';

        get_search_form();
        ?>

      </div>

      <div class="block"></div>

    </div>

  </div>

</header>
