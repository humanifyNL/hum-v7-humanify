<?php
/**
 * The template part for displaying results in search pages
 *
 * @package hum-v7-humanify
 */

$post_type = get_post_type();
$post_type_s = rtrim($post_type, "s");
?>

<article id="post-<?php the_ID(); ?>" class="preview preview--search has-label clickable">

	<header class="block__header">

		<?php
		the_title( '<h2 class="block__title">', '</h2>' );
		echo '<div class="block__type">'.$post_type_s.'</div>';
		?>

	</header>

	<div class="block__text">

		<?php the_excerpt(); ?>

	</div>

	<div class="block__meta">

		<?php get_template_part( 'partial-templates/meta', 'bottom' ); ?>

	</div>


	<footer class="block__footer label">

		<?php
		echo '<a class="block__link block__link--label click" href="' . esc_url( get_permalink() ) . '" rel="bookmark">Ga naar</a>';
		?>

	</footer>



</article>
