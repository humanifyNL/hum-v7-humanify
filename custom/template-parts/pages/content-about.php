<?php
/**
 * About content
 *
 * @package hum-v7-humanify
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/pages/page/header', 'page' );
	?>

	<div class="page-content">

		<?php
		// content
		// get_template_part( 'template-parts/acf/rows/row', '-intro-blocks' );

		get_template_part( 'template-parts/acf/flex-page' );

		get_template_part( 'template-parts/pages/page/footer', 'page' );
		?>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
