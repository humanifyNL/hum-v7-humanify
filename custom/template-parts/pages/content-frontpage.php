<?php
/**
 * Front Page content for front-page.php.
 *
 * @package hum-v7-humanify
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	// page header
	get_template_part( 'template-parts/pages/page/header', 'page');
	?>

	<div class="page-content">

		<?php
		get_template_part( 'template-parts/acf/flex', 'home');
		?>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
