<?php
/**
 * Page-learn content
 *
 * @package hum-v7-humanify
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/pages/page/header', 'page__landing' );
	?>

	<div class="page-content">

		<?php
		get_template_part( 'template-parts/acf/queries/row', '-learn__iso' );
		?>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
