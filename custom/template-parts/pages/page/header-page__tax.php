<?php
/**
 * Template part for page-header of taxonomy page
 *
 * @package hum-v7-humanify
 */

$term_obj = get_queried_object();
$term_title = get_field( 'cat_title', $term_obj );
$term_subtitle = get_field( 'cat_subtitle', $term_obj );
$term_descr = get_field( 'cat_intro_text', $term_obj );
$term_descr_2 = get_field( 'cat_more_text', $term_obj );
?>

<header class="page-header page-header--archive style-1">

  <div class="wrap">

    <div class="grid grid--60">

      <div class="block block--title">

        <?php
        // title
        if ( is_tax( 'cat_focus' ) || is_tax( 'tag_onderwerp' ) ) {

          if ( !empty($term_title) ) {
            echo '<h1 class="page-title">'.$term_title.'</h1>';
          }
          if ( !empty($term_subtitle) ) {
            echo '<h3 class="page-subtitle">'.$term_subtitle.'</h3>';
          }

        } else {
          echo '<h1 class="page-title">'; single_term_title(); echo '</h1>';
        }

        // text
        if( !empty($term_descr) ) {

          echo '<div class="block__text">'.$term_descr.'</div>';

        }
        ?>

      </div>

      <div class="block block--meta"></div>

    </div>

  </div>

</header>
