<?php
/**
 * Template part used for displaying page sibling links.
 *
 * @package hum-v7-humanify
 */

$parent_id = get_field( 'page_about_main', 'option' );
$block_title = get_field( 'about_title', 'option');

$child_pages = get_pages( array(

	'child_of' => $parent_id,
	'exclude' => array( get_the_id() ),
	'sort_column' => 'menu_order',

) );

if ( !empty($child_pages) ) {

	?><!-- query-links-page-siblings -->

	<?php
	if ( $block_title ) {
		echo '<h3 class="block__title">'.$block_title.'</h3>';
	}
	?>

	<ul class="block__list list--links">

		<?php
	  foreach ($child_pages as $page) {

		  setup_postdata( $page );
			$page_link = get_page_link( $page->ID );
			$page_title = get_field( 'post_title_short' , $page->ID );
			if ( !$page_title ) {
				$page_title = $page->post_title;
			}

			echo '<li>';

				echo '<a class="block__link block__link--icon" href="'.$page_link.'">'.$page_title.'</a>';

	    echo '</li>';

  	}
		wp_reset_postdata();
		?>

	</ul>
	<?php
}
