<?php
/**
 * Template part for standard header
 *
 * @package hum-v7-humanify
 */
?>

<header class="page-header style-1">

  <div class="wrap">

    <div class="grid grid--60">

      <div class="block block--title">

        <?php
        // title
        the_title( '<h1 class="page-title">', '</h1>' );
        echo '<link href="'; the_permalink(); echo'"/>';

        if ( !post_password_required( $post ) ) {

          $page_subtitle = get_field( 'page_subtitle' );
          if ( !empty($page_subtitle) ) {

            echo '<h3 class="page-subtitle">'.$page_subtitle.'</h3>';
          }

          if ( have_rows( 'page_intro_group' ) ) {
            while ( have_rows( 'page_intro_group' ) ) {

              the_row();
              include( locate_template( 'template-parts/acf/partials/text.php') );
              include( locate_template( 'template-parts/acf/partials/link__repeater.php') );

            }
          }
        }
        ?>

      </div>

      <?php
      if ( is_front_page() ) {

        get_template_part( 'template-parts/site/blocks/block-humanify' );

      } else {

        ?>
        <div class="block block--meta">

          <?php
          $image = get_field( 'header_img_url' );

          if( !empty( $image ) ) {

            echo '<div class="block__img">';
            // vars
            $img_id = $image['id'];
            $alt = $image['alt'];
            $caption = $image['caption'];
            $size = 'medium';

            echo wp_get_attachment_image( $img_id, $size, '', array( 'class' => 'section-img') );

            echo '</div>';
          }
          ?>

        </div>
        <?php
      }
      ?>

    </div>

  </div>

</header>
