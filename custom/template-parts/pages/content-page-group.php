<?php
/**
 * Page group content
 *
 * @package hum-v7-core
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/pages/page/header', 'page' );
	?>

	<div class="page-content">

		<?php
   	// content
		get_template_part( 'template-parts/acf/flex-page' );

		if ( have_rows ( 'template_page_group' ) ) {
			while ( have_rows ( 'template_page_group' ) ) {

				the_row();
				include( locate_template( 'template-parts/acf/queries/row--page-children__custom.php') );
			}
		}

		/*
		if ( is_page() && $post->post_parent ) {
    // This is a subpage

		} else {
		    // This is not a subpage
		}
		*/
		?>

		</section>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
