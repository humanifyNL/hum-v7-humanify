<?php
/**
 * The template for displaying post_learn content
 *
 * @package hum-v7-humanify
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <?php
  get_template_part( 'template-parts/singles/post_learn/header', 'learn' );
  ?>

  <section class="post-content">

		<div class="wrap--post">

      <?php
      // content
      if ( have_rows( 'flex_content_post' ) ) {

        get_template_part( 'template-parts/acf/flex-post' );

      } else {

        get_template_part( 'template-parts/site/the-content' );

      }
      ?>
      <div class="row row--meta">

        <div class="wrap">
          <?php get_template_part( 'template-parts/singles/post_learn/meta', 'learn--bot' );?>
        </div>

      </div>

      <?php
      get_template_part( 'template-parts/acf/queries/row', '-learn__rel' );
      ?>

		</div>

	</section>


</article><!-- #post-<?php the_ID(); ?> -->

<?php
get_template_part( 'template-parts/singles/post/footer', 'post' );
?>
