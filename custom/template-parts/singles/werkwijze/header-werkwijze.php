<?php
/**
 * Template part for header - post
 *
 * @package hum-v7-humanify
 */
?>

<header class="page-header post-header page-header--archive style-1">

  <div class="wrap">

    <div class="grid grid--60">

      <div class="block block--title">

        <?php
        // title
        echo '<div class="block__meta">';
        hum_post_type();
        //hum_cats_cp_rollout( 'category' );
        echo '</div>';

        the_title( '<h1 class="page-title">', '</h1>' );
        echo '<link href="'; the_permalink(); echo'"/>';

        $page_subtitle = get_field( 'post_title_short' );
        if ( !empty($page_subtitle) ) {
          echo '<p class="undertext">Want '.$page_subtitle.'</p>';
        }

        if ( have_rows( 'page_intro_group' ) ) {
          while ( have_rows( 'page_intro_group' ) ) {

            the_row();
            include( locate_template( 'template-parts/acf/partials/text.php') );
            include( locate_template( 'template-parts/acf/partials/link__repeater.php') );

          }
        }
        ?>

      </div>

      <div class="block block--meta">

        <?php
        echo '<div class="block__meta block__meta--pub">';

          hum_meta_published();

        echo '</div>';

        echo '<div class="block__meta block__meta--tax">';

          hum_meta_cp();

        echo '</div>';
        ?>

      </div>

    </div>

  </div>

</header>
