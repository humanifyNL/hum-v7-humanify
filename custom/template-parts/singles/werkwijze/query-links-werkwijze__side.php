<?php
/**
 * Template part for query of product links sorted by cat_focus
 *
 * @package hum-v7-humanify
 */
?>
<!--query__links__werkwijze__side-->

<div class="block__sticky">

  <?php
  $current_post_id = get_the_id();

  $query_per_cat = new WP_Query;

  $query_per_cat->query( array(
    'post_type' => array( 'werkwijze' ),
    'meta_key' => 'acf_post_order',
    'orderby' => 'meta_value_num',
    'order' => 'ASC',

    'tax_query' => array(
      'relation' => 'AND',
      array(
        'taxonomy' => 'category',
        'field' => 'slug',
        'terms' => array( 'focus', 'wordpress' ),
        'operator' => 'IN'
      ),
    ),
  ));


  if ( $query_per_cat->have_posts() ) {

    ?>
    <div class="block__frame">

      <?php
      $row_title = 'Kijk verder'; //$cat->name;
      if ( $row_title ) { echo '<h4 class="block__title">'.$row_title.'</h4>'; }
      ?>

      <ul class="block__list list--links">

        <?php
        while ( $query_per_cat->have_posts() ) {

          $query_per_cat->the_post();
          $prod_url = get_permalink();
          $post_id = get_the_id();

          $short_title = get_field( 'post_title_index' );
          if ( $short_title ) {
            $title = $short_title;
          } else {
            $title = the_title( '', '', false );
          }

          if ( has_term( 'focus','category' ) ) {

            echo '<li class="block__item">';

            echo '<a class="block__link block__link--icon is-after'; if ( $post_id == $current_post_id ) { echo ' is-active'; } echo '" href="'.$prod_url.'">'.$title.'</a>';

            echo '</li>';
          }
        }
        wp_reset_postdata();
        ?>

      </ul>

    </div>

    <?php
    $enable_cat_wp = get_field( 'enable_cat_wp_side', 'option' );
    if ( $enable_cat_wp ) {

      ?>
      <div class="block__frame">

        <?php
        $row_title = 'Meer Wordpress'; //$cat->name;
        if ( $row_title ) { echo '<h4 class="block__title">'.$row_title.'</h4>'; }
        ?>

        <ul class="block__list list--links">

          <?php
          while ( $query_per_cat->have_posts() ) {

            $query_per_cat->the_post();
            $prod_url = get_permalink();

            $short_title = get_field( 'post_title_index' );
            if ( $short_title ) {
              $title = $short_title;
            } else {
              $title = the_title( '', '', false );
            }

            if ( has_term( 'wordpress','category' ) ) {

              echo '<li class="block__item">';
              echo '<a class="block__link block__link--icon is-after product" href="'.$prod_url.'">'.$title.'</a>';
              echo '</li>';
            }
          }
          wp_reset_postdata();
          ?>

        </ul>

      </div>
      <?php

    }
  }
  ?>

</div>
