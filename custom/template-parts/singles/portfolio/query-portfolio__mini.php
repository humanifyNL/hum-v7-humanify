<?php
/**
 * Template part for query of portfolio posts
 * // fix grid
 * @package hum-v7-humanify
 */

$args_pf = array(
  'post_type' => array( 'portfolio' ),
  'posts_per_page' => -1,
  'orderby' => 'date',
  'order' => 'DESC',
  'tax_query' => array(
    'relation' => 'AND',
    array(
      'taxonomy' => 'tag_types',
      'field' => 'slug',
      'terms' => array( 'mini' ),
      'operator' => 'IN',
    ),
  ),
);

$query_pf = new WP_query ( $args_pf );

if ( $query_pf->have_posts() ) {

  ?><!--query-portfolio__mini-->

  <section class="row row--portfolio--mini">

    <div class="block-body wrap">

      <?php
      // set title
      $rel_title = 'Oudere projecten';
      echo '<h2 class="row__title">'.$rel_title.'</a></h2>';
      ?>

      <div class="grid--previews grid--50 grid--portfolio">

        <?php
        while ( $query_pf->have_posts() ) {

          $query_pf->the_post();

          include( locate_template( 'template-parts/singles/portfolio/preview-portfolio__mini.php' ) );

        }

        wp_reset_postdata();
        ?>

      </div>

    </div>

  </section>
  <?php // end of if (query->have posts)

}
