<?php
/**
 * Template part for related products on portfolio page
 *
 * @package hum-v7-humanify
 */

$build = get_field( 'portfolio_build', $post->ID );
$optimize = get_field( 'portfolio_optimize', $post->ID );
$maintain = get_field( 'portfolio_maintain', $post->ID );
$method = get_field( 'portfolio_method', $post->ID );

$designer = get_field( 'portfolio_design', $post->ID );
$design_link = get_field( 'portfolio_design_link', $post->ID );


if ( $method ) {

  echo '<section class="portfolio-products">';

    echo '<div class="tax tax--prod">';

      echo '<div class="tax__label">Uitvoering:</div>';

      echo '<div class="tax__bg">';

        $m_name = $method->post_title;
        $m_title = 'Ga naar '. $m_name;
        $m_link = get_permalink($method);
        $m_slug = $method->post_name;

        echo '<a class="btn no-link button--w tax__link tax__link--method '.$m_slug.'">'.$m_name.'</a>';

      echo '</div>';

    echo '</div>';


    if ( $build ) {

      echo '<div class="tax tax--prod">';

        echo '<div class="tax__label">Build:</div>';

        echo '<div class="tax__bg">';

          foreach( $build as $b_product ) {

            $p_name = $b_product->post_title;
            $p_title = 'Ga naar '. $p_name;
            $p_link = get_permalink($b_product);
            $p_slug = $b_product->post_name;
            $build = get_the_terms( $b_product, 'cat_focus' );

            echo '<a class="btn no-link button--w tax__link tax__link--prod '.$p_slug.'">'.$p_name.'</a>';
          }

        echo '</div>';

      echo '</div>';
    }

    if ( $optimize ) {

      echo '<div class="tax tax--prod">';

        echo '<div class="tax__label">Optimize:</div>';

        echo '<div class="tax__bg">';

          foreach( $optimize as $o_product ) {

            $p_name = $o_product->post_title;
            $p_title = 'Ga naar '. $p_name;
            $p_link = get_permalink($o_product);
            $p_slug = $o_product->post_name;
            $build = get_the_terms( $o_product, 'cat_focus' );

            echo '<a class="btn no-link button--w tax__link tax__link--prod '.$p_slug.'">'.$p_name.'</a>';
          }

        echo '</div>';

      echo '</div>';
    }


    if ( $maintain ) {

      echo '<div class="tax tax--prod">';

        echo '<div class="tax__label">Maintain:</div>';

        echo '<div class="tax__bg">';

          foreach( $maintain as $m_product ) {

            $p_name = $m_product->post_title;
            $p_title = 'Ga naar '. $p_name;
            $p_link = get_permalink($m_product);
            $p_slug = $m_product->post_name;
            $build = get_the_terms( $m_product, 'cat_focus' );

            echo '<a class="btn no-link button--w tax__link tax__link--prod '.$p_slug.'">'.$p_name.'</a>';
          }

        echo '</div>';

      echo '</div>';
    }


    if ( $designer || $design_link ) {

      echo '<div class="tax tax--prod design">';

      echo '<div class="tax__label">Design:</div>';

        echo '<div class="tax__bg">';

        if ( empty( $design_link ) ) {

          echo '<span class="btn button--w tax__link tax__link--method no-link">'.$designer.'</span>';

        } elseif ( !empty( $design_link ) ) {

          $d_link = $design_link['url'];
          $d_name = $design_link['title'];
          if (empty($d_name)){ $d_name = $d_link; } else { $d_name = $d_link; }
          $d_title = 'Naar de website van '. $designer;
          $d_target = $design_link['target'] ? $design_link['target'] : '_self';

          echo '<a class="btn button--w tax__link tax__link--method" href="'.$d_link.'" rel="nofollow" target="'.$d_target.'" title="'.$d_title.'">'.$d_name.'</a>';
        }


        echo '</div>';

      echo '</div>';
    }


  echo '</section>';

}
