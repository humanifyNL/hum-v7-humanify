<?php
/**
 * Template part for query of portfolio posts
 * // fix grid
 * @package hum-v7-humanify
 */

$args_pf = array(
  'post_type' => array( 'portfolio' ),
  'posts_per_page' => -1,
  'orderby' => 'date',
  'order' => 'DESC',
  'tax_query' => array(
    'relation' => 'AND',
    array(
      'taxonomy' => 'tag_types',
      'field' => 'slug',
      'terms' => array( 'mini' ),
      'operator' => 'NOT IN',
    ),
  ),
);

$query_pf = new WP_query ( $args_pf );

if ( $query_pf->have_posts() ) {

  ?><!--query-portfolio-->

  <?php
  // set title
  $rel_title = get_field( 'grid_title_portfolio', 'option');
  $pf_archive_url = get_post_type_archive_link( 'portfolio' );

  if ( $rel_title ) {
    echo '<h2 class="row__title"><a href="'.$pf_archive_url.'">'.$rel_title.'</a></h2>';
  }
  ?>

  <div class="grid--previews grid--50">

    <?php
    while ( $query_pf->have_posts() ) {

      $query_pf->the_post();

      include( locate_template( 'template-parts/singles/portfolio/preview-portfolio.php' ) );

    }

    wp_reset_postdata();
    ?>

  </div>


  <?php

}
