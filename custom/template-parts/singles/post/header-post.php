<?php
/**
 * Template part for header - post
 *
 * @package hum-v7-humanify
 */
?>

<header class="page-header post-header style-1">

  <div class="block-body wrap wrap--post">

    <div class="grid grid--75">

      <div class="block">

        <?php
        // title
        the_title( '<h1 class="page-title">', '</h1>' );
        echo '<link href="'; the_permalink(); echo'"/>';

        // excerpt
        hum_excerpt_single();
        ?>

      </div>

      <div class="block block--meta">

        <?php
        echo '<div class="block__meta block__meta--tax">';
          hum_cats_cp_rollout();
        echo '</div>';

        echo '<div class="block__meta block__meta--pub">';
          hum_meta_published();
        echo '</div>';
        ?>

      </div>

    </div>

  </div>

  <?php
  /*
  // post image
  if ( has_post_thumbnail() ) {
    echo '<div class="post-featured-image wrap">'; the_post_thumbnail( 'featured' ); echo '</div>';
  }*/
  ?>

</header>
