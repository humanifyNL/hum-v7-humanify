<?php
/**
 * Template part used for displaying links in loops
 *
 * @package hum-v7-humanify
 */

?>
<article id="post-<?php the_id();?>" class="clickable block block--link">

    <?php
    echo '<a href="'; the_permalink(); echo '" class="click btn button--alt">';
      the_title();
    echo '</a>';
    ?>

</article>
