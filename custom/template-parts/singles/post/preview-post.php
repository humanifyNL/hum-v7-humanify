<?php
/**
 * Template part used for displaying post frames in loop
 *
 * @package hum-v7-humanify
 */

?>
<article id="post-<?php the_id();?>" class="clickable preview preview--post has-label <?php hum_iso_class('category');?>">

  <?php
  // thumbnail
  if ( has_post_thumbnail( $post->ID ) ) {

    echo '<div class="block__thumb">';

      the_post_thumbnail('medium');

    echo '</div>';
  }

  // meta
  if( has_category() && !is_archive() ) {

    echo '<div class="block__meta">';
      hum_meta_cp();
    echo '</div>';
  }

  // title
  echo '<h3 class="block__title block__title--icon pad">'; the_title(); echo '</h3>';

  // excerpt
  hum_excerpt( 'block__text is-excerpt pad' );

  // link
  echo '<div class="block__footer label">';
    echo '<a href="'; the_permalink(); echo '" class="'. hum_button_class( 'post' ). '">'.$link_title.'</a>';
  echo '</div>';
  ?>

</article>
