<?php
/**
 * Template part used for displaying related post frames in loop
 *
 * @package hum-v7-humanify
 */

?>
<article id="post-<?php the_id();?>" class="clickable preview preview--post preview--post__rel">

  <?php
  // thumbnail
  if ( has_post_thumbnail( $post->ID ) ) {

    echo '<div class="block__thumb">';

      the_post_thumbnail('medium');

    echo '</div>';
  }
  ?>

  <div class="block__body">

    <h3 class="block__title block__title--icon">
      <a href="<?php the_permalink();?>" class="click"><?php the_title() ?></a>
    </h3>

    <?php
    hum_excerpt();
    ?>

  </div>

</article>
