<?php
/**
 * Template part for document post type
 *
 * @package hum-v7-humanify
 */
?>

<article id="doc-<?php the_ID(); ?>" <?php post_class(); ?>>


	<?php
	get_template_part( 'template-parts/singles/post_docs/header', 'doc' );
	?>

	<section class="post-content">

		<div class="wrap--post">

	   	<?php
			get_template_part( 'template-parts/site/the-content' );

			get_template_part( 'template-parts/acf/queries/row', '-docs__rel' );
			?>

	  </div>

	</section>

</article><!-- #post-<?php the_ID(); ?> -->
