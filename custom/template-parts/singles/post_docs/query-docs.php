<?php
/**
 * Template part for query of document posts
 *
 * @package hum-v7-humanify
 */

$args_pf = array(
  'post_type' => array( 'post_docs' ),
  'posts_per_page' => -1,
  'orderby' => 'date',
  'order' => 'DESC',
);


$query_pf = new WP_query ( $args_pf );

if ( $query_pf->have_posts() ) {

  ?><!--query-docs-->

  <?php
  // set title
  $rel_title = get_field( 'grid_title_docs', 'option');
  $rel_title_single = get_field( 'related_title_docs', 'option' );
  $pf_archive_url = get_post_type_archive_link( 'post_docs' );

  if ( $rel_title ) {
    echo '<h2 class="row__title"><a href="'.$pf_archive_url.'">'.$rel_title.'</a></h2>';
  }
  ?>

  <div class="grid--previews grid--50 grid--docs">

    <?php
    while ( $query_pf->have_posts() ) {

      $query_pf->the_post();
      include( locate_template( 'template-parts/singles/post_docs/preview-doc.php' ) );

    }

    wp_reset_postdata();
    ?>

  </div>

  <?php

}
