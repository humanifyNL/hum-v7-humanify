<?php
/**
 * Template part for header - post_docs
 *
 * @package hum-v7-humanify
 */
?>

<header class="page-header post-header post-header--docs style-1">

  <div class="wrap wrap--post">

    <div class="grid grid--75">

      <div class="block block--title">

        <?php
        // title
        the_title( '<h1 class="page-title">', '</h1>' );
        echo '<link href="'; the_permalink(); echo'"/>';
        ?>

        <?php
        hum_excerpt_single();

        $download_url = get_field( 'doc_pdf' );
        echo '<a href="'.$download_url.'" class="btn button--w click">Download PDF</a>';
        ?>

      </div>

      <div class="block block--meta block--meta--top">

        <?php
        echo '<div class="block__meta block__meta--tax">';

          hum_type_rollout();

        echo '</div>';

        echo '<div class="block__meta block__meta--pub">';

          hum_meta_published();

        echo '</div>';

        ?>

      </div>

    </div>

  </div>

</header>
