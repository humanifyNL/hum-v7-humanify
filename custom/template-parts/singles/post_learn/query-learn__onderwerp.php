<?php
/**
 * Query posts_learn
 *
 * @package hum-v7-humanify
 */


if ( have_posts() ) {

  ?><!-- query-learn_onderwerp -->

  <?php
  $enable_iso = false;
  ?>

  <div class="grid--previews grid--50">

    <?php
    while ( have_posts() ) {

      the_post();
      include( locate_template( 'template-parts/singles/post_learn/preview-learn__iso-tags.php' ));

    }
    wp_reset_postdata();
    ?>

  </div>

  <?php
}
