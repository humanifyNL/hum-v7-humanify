<?php
/**
 * Meta tags top - post_learn
 *
 * @package hum-v7-core
 */
?>

<div class="block block--meta block--meta--top">

  <?php
  echo '<div class="block__meta block__meta--tax">';
    hum_cats_cp_rollout( 'tag_onderwerp' );
  echo '</div>';

  echo '<div class="block__meta block__meta--pub">';
    hum_meta_updated();
  echo '</div>';

  echo '<div class="block__meta block__meta--tax">';
    hum_meta_cp();
  echo '</div>';
  ?>

</div>
