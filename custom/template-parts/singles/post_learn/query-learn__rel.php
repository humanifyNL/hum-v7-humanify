<?php
/**
 * Template part for query of post_learn related posts
 *
 * @package hum-v7-humanify
 */

$tag_ids = hum_term_ids( 'post_tag' );

$args_pl = array(
  'post_type' => array( 'post_learn' ),
  'post__not_in' => array( get_the_id() ),
  'orderby' => 'date',
  'order' => 'DESC',
  'tax_query' => array(
    'relation' => 'AND',
    array(
      'taxonomy' => 'post_tag',
      'field' => 'id',
      'terms' => $tag_ids,
      'operator' => 'IN'
    ),
  ),
);


$query_pl = new WP_query ( $args_pl );

if ( $query_pl->have_posts() ) {

  ?><!--query-learn__rel-->

  <?php
  if ( $query_pl->have_posts() ) {

    // set title
    $tag_names = hum_term_names( 'post_tag' );
    $rel_tags_title = implode( ', ', $tag_names );
    $archive_url = get_post_type_archive_link( 'post_learn' );

    $rel_tags_sub = 'Meer over '.$rel_tags_title;

    echo '<h2 class="row__title row__title--theme">'.$rel_tags_sub.'</h2>';
    ?>

    <div class="grid--previews related">

      <?php
      while ( $query_pl->have_posts() ) {

        $query_pl->the_post();

        include( locate_template( 'template-parts/singles/post/preview-post__rel.php' ) );

      }
      wp_reset_postdata();

      ?>

    </div>

    <?php
  }

}
