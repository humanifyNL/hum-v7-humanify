<?php
/**
 * Query posts_learn
 *
 * @package hum-v7-humanify
 */


if ( have_posts() ) {

  ?><!-- query-learn_focus -->

  <?php
  if ( is_tax( 'cat_focus' ) ) {

    $row_title = 'Hoe zit dat nou eigenljk?';
    echo '<h2 class="row__title">'.$row_title.'</h2>';

    $tax_name = lcfirst( single_term_title( '', false) );
    echo '<div class="row__text"><p>Het is logisch om met vragen te zitten. Een website bouwen is een diepgaand proces, hier lees je meer over hoe dat allemaal zit en wat er nou echt belangrijk is.</p></div>';
  }

  $enable_iso = false;
  ?>

  <div class="grid--previews">

    <?php
    while ( have_posts() ) {

      the_post();
      include( locate_template( 'template-parts/singles/post_learn/preview-learn__iso-tags.php' ));

    }
    wp_reset_postdata();
    ?>

  </div>

  <?php
}
