<?php
/**
 * Template part for query of post_learn posts
 *
 * @package hum-v7-humanify
 */

$cat_terms = get_the_terms( get_the_id(), 'category' );
if ( !empty( $cat_terms ) ) {
  $cat_ids = wp_list_pluck( $cat_terms, 'term_id' );
  $cat_names = wp_list_pluck( $cat_terms, 'name' );
  $n_cats = count($cat_ids);
}

$tag_terms = get_the_terms( get_the_id(), 'post_tag' );
if ( !empty( $tag_terms ) ) {
  $tag_ids = wp_list_pluck( $tag_terms, 'term_id' );
  $tag_names = wp_list_pluck( $tag_terms, 'name' );
  $n_tags = count($tag_ids);
}
?>

<!--query__learn__rel cats-->

<?php
echo '<ul class="block__list list--links">';

  foreach( $cat_terms as $cat_term ) {

    $cat_url = get_term_link( $cat_term );
    $rel_cats_title = strtoupper($cat_term->name);

    echo '<li class="block block--link block--link__foot">';

      echo '<a class="block__link block__link--icon" href="'.$cat_url.'">Alles over '.$rel_cats_title.'</a>';

    echo '</li>';

  }

  foreach( $tag_terms as $tag_term ) {

    $tag_url = get_term_link( $tag_term );
    $rel_tags_title = strtoupper($tag_term->name);

    echo '<li>';

      echo '<a class="block__link block__link--icon" href="'.$tag_url.'">Alles over '.$rel_tags_title.'</a>';

    echo '</li>';

  }

echo '</ul>';

?>
