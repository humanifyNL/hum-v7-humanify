<?php
/**
 * Template part for header - post_learn
 *
 * @package hum-v7-humanify
 */
?>

<header class="page-header post-header post-header--learn style-1">

  <div class="wrap wrap--post">

    <div class="grid grid--75">

      <div class="block block--title">

        <?php
        // title
        the_title( '<h1 class="page-title">', '</h1>' );
        echo '<link href="'; the_permalink(); echo'"/>';

        // excerpt
        hum_excerpt_single();
        ?>

      </div>

      <?php
      get_template_part( 'template-parts/singles/post_learn/meta', 'learn' );
      ?>

    </div>

  </div>

</header>
