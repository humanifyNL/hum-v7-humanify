<?php
/**
 * Template part for product page
 *
 * @package hum-v7-humanify
 */
?>

<article id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/singles/werkwijze/header', 'werkwijze' );
	?>

	<div class="post-content">

		<?php
		if ( get_field('enable_page_parent') ) {

			// is parent
			get_template_part( 'template-parts/acf/rows/row', '-index' );
			get_template_part( 'template-parts/acf/rows/row', '-post--index' );

		} else {
			// is child
			get_template_part( 'template-parts/acf/rows/row', '-post--side' );

		}
		get_template_part( 'template-parts/acf/flex', 'werkwijze' );

		?>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->

<?php
//get_template_part( 'template-parts/singles/products/footer', 'products');
?>
