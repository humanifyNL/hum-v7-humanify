/**
 * Use post-content titles to make post index links with anchor
 *
 * @package hum-v7-core
 */

jQuery( document ).ready( function($) {

  var i = 0;
  var linkClass = 'class="anchor block__link block__link--icon"';

  $(".post-content .row--section").each(function(i, el){

    var anchorCount = 'anchor-' + i++;
    var title = $(this).find('.block__text > h2').text();

    $(this).find('.block__text > h2').attr('id', anchorCount);

    var titleItem = '<li class="block__item"><a href="#' + anchorCount + '"' + linkClass + '>' + title + '</a></li>';

    $(".js-index .block__list").append(titleItem);
  });

});
