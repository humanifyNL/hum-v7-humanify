/**
 * Add class when scrolling up\
 *
 * @link https://stackoverflow.com/questions/4326845/how-can-i-determine-the-direction-of-a-jquery-scroll-event
 * @package hum-v7-core
 */

jQuery(document).ready(function($) {

  var lastScrollTop = 0;

  $(window).on('load scroll', function() {

    // when scrolled 0.8x past top
    var scrollTop = $(this).scrollTop();
    var bounds = $(window).height() * 0.75;

    // add .visible class
    if ( scrollTop > lastScrollTop ) {

      $('.footer--float').addClass('visible');

    } else {

      $('.footer--float').removeClass('visible');
    }


    // when at the bottom, minus footer height
    var botBounds = scrollTop + $(window).height();
    var footerHeight = $('.site-footer').outerHeight();
    var bot = $(document).height() - footerHeight;

    if ( botBounds >= bot ) {
      $('.footer--float').addClass('at-bottom');
    } else {
      $('.footer--float').removeClass('at-bottom');
    }

  });

});
