<?php
/**
 * Custom template tags for portfolio
 *
 * @package hum-v7-humanify
 */


if ( ! function_exists( 'hum_portfolio_link' ) ) {

  function hum_portfolio_link($class = '', $title = 'Website') {

    $link_array = get_field( 'portfolio_url');

    if( $link_array ) {
      $link_url = $link_array['url'];
      $link_title = $link_array['title'];
      $link_target = $link_array['target'] ? $link_array['target'] : '_self';

      echo '<a class="block__btn'; if( $class ){ echo ' '.$class; } echo '" href="'. esc_url($link_url) .'" target="'. esc_attr($link_target) .'">'; if( $title ) { echo esc_html($title); } echo '</a>';
    }
  }
}


if ( ! function_exists( 'hum_method_name' ) ) {

  function hum_method_name($class = '') {

    $pf_method = get_field( 'portfolio_method' );

    if ( $pf_method ) {

      $pf_method_name = $pf_method->post_title;
      echo '<div class="block__label'; if( $class ){ echo ' '.$class; } echo '">'.$pf_method_name.'</div>';
    }
  }
}

if ( ! function_exists( 'hum_case_type' ) ) {

  function hum_case_type( $tax = 'post_tag', $class = '' ) {

    $get_post_terms = get_the_terms( get_the_id(), $tax );
    if ( !empty( $get_post_terms ) && !is_wp_error( $get_post_terms ) ) {

      foreach ( $get_post_terms as $post_term ) {

        $pf_type_name = $post_term->name;

        echo '<div class="tax__label'; if( $class ){ echo ' '.$class; } echo '">'.$pf_type_name.'</div>';

      }
    }

  }
}
