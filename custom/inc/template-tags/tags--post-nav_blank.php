<?php
/**
 * Custom post nav blank
 *
 * @package hum-v7-humanify
 */

if ( ! function_exists( 'hum_post_nav_blank' ) ) {

  function hum_post_nav_blank() {

    $exclude_term = array( 83 ); // id = 83 on live

    $prev_post = get_previous_post( false , $exclude_term );
    $next_post = get_next_post( false, $exclude_term );

    if ( !empty($prev_post) || !empty($next_post) ) {

      echo '<nav class="navigation post-navigation" role="navigation">';

        echo '<div class="nav-links wrap';
        if ( $prev_post && !$next_post) { echo ' has-prev'; }
        if ( $next_post && !$prev_post) { echo ' has-next'; }
        if ( $next_post && $prev_post) { echo ' has-prev-next'; }
        echo '">';

        if ( !empty( $prev_post ) ) {

          $prev_title = $prev_post->post_title;
          $prev_id = $prev_post->ID;
          $prev_url = get_permalink($prev_id);

          echo '<div class="nav-previous">';

            echo '<a class="block__link" href="'.$prev_url.'" rel="previous" title="Vorige case">';
              // link
              echo '<span class="block__linktext"><span class="small">Vorige case</span>'.$prev_title.'</span>';

            echo '</a>';

          echo '</div>';
        }

        if ( !empty( $next_post ) ) {

          $next_title = $next_post->post_title;
          $next_id = $next_post->ID;
          $next_url = get_permalink($next_id);

          echo '<div class="nav-next">';

            echo '<a class="block__link" href="'.$next_url.'" rel="next" title="Volgende case">';
              // link
              echo '<span class="block__linktext"><span class="small">Volgende case</span>'.$next_title.'</span>';

            echo '</a>';

          echo '</div>';
        }

        echo '</div>';

      echo '</nav>';
    }


  }
}
