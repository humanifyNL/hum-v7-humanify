<?php
/**
 * Hum Base ACF options setup
 *
 * @package hum-v7-core
 */

function hum_base_acf_options() {

	if ( function_exists('acf_add_options_page') ) {

		acf_add_options_page(array(
	    'page_title' 	=> 'Humanify',
	    'menu_title'	=> 'Humanify',
	    'menu_slug' 	=> 'theme-general-settings',
	    'capability'	=> 'edit_posts',
			'icon_url' 		=> 'dashicons-admin-settings',
	    'redirect'		=> false
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Contact defaults',
			'menu_title'	=> 'Contact defaults',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Setup - Header',
			'menu_title'	=> 'Setup - Header',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Setup - Footer',
			'menu_title'	=> 'Setup - Footer',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Setup - Pages',
			'menu_title'	=> 'Setup - Pages',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Setup - Werkwijze',
			'menu_title'	=> 'Setup - Werkwijze',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Setup - Portfolio',
			'menu_title'	=> 'Setup - Portfolio',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Setup - Blog posts',
			'menu_title'	=> 'Setup - Blog posts',
			'parent_slug'	=> 'theme-general-settings',
		));
	}
}
add_action( 'acf/init' , 'hum_base_acf_options' );
