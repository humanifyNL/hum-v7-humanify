<?php
/**
 * Various small tweaks
 *
 * @package hum-v7-core
 */

/**
 * No more jumping for read more link.
 *
 * @link https://digwp.com/2010/01/wordpress-more-tag-tricks/
 */

function hum_core_remove_more_jump_link($link) {

  $offset = strpos($link, '#more-');

 	if ($offset) {
     $end = strpos($link, '"',$offset);
   }

   if ($end) {
     $link = substr_replace($link, '', $offset, $end-$offset);
   }
   return $link;
}

add_filter( 'the_content_more_link', 'hum_core_remove_more_jump_link' );


/**
* Remove curly quotes in WordPress. For more options:
*
* @link http://www.smashingmagazine.com/2013/06/21/five-wordpress-functions-blogging-easier/
*/

remove_filter( 'the_content', 'wptexturize' );
remove_filter( 'the_excerpt', 'wptexturize' );
remove_filter( 'comment_text', 'wptexturize' );


/**
* Completely disable the Post Formats support in the theme and Post Formats UI in the post editor screen.
*
*/
function hum_core_remove_post_formats() {
  remove_theme_support( 'post-formats' );
}
add_action( 'after_setup_theme', 'hum_core_remove_post_formats', 11 );



/**
 * Add meta to head - humans.txt
 */

function hum_core_add_meta_to_head () {

	// We are people, not machines. Read more at: humanstxt.org. Edit file humans.txt to include your information.
	echo "\n"; echo '<!-- Find out who built this website! Read humans.txt for more information. -->'; echo "\n"; echo '<link rel="author" type="text/plain" href="'.get_template_directory_uri().'/inc/humans.txt" />'; echo "\n";

}
add_action( 'wp_head', 'hum_core_add_meta_to_head' );


/**
 * Reverse post comments
 *
 */

if ( ! function_exists ('hum_core_reverse_comments')) {

  function hum_core_reverse_comments($comments) {

    return array_reverse($comments);

  }
}
add_filter ('comments_array', 'hum_core_reverse_comments');



/**
 * Change title for protected and private posts - words "protected" and "private" are replaced by lock symbol.
 *
 */
if ( ! function_exists( 'hum_core_the_title_trim' ) ) {

  // If you're using localized WordPress, replace words 'Protected' and 'Private' with the corresponding words in your language.

	function hum_core_the_title_trim($title) {

		$title = esc_attr($title); // Sanitize HTML characters in the title. Comment out this line if you want to use HTML in post titles.
		$findthese = array(
			'#Beschermd:#',
			'#Private:#'
		);
		$replacewith = array(
			// What to replace "Protected:" with
			'',
			// What to replace "Private:" with
			''
		);
		$title = preg_replace($findthese, $replacewith, $title);
		return $title;

	}
}

add_filter( 'the_title', 'hum_core_the_title_trim' );
