<?php
/**
 * The template for displaying post archive page for jobs
 *
 * @package hum-v7-humanify
 */

get_header();
?>

<div class="wrap-main">

	<section id="primary" class="content-area">

		<main id="main" class="site-main">

			<?php
			get_template_part( 'template-parts/pages/page/header', 'page__archive' );

      if ( have_posts() ) {

				?>
				<div class="page-content">

	        <section class="row row--previews">

						<div class="block-body wrap wrap--posts">

							<?php
							$enable_iso = true;
							hum_get_query_buttons_iso( $wp_query, 'post_tag' );
							?>

							<div class="grid--previews grid--50 grid--iso">

		            <?php

		            while ( have_posts() ) {

		              the_post();
									include( locate_template( 'template-parts/singles/post_learn/preview-learn.php' ));

		            }
								wp_reset_postdata();
		            ?>

		          </div>

						</div>

					</section>

				</div>
        <?php

      }
			?>

		</main>

	</section>

</div>
<?php

get_footer();
