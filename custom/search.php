<?php
/**
 * Search results template
 *
 * @package hum-v7-humanify
 */

get_header();
?>

<div class="wrap-main">

	<section id="primary" class="content-area">

		<main id="main" class="site-main">

			<?php
			get_template_part( 'template-parts/pages/search/header', 'search' );

			if ( have_posts() ) {

				?>
        <section class="row row--previews search">
        	<div class="wrap">
        		<div class="grid">

						<?php
		        while ( have_posts() ) {

		          the_post();
		          get_template_part( 'template-parts/pages/search/preview', 'search' );

		        }

		        hum_archive_page_nav();
						?>

        		</div>
					</div>
				</section>
				<?php

      } else {

				get_template_part( 'template-parts/pages/content', 'noresults' );

      }
			?>

		</main>

	</section>

</div>

<?php
get_footer();
