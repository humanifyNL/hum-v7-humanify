<?php
/**
 * Category template
 *
 * @package hum-v7-humanify
 */

get_header();
?>

<div class="wrap-main">

	<section id="primary" class="content-area">

		<main id="main" class="site-main">

			<?php
			get_template_part( 'template-parts/pages/page/header', 'page__tax' );


			if ( have_posts() ) {

				?>
				<section class="row row--previews row--previews--learn cats">

					<div class="block-body wrap">

						<div class="grid--previews grid--50">

							<?php
							while ( have_posts() ) {

								the_post();

								include( locate_template( 'template-parts/singles/post_learn/preview-learn.php' ) );

							}
							?>

						</div>

					</div>

				</section>
				<?php

			}
			?>

		</main>

	</section>

</div>

<?php
get_footer();
