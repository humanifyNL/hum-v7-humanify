<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package hum-v7-core
 */

get_header();
?>

<div class="wrap-main">

	<div id="primary" class="content-area">

		<main id="main" class="site-main">

			<?php
			get_template_part( 'template-parts/pages/content', '404' );
			?>

		</main>

	</div>

</div>

<?php
get_footer();
