<?php
/**
 * Landing content
 *
 * @package hum-v7-core
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/pages/page/header', 'page' );
	?>

	<div class="page-content">

		<?php
		// content
		get_template_part( 'template-parts/acf/flex-landing' );

		if ( have_rows( 'template_page' ) ) {
			while ( have_rows( 'template_page' ) ) {

				the_row();
				include( locate_template( 'template-parts/acf/queries/row--posts__cat.php' ) );
			}
		}

		get_template_part( 'template-parts/acf/queries/row--landing__siblings' );
		?>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
