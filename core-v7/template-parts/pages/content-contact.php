<?php
/**
 * Contact content
 *
 * @package hum-v7-core
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <?php
  // page header
  get_template_part( 'template-parts/pages/page/header', 'page' );
  ?>

  <div class="page-content">

    <?php
    if ( have_rows( 'contact_row' ) ) {

      while ( have_rows( 'contact_row' ) ) {

        the_row();

        ?>
        <section class="row row--contact">

          <div class="block-body wrap">

            <div class="grid grid--50">

              <div class="block section-body">

                <?php
                include( locate_template( 'template-parts/acf/partials/text__wysi.php') );
                ?>

              </div>

              <?php
              include( locate_template( 'template-parts/pages/contact/block--contact__links.php') );
              ?>

            </div>

          </div>

        </section>
        <?php
      }
    }
    ?>


    <section class="row row--contact company">

      <div class="block-body wrap">

        <div class="grid grid--50">

          <?php
          include( locate_template( 'template-parts/pages/contact/block--contact__address.php') );

          include( locate_template( 'template-parts/pages/contact/block--contact__company.php') );
          ?>

        </div>

      </div>

    </section>


    <section class="row row--contact map">

      <div class="block block--map">

        <?php
        include( locate_template( 'template-parts/acf/partials/map.php' ) );
        ?>

      </div>

    </section>


  </div>

</article><!-- #post-<?php the_ID(); ?> -->
