<?php
/**
 * About content
 *
 * @package hum-v7-core
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/pages/page/header', 'page__sq' );
	?>

	<div class="page-content">

		<?php
		// content
		get_template_part( 'template-parts/acf/flex-about' );

		get_template_part( 'template-parts/acf/queries/row--page-siblings' );
		?>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
