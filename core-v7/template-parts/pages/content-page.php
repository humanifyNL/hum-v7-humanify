<?php
/**
 * Page content
 *
 * @package hum-v7-core
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/pages/page/header', 'page' );
	?>

	<div class="page-content">

		<div class="wrap">

			<div class="post-body">

				<?php
				the_content();
				?>

			</div>
		</div>

		<?php 
		get_template_part( 'template-parts/acf/flex-page' );

		if ( get_field( 'enable_pagelinks_siblings', 'option') ) {
			get_template_part( 'template-parts/acf/queries/row--page-siblings');
		}
		?>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
