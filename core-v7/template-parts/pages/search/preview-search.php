<?php
/**
 * The template part for displaying results in search pages
 *
 * @package hum-v7-core
 */

$post_type = get_post_type();
$post_type_s = rtrim($post_type, "s");
?>

<article id="post-<?php the_ID(); ?>" class="block preview--search clickable">

	<header class="block__header">

		<?php
		the_title( '<h2 class="block__title">', '</h2>' );
		echo '<div class="block__type">'.$post_type_s.'</div>';
		?>


	</header>

	<div class="block__text">

		<?php the_excerpt(); ?>

	</div>

	<div class="block__meta">

		<?php get_template_part( 'template-parts/singles/post/meta', 'post__bot' ); ?>

	</div>


	<footer class="block__footer">

		<?php

		echo '<a class="block__link click" href="' . esc_url( get_permalink() ) . '" rel="bookmark">Ga naar '.$post_type_s.'</a>';
		?>

	</footer>



</article>
