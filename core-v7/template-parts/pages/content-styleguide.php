<?php
/**
 * Content styleguide
 *
 * @package hum-v7-core
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/pages/page/header', 'page' );
	?>

	<div class="page-content">

		<section class="row row--section guide">

			<div class="block-body wrap">

				<div class="grid">

					<div class="block block--colors">

						<div class="color color-primary"></div>
						<div class="color color-secondary"></div>
						<div class="color color-alt-white"></div>
						<div class="color color-grey"></div>
						<div class="color color-black"></div>
						<div class="color color-tertiary"></div>

					</div>

				</div>

			</div>

		</section>

		<section class="row row--section guide">

		  <div class="block-body wrap">

		    <div class="grid grid--50">

		      <div class="block block--styles">

						<h1>Heading h1</h1>
						<h2>Heading h2</h2>
						<h3>Heading h3</h3>
						<h4>Heading h4</h4>
						<p class="paragraph">This is a paragraph which often consists of multiple lines of characters which form words and sentences to tell your story. Each paragraph ends with a bottom margin to indicate the next paragraph.</p>

		      </div>

					<div class="block block--styles">

						<p>
							<?php
							// uppercase
							$glyphs  = '<span class="glyphs">A</span>';
							$glyphs .= '<span class="glyphs">B</span>';
							$glyphs .= '<span class="glyphs">C</span>';
							$glyphs .= '<span class="glyphs">D</span>';
							$glyphs .= '<span class="glyphs">E</span>';
							$glyphs .= '<span class="glyphs">F</span>';
							$glyphs .= '<span class="glyphs">G</span>';
							$glyphs .= '<span class="glyphs">H</span>';
							$glyphs .= '<span class="glyphs">I</span>';
							$glyphs .= '<span class="glyphs">J</span>';
							$glyphs .= '<span class="glyphs">K</span>';
							$glyphs .= '<span class="glyphs">L</span>';
							$glyphs .= '<span class="glyphs">M</span>';
							$glyphs .= '<span class="glyphs">N</span>';
							$glyphs .= '<span class="glyphs">O</span>';
							$glyphs .= '<span class="glyphs">P</span>';
							$glyphs .= '<span class="glyphs">Q</span>';
							$glyphs .= '<span class="glyphs">R</span>';
							$glyphs .= '<span class="glyphs">S</span>';
							$glyphs .= '<span class="glyphs">T</span>';
							$glyphs .= '<span class="glyphs">U</span>';
							$glyphs .= '<span class="glyphs">V</span>';
							$glyphs .= '<span class="glyphs">W</span>';
							$glyphs .= '<span class="glyphs">X</span>';
							$glyphs .= '<span class="glyphs">Y</span>';
							$glyphs .= '<span class="glyphs">Z</span>';
							// lowercase
							$glyphs .= '<span class="glyphs">a</span>';
							$glyphs .= '<span class="glyphs">&#224</span>';
							$glyphs .= '<span class="glyphs">&#225</span>';
							$glyphs .= '<span class="glyphs">b</span>';
							$glyphs .= '<span class="glyphs">c</span>';
							$glyphs .= '<span class="glyphs">d</span>';
							$glyphs .= '<span class="glyphs">e</span>';
							$glyphs .= '<span class="glyphs">&#232</span>';
							$glyphs .= '<span class="glyphs">&#233</span>';
							$glyphs .= '<span class="glyphs">&#235</span>';
							$glyphs .= '<span class="glyphs">e</span>';
							$glyphs .= '<span class="glyphs">f</span>';
							$glyphs .= '<span class="glyphs">g</span>';
							$glyphs .= '<span class="glyphs">h</span>';
							$glyphs .= '<span class="glyphs">i</span>';
							$glyphs .= '<span class="glyphs">&#239</span>';
							$glyphs .= '<span class="glyphs">j</span>';
							$glyphs .= '<span class="glyphs">k</span>';
							$glyphs .= '<span class="glyphs">l</span>';
							$glyphs .= '<span class="glyphs">m</span>';
							$glyphs .= '<span class="glyphs">n</span>';
							$glyphs .= '<span class="glyphs">o</span>';
							$glyphs .= '<span class="glyphs">&#243</span>';
							$glyphs .= '<span class="glyphs">p</span>';
							$glyphs .= '<span class="glyphs">q</span>';
							$glyphs .= '<span class="glyphs">r</span>';
							$glyphs .= '<span class="glyphs">s</span>';
							$glyphs .= '<span class="glyphs">t</span>';
							$glyphs .= '<span class="glyphs">u</span>';
							$glyphs .= '<span class="glyphs">v</span>';
							$glyphs .= '<span class="glyphs">w</span>';
							$glyphs .= '<span class="glyphs">x</span>';
							$glyphs .= '<span class="glyphs">y</span>';
							$glyphs .= '<span class="glyphs">z</span>';
							// digits
							$glyphs .= '<span class="glyphs">0</span>';
							$glyphs .= '<span class="glyphs">1</span>';
							$glyphs .= '<span class="glyphs">2</span>';
							$glyphs .= '<span class="glyphs">3</span>';
							$glyphs .= '<span class="glyphs">4</span>';
							$glyphs .= '<span class="glyphs">5</span>';
							$glyphs .= '<span class="glyphs">6</span>';
							$glyphs .= '<span class="glyphs">7</span>';
							$glyphs .= '<span class="glyphs">8</span>';
							$glyphs .= '<span class="glyphs">9</span>';
							$glyphs .= '<span class="glyphs">0</span>';
							// rest
							$glyphs .= '<span class="glyphs">&#38</span>';
							$glyphs .= '<span class="glyphs">&#39</span>';
							$glyphs .= '<span class="glyphs">&#40</span>';
							$glyphs .= '<span class="glyphs">&#41</span>';
							$glyphs .= '<span class="glyphs">&#42</span>';
							$glyphs .= '<span class="glyphs">&#43</span>';
							$glyphs .= '<span class="glyphs">&#44</span>';
							$glyphs .= '<span class="glyphs">&#45</span>';
							$glyphs .= '<span class="glyphs">&#46</span>';
							$glyphs .= '<span class="glyphs">&#47</span>';
							$glyphs .= '<span class="glyphs">&#58</span>';
							$glyphs .= '<span class="glyphs">&#59</span>';
							$glyphs .= '<span class="glyphs">&#60</span>';
							$glyphs .= '<span class="glyphs">&#61</span>';
							$glyphs .= '<span class="glyphs">&#62</span>';
							$glyphs .= '<span class="glyphs">&#63</span>';
							$glyphs .= '<span class="glyphs">&#64</span>';
							$glyphs .= '<span class="glyphs">&#94</span>';
							$glyphs .= '<span class="glyphs">&#95</span>';
							$glyphs .= '<span class="glyphs">&#128</span>';
							$glyphs .= '<span class="glyphs">&#145</span>';
							$glyphs .= '<span class="glyphs">&#146</span>';
							$glyphs .= '<span class="glyphs">&#147</span>';
							$glyphs .= '<span class="glyphs">&#148</span>';
							$glyphs .= '<span class="glyphs">&#169</span>';
							echo $glyphs;

							?>
						</p>


					</div>

		    </div>

		  </div>

		</section>


		<section class="row row--section buttons has-bg">

			<div class="block-body wrap">

				<h2 class="row__title">Main buttons</h2>

				<div class="grid nowrap">

					<div class="block">

						<a href="" class="click btn button">Default button</a>

					</div>

					<div class="block">

						<a href="" class="click btn button--w">White button</a>

					</div>

					<div class="block">

						<a href="" class="click btn button--wired">Wired button</a>

					</div>

				</div>

			</div>

		</section>

		<section class="row row--section buttons style-1">

			<div class="block-body wrap">

				<h2 class="row__title">Style-1 buttons</h2>

				<div class="grid nowrap">

					<div class="block">

						<a href="" class="click btn button">Default button</a>

					</div>

					<div class="block">

						<a href="" class="click btn button--w">White button</a>

					</div>

					<div class="block">

						<a href="" class="click btn button--wired">Wired button</a>

					</div>

				</div>

			</div>

		</section>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
