<?php
/**
 * Contact company
 *
 * @package hum-v7-core
 */

if ( have_rows( 'contact_company_grp', 'option' ) ) {

  while ( have_rows( 'contact_company_grp', 'option' ) ) {

    the_row();

    // vars
    $comp_title = get_sub_field ( 'company_title', 'option' );
    $comp_kvk = get_sub_field ( 'company_kvk', 'option' );
    $comp_btw = get_sub_field ( 'company_btw', 'option' );
    $comp_disclaimer_url = get_sub_field ( 'company_disclaim_url', 'option' );
    $comp_disclaimer_linktext = get_sub_field ( 'company_disclaim', 'option' );
    $comp_privacy_url = get_sub_field ( 'company_privacy_url', 'option' );
    $comp_privacy_linktext = get_sub_field ( 'company_privacy', 'option' );
    ?>

    <div class="block block--contact block--contact__company">

      <?php
      if ( $comp_title ) { echo '<h3 class="block__title block__title--big">'.$comp_title.'</h3>'; } ?>

      <ul class="block__list company">

        <?php
        if ( $comp_kvk ) { echo '<li class="company__kvk"><span class="contact__label">KvK: </span>'.$comp_kvk.'</li>'; }
        if ( $comp_btw ) { echo '<li class="company__btw"><span class="contact__label">BTW: </span>'.$comp_btw.'</li>'; }
        if ( $comp_disclaimer_url ) { echo '<li class="company__registrars"><a href="'.$comp_disclaimer_url.'" target="_blank">'.$comp_disclaimer_linktext.'</a></li>'; }
        if ( $comp_privacy_url ) { echo '<li class="company__registrars"><a href="'.$comp_privacy_url.'" target="_blank">'.$comp_privacy_linktext.'</a></li>'; }
        ?>

      </ul>

    </div>
    <?php
  }
}
