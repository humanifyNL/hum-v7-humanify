<?php
/**
 * Template part for standard page-header
 *
 * @package hum-v7-core
 */
?>

<header class="page-header">

  <?php
  // post image
  if ( has_post_thumbnail() ) {
    echo '<div class="page-featured-image">'; the_post_thumbnail( 'featured' ); echo '</div>';
  }

  // breadcrumbs
  get_template_part( 'template-parts/site/yoast', 'breadcrumbs' );

  // title
  the_title( '<h1 class="page-title wrap">', '</h1>' );
  echo '<link href="'; the_permalink(); echo'"/>';
  ?>

</header>
