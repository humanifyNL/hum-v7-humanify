<?php
/**
 * Template part for post-header without title
 *
 * @package hum-v7-core
 */
?>

<header class="page-header">

  <?php
  // post image
  if ( has_post_thumbnail() ) {
    echo '<div class="page-featured-image">'; the_post_thumbnail( 'featured' ); echo '</div>';
  }

  get_template_part( 'template-parts/site/yoast', 'breadcrumbs' );
  ?>

</header>
