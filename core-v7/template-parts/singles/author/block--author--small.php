<?php
/**
 * Template part for displaying Author bio
 *
 * @package hum-v7-core
 */
?>

<div class="block block--author">

	<div class="author__avatar">

		<?php
		/* Filter the author bio avatar size.
		* @param int $size The avatar height and width size in pixels.
		*/
		$author_bio_avatar_size = apply_filters( 'hum_base_author_bio_avatar_size', 48 );
		echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
		?>

	</div>

	<div class="block__body">

		<?php
		echo '<h3 class="block__title">';

  		hum_meta_author();

		echo '</h3>';
		?>

	</div>

</div>
