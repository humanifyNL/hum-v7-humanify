<?php
/**
 * Query posts by category_id
 *
 * @package hum-v7-core
 */

// get var from acf/queries/row--posts__cat
if ( $cat_id ) {

  $args = array(
    'post_type' => array( 'post' ),
    'orderby' => 'date',
    'order' => 'ASC',
    'tax_query' => array(
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        'terms' => array( $cat_id ),
        'include_children' => true,
        'operator' => 'IN',
      )
    ),
  );

  $query_posts = new WP_query ( $args );

  if ( $query_posts->have_posts() ) {

    ?>
    <div class="grid--previews <?php echo hum_grid_preview();?>">

      <?php
      while ( $query_posts->have_posts() ) {

        $query_posts->the_post();
        include( locate_template( 'template-parts/singles/post/preview-post__rel.php' ) );

      }
      wp_reset_postdata();

    }
}
