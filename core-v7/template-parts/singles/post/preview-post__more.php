<?php
/**
 * Post preview with -more- tag
 *
 * @package hum-v7-core
 */

?>
<article id="post-<?php the_id();?>" class="clickable preview preview--post <?php if ( !empty($enable_iso) ) { hum_iso_class('category'); }?>">

  <?php
  if ( !is_front_page() && !is_home() ) {

    // thumbnail
    $thumb = get_the_post_thumbnail( $post->ID,'medium' );

    echo '<div class="block__thumb">';
      if ( !$thumb ) { echo hum_default_img();
      } else { echo $thumb; }
    echo '</div>';
  }
  ?>

  <div class="block__body">

    <?php
    // meta
    if( !is_archive() ) {

      echo '<div class="block__meta">';
        hum_meta_cp('post_tag');
        hum_meta_published();
      echo '</div>';
    }

    echo '<h3 class="block__title">'; the_title(); echo '</h3>';

    echo '<div class="block__text is-excerpt">';
      hum_post_more();
    echo '</div>';
    ?>

  </div>

</article>
