<?php
/**
 * Post preview as calendar item
 *
 * @package hum-v7-core
 */
?>

<article id="post-<?php the_id();?>" class="clickable preview preview--post--calendar">

  <?php
  $date_start = get_the_date();

  if( !empty($date_start) ) {
    $date_start_day = date_i18n("j", strtotime($date_start));
    $date_start_month = date_i18n("M", strtotime($date_start));

    echo '<div class="date--square">';
      echo '<div class="date__day">'.$date_start_day.'</div>';
      echo '<div class="date__month">'.$date_start_month.'</div>';
    echo '</div>';
  }

  echo '<a class="click block__link" href="'; the_permalink(); echo '">';

    echo '<h3 class="block__title">'; the_title(); echo '</h3>';

  echo '</a>';
  ?>

</article>
