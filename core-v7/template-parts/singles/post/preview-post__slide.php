<?php
/**
 * Template part used for displaying post frames in loop
 *
 * @package hum-base-v6
 */

?>
<article id="post-<?php the_id();?>" class="clickable preview preview--post preview--post--slide <?php if ( !empty($enable_iso) ) { hum_iso_class('category'); }?>">


  <?php
  // thumbnail
  $thumb = get_the_post_thumbnail( $post->ID,'medium' );
  ?>

  <div class="block__thumb">

    <?php
    if ( !$thumb ) { echo hum_default_img();
    } else { echo $thumb; }
    ?>

    <div class="block__body inside">

      <h3 class="block__title block__title"><?php the_title();?></h3>
      <?php
      // meta
      if( !is_archive() ) {

        echo '<div class="block__meta">';
        hum_meta_cp('post_tag','button--w');
        hum_meta_published();
        echo '</div>';
      }

      hum_excerpt( 'block__text is-excerpt' );

      $link_title = get_field( 'post_links_title' , 'option');

      echo '<div class="block__footer">';
      echo '<a href="'; the_permalink(); echo '" class="'. hum_button_class( 'post' ) .'">'.$link_title.'</a>';
      echo '</div>';
      ?>

    </div>

  </div>

</article>
