<?php
/**
 * Flex content - page
 *
 * ACF field: group_5f15ae26b85ae
 *
 * @package hum-v7-core
 */

if ( have_rows( 'flex_content_home' )) {

  while ( have_rows( 'flex_content_home' )) {

    the_row();

    // layouts
    if ( get_row_layout() == 'form' ) {

      get_template_part( 'template-parts/acf/rows/row--form' );
    }

    if ( get_row_layout() == 'text_form' ) {

      get_template_part( 'template-parts/acf/rows/row--text-form' );
    }

    if ( get_row_layout() == 'text' ) {

      get_template_part( 'template-parts/acf/rows/row--text' );
    }

    if ( get_row_layout() == 'text_text' ) {

      get_template_part( 'template-parts/acf/rows/row--text-text-wysi' );
    }

    if ( get_row_layout() == 'text_text_text' ) {

      get_template_part( 'template-parts/acf/rows/row--text-text-text' );
    }

    if ( get_row_layout() == 'text_image' ) {

      get_template_part( 'template-parts/acf/rows/row--text-image' );
    }

    if ( get_row_layout() == 'text_image_slider' ) {

      get_template_part( 'template-parts/acf/rows/row--text-slider' );
    }

    if ( get_row_layout() == 'text_gallery' ) {

      get_template_part( 'template-parts/acf/rows/row--text-gallery' );
    }

    if ( get_row_layout() == 'link_pages' ) {

      get_template_part( 'template-parts/acf/rows/row--pagelinks' );
    }

    if ( get_row_layout() == 'link_posts' ) {

      get_template_part( 'template-parts/acf/rows/row--postlinks' );
    }

    if ( get_row_layout() == 'landing_pages' ) {

      get_template_part( 'template-parts/acf/queries/row--landing' );
    }

    if ( get_row_layout() == 'collapse' ) {

      get_template_part( 'template-parts/acf/rows/row--collapse' );
    }

    if ( get_row_layout() == 'lastpost_image' ) {

      get_template_part( 'template-parts/acf/queries/row--posts__lastpost' );
    }
  }

}
