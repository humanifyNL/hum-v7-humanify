<?php
/**
  * Gallery row
  *
  * ACF field: group_5f12cd6d417bc
  *
  * @package hum-v7-core
  */

$images = get_sub_field( 'gallery' );

if ( $images ) {

  ?>
  <section class="row row--gallery <?php echo hum_row_style(); ?>">

    <div class="section-body wrap">

      <?php
      include( locate_template( 'template-parts/acf/partials/title__row.php' ) );
      include( locate_template( 'template-parts/acf/partials/text__row.php' ) );
      ?>

      <div class="grid">

        <div class="block block--gallery">

          <?php
          include( locate_template( 'template-parts/acf/partials/gallery.php' ) );
          ?>

        </div>

      </div>

    </div>

  </section>
  <?php

}
