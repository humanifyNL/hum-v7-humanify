<?php
/**
 * section with 3 text blocks
 *
 * ACF field: group_60019a19b6ea1
 *
 * @package hum-v7-core
 */

$center_align = get_sub_field( 'layout_text_center' );
?>

<section class="row row--section text_text_text <?php echo hum_row_style();?> ">

  <div class="section-body wrap">

    <div class="grid grid--33">

      <?php
      if ( have_rows( 'text_group_1' ) ) {
        while ( have_rows( 'text_group_1' ) ) {

          the_row();

          echo '<div class="block block--text'; if ( $center_align ) { echo ' center'; } echo '">';

            include( locate_template( 'template-parts/acf/partials/image__icon.php') );
            include( locate_template( 'template-parts/acf/partials/title.php') );
            include( locate_template( 'template-parts/acf/partials/text.php') );

          echo '</div>';

        }
      }

      if ( have_rows( 'text_group_2' ) ) {
        while ( have_rows( 'text_group_2' ) ) {

          the_row();

          echo '<div class="block block--text'; if ( $center_align ) { echo ' center'; } echo '">';

            include( locate_template( 'template-parts/acf/partials/image__icon.php') );
            include( locate_template( 'template-parts/acf/partials/title.php') );
            include( locate_template( 'template-parts/acf/partials/text.php') );

          echo '</div>';

        }
      }

      if ( have_rows( 'text_group_3' ) ) {
        while ( have_rows( 'text_group_3' ) ) {

          the_row();

          echo '<div class="block block--text'; if ( $center_align ) { echo ' center'; } echo '">';

            include( locate_template( 'template-parts/acf/partials/image__icon.php') );
            include( locate_template( 'template-parts/acf/partials/title.php') );
            include( locate_template( 'template-parts/acf/partials/text.php') );

          echo '</div>';

        }
      }
      ?>

    </div>

  </div>

</section>
