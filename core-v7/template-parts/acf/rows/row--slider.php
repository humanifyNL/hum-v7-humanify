<?php
/**
 * Slider with swiper.js
 *
 * ACF field: group_5f12ccb13d4ea
 *
 * @package hum-v7-core
 */
?>

<section class="row row--slider <?php echo hum_row_style(); ?>">

  <div class="section-body wrap">

    <?php
    include( locate_template( 'template-parts/acf/partials/title__row.php' ) );
    ?>

    <div class="grid">

      <div class="block block--slider">

        <?php
        include( locate_template( 'template-parts/acf/partials/slider.php' );
        ?>

      </div>

    </div>

  </div>

</section>
