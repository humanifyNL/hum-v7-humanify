<?php
/**
 * Text with form
 *
 * ACF field: group_5fe498bc1f515
 *
 * @package hum-v7-core
 */

$glue = get_sub_field( 'text_section_connect' );
?>

<section class="row row--section text_form <?php if( $glue ){ echo ' connect '; } echo hum_row_style();?>">

  <div class="section-body wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <div class="block block--text">

        <?php
        include( locate_template( 'template-parts/acf/partials/text__wysi.php') );

        include( locate_template( 'template-parts/acf/partials/link__repeater.php') );
        ?>

      </div>

      <div class="block block--form">

          <?php
          include( locate_template( 'template-parts/acf/partials/title__row.php' ) );
          include( locate_template( 'template-parts/acf/partials/text__row.php' ) );

          include( locate_template( 'template-parts/acf/partials/form.php' ) );
          ?>

      </div>

    </div>

  </div>

</section>
