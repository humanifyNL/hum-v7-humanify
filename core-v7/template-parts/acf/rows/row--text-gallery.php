<?php
/**
 * Text section with image
 *
 * ACF field: group_5f0b203b4a4af
 *
 * @package hum-v7-core
 */

$glue = get_sub_field( 'text_section_connect' );
?>

<section class="row row--section text_gallery <?php echo hum_row_style(); if( $glue ){ echo ' connect '; }?>">

  <div class="section-body wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <div class="block block--text">

        <?php
        include( locate_template( 'template-parts/acf/partials/text__wysi.php') );

        include( locate_template( 'template-parts/acf/partials/link__repeater.php') );
        ?>

      </div>


      <div class="block block--gallery">

        <?php
        include( locate_template( 'template-parts/acf/partials/gallery.php' ) );
        ?>

      </div>

    </div>

  </div>

</section>
