<?php
/**
 * Link partial
 *
 * @package hum-v7-core
 */

$link_rep = get_sub_field( 'link_repeater' );

if ( have_rows( 'link_repeater' ) ) {

  $count = count( $link_rep );

  echo '<div class="button-group'; if ( $count ) { echo ' n-'.$count; } echo '">';

    while ( have_rows( 'link_repeater' ) ) {

      the_row();

      include( locate_template( 'template-parts/acf/partials/link.php') );

    }

  echo '</div>';

}
