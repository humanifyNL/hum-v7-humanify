<?php
/**
 * Text partial
 *
 * @package hum-v7-core
 */

$text = get_sub_field( 'text' );

if ( $text ) {

  echo '<div class="block__text">';

    echo $text;

  echo '</div>';

}
