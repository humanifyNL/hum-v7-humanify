<?php
/**
 * Flex content - post
 *
 * ACF field: group_5f1451d459925
 *
 * @package hum-v7-core
 */

if ( have_rows( 'flex_content_post' )) {

  while ( have_rows( 'flex_content_post' )) {

    the_row();

    // layouts
    if ( get_row_layout() == 'image_gallery' ) {

      get_template_part( 'template-parts/acf/rows/row--gallery' );
    }

    if ( get_row_layout() == 'text' ) {

      get_template_part( 'template-parts/acf/rows/row--text' );
    }

    if ( get_row_layout() == 'text_image' ) {

      get_template_part( 'template-parts/acf/rows/row--text-image' );
    }

    if ( get_row_layout() == 'text_image_slider' ) {

      get_template_part( 'template-parts/acf/rows/row--text-slider' );
    }

    if ( get_row_layout() == 'text_gallery' ) {

      get_template_part( 'template-parts/acf/rows/row--text-gallery' );
    }

  }

}
