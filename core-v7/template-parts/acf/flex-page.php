<?php
/**
 * Flex content - page
 *
 * ACF field: group_5f06ea7d13be4
 *
 * @package hum-v7-core
 */

if ( have_rows( 'flex_content_page' )) {

  while ( have_rows( 'flex_content_page' )) {

    the_row();

    // layouts
    if ( get_row_layout() == 'image_gallery' ) {

      get_template_part( 'template-parts/acf/rows/row--gallery' );
    }

    if ( get_row_layout() == 'form' ) {

      get_template_part( 'template-parts/acf/rows/row--form' );
    }

    if ( get_row_layout() == 'text' ) {

      get_template_part( 'template-parts/acf/rows/row--text-wysi' );
    }

    if ( get_row_layout() == 'text_text' ) {

      get_template_part( 'template-parts/acf/rows/row--text-text-wysi' );
    }

    if ( get_row_layout() == 'text_image' ) {

      get_template_part( 'template-parts/acf/rows/row--text-image' );
    }

    if ( get_row_layout() == 'collapse' ) {

      get_template_part( 'template-parts/acf/rows/row--collapse' );
    }

    if ( get_row_layout() == 'text_image_slider' ) {

      get_template_part( 'template-parts/acf/rows/row--text-slider' );
    }

    if ( get_row_layout() == 'text_gallery' ) {

      get_template_part( 'template-parts/acf/rows/row--text-gallery' );
    }

    if ( get_row_layout() == 'link_pages' ) {

      get_template_part( 'template-parts/acf/rows/row--pagelinks' );
    }
  }

}
