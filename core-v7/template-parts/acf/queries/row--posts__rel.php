<?php
/**
 * Related posts query
 *
 * @package hum-v7-core
 */

?>
<section class="row row--previews related">

  <div class="wrap block-body">

    <?php
    $post_sib_title = get_field( 'related_posts_title', 'option');
    if ( $post_sib_title ) { echo '<h2 class="row__title">'.$post_sib_title.'</h2>'; }

    include( locate_template( 'template-parts/singles/post/query-posts__rel.php' ) );
    ?>

  </div>

</section>
