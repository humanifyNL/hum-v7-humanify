<?php
/**
 * Query row - posts (with isotope)
 *
 * @package hum-v7-core
 */

?>
<section class="row row--previews iso">

  <div class="block-body wrap">

    <?php
    include( locate_template( 'template-parts/singles/post/query-posts__iso.php' ) );
    ?>

  </div>

</section>
