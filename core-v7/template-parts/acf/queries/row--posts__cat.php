<?php
/**
 * Query row - posts
 *
 * @package hum-v7-core
 */

$main_id = get_the_id();
$cat_id = get_sub_field( 'page_category_id', $main_id);

if ( $cat_id ) {

  ?>
  <section class="row row--previews <?php echo hum_row_style();?>">

    <div class="block-body wrap">

      <?php
      include( locate_template( 'template-parts/acf/partials/title__row.php' ) );

      include( locate_template( 'template-parts/singles/post/query-posts__cat.php' ) );
      ?>

    </div>

  </section>
  <?php
}
