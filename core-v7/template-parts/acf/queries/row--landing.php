<?php
/**
 * Query row - landing pages
 *
 * @package hum-v7-core
 */

?>
<section class="row row--landing <?php echo hum_row_style(); ?>">

  <div class="block-body wrap">

    <?php
    include( locate_template( 'template-parts/acf/partials/title__row.php' ) );

    include( locate_template( 'template-parts/pages/landing/query-landing.php' ) );
    ?>

  </div>

</section>
