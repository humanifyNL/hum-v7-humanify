<?php
/**
 * Query row - posts
 *
 * @package hum-v7-core
 */

?>
<section class="row row--previews">

  <div class="block-body wrap">

    <?php
    include( locate_template( 'template-parts/singles/post/query-posts.php' ) );
    ?>

  </div>

</section>
