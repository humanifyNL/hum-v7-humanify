<?php
/**
 * Query row - page children - custom parents
 *
 * @package hum-v7-core
 */

?>
<section class="row row--pages children custom">

	<div class="block-body wrap">

		<?php
		include( locate_template( 'template-parts/acf/partials/title__row.php' ) );

		include( locate_template( 'template-parts/pages/page/query-page-children__custom.php' ) );
		?>

	</div>

</section>
