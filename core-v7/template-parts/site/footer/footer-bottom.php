<?php
/**
 * Template part for footer bottom bar
 *
 *
 * @package hum-v7-core
 */

$enable_foot_bot = get_field( 'enable_footer_bot', 'option' );
$footer_bot_checks = get_field( 'footer_bot_checks', 'option' );

if ( have_rows( 'contact_location_rep', 'option' ) ) {

  while ( have_rows( 'contact_location_rep', 'option' ) ) {

    the_row();
    $loc_name = get_sub_field ( 'location_name', 'option' );
  }
}


if ( have_rows( 'contact_company_grp', 'option' ) ) {

  while ( have_rows( 'contact_company_grp', 'option' ) ) {

    the_row();

    $comp_privacy_url = get_sub_field ( 'company_privacy_url', 'option' );
    $comp_privacy_linktext = get_sub_field ( 'company_privacy', 'option' );
    $comp_disclaimer_url = get_sub_field ( 'company_disclaim_url', 'option' );
    $comp_disclaimer_linktext = get_sub_field ( 'company_disclaim', 'option' );
  }
}


if ( $enable_foot_bot ) {

  echo '<div class="footer footer--bottom site-info">';

    echo '<div class="wrap wrap--footer">';

      echo '<div class="grid">';

        echo '<div class="block block--footer--bot">';

          echo '<ul class="list--hor">';

            if ( $footer_bot_checks && in_array( 'copy', $footer_bot_checks ) ) {
              echo '<li>Copyright &copy; '.$loc_name.'</li>';
            }

            if ( $footer_bot_checks && in_array( 'privacy', $footer_bot_checks ) ) {
              echo '<li><a class="link privacy" href="'.$comp_privacy_url.'">'.$comp_privacy_linktext.'</a></li>';
            }

            if ( $footer_bot_checks && in_array( 'disclaimer', $footer_bot_checks ) ) {
              echo '<li><a class="link privacy" href="'.$comp_disclaimer_url.'">'.$comp_disclaimer_linktext.'</a></li>';
            }

            echo '<li>Website: <a class="link" href="https://humanify.nl">Humanify</a></li>';

          echo '</ul>';

        echo '</div>';

      echo '</div>';

    echo '</div>';

  echo '</div>';
}
