<?php
/**
 * Page content WP tinyMCE
 *
 * @package hum-v7-core
 */

if ( !empty( get_the_content() ) ) {

  ?>
  <section class="row row--content">

    <div class="wrap">

      <?php
      get_template_part( 'template-parts/site/share-dialog');
      ?>

      <div class="post-body">

        <?php
        the_content();
        ?>

      </div>

      <?php
      if ( get_post_type( get_the_ID() ) == 'post' ) {
        get_template_part( 'template-parts/site/share-dialog-bot');
      }

      get_template_part( 'template-parts/singles/post/meta', 'post__bot' );
      ?>

    </div>


  </section>
  <?php
}
