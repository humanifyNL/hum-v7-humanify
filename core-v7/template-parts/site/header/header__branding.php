<?php
/**
 * Header branding
 *
 * @package hum-v7-core
 */
?>

<div class="site-branding">

	<?php
	hum_site_logo();

	$enable_brand_text = get_field( 'enable_branding_text' , 'option' );
	if ( $enable_brand_text ) {

		hum_branding_text();
	}
	?>

</div>
