<?php
/**
 * The template for displaying comments
 * Current comments and comment form
 *
 * @package hum-v7-core
 */

if ( post_password_required() ) {
	return;
}
// If the current post is protected by a password and the visitor has not yet entered the password,
// we will return early without loading the comments.

?>
<div class="wrap">

	<section class="row row--comments">

		<div class="comments">

			<?php
			comment_form();

			if ( have_comments() ) {

				?>
				<header class="comments__header">

					<h2 class="comments__title">

						<?php
						$get_comment_count = get_comments_number();

						if ( '1' === $get_comment_count ) {
							printf(
								/* translators: 1: title. */
								/* esc_html__( 'One thought on &ldquo;%1$s&rdquo;', 'hum-base' ), */
								esc_html__( 'Comments', 'hum-base' ),
								'<span>' . get_the_title() . '</span>'
							);
						}	else {
							printf( // WPCS: XSS OK.
								/* translators: 1: comment count number, 2: title. */
								/* esc_html( _nx( '%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $hum_comment_count, 'comments title', 'hum-base' ) ),*/
								esc_html( _nx( 'Comments (%1$s)', 'Comments (%1$s)', $get_comment_count, 'comments title', 'hum-base' ) ),
								number_format_i18n( $get_comment_count ),
								'<span>' . get_the_title() . '</span>'
							);
						}
						?>

					</h2>

				</header>

				<?php
				the_comments_navigation();

				echo '<ol class="comments__list">';

				wp_list_comments( array(
					'callback' => 'hum_comment',
					'style'    => 'ol',
				) );

				echo '</ol>';

				the_comments_navigation();
				// If comments are closed and there are comments, let's leave a little note, shall we?
				if ( ! comments_open() ) {

					?>
					<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'hum-base' ); ?></p>
					<?php
				}

			} ?>
		</div>

	</section>

</div>
