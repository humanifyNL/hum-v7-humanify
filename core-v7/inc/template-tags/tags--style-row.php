<?php
/**
 * Row theme layout
 *
 * ACF field: group_5f76e008a957f
 *
 * @package hum-v7-core
 */

if ( !function_exists( 'hum_row_style' ) ) {

  function hum_row_style() {

    $theme_select = get_sub_field( 'row_theme' );

    if ( isset($theme_select) ) {

      if ( $theme_select !== 'default' ) {

        $theme = $theme_select;

      } else {

        return;

      }

      return $theme;
    }
  }
}

/* ACF populate select field
 *
 * https://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 * fieldname = row_theme
 */

function acf_load_row_theme_field_choices( $field ) {

    // reset choices
    $field['choices'] = array(
      'default' => 'Default',
      'has-bg' => 'White background',
      'style-1' => 'Primary background',
      'style-2' => 'Secondary background',
    );

    // return the field
    return $field;

}

add_filter('acf/load_field/name=row_theme', 'acf_load_row_theme_field_choices');
