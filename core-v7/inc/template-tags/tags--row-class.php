<?php
/**
 * Hum Base determine row location and print it in row class
 *
 * @package hum-v7-core
 */

if ( !function_exists( 'hum_row_loc') ) {

  function hum_row_loc() {

    $classes = array();

    // homepage
    if ( !is_home() && is_front_page() ) {

      $classes[] = ' home';
    }

    if ( is_page_template( 'page-templates/about.php' ) ) {

      $classes[] = ' about';
    }

    if ( is_page_template( 'page-templates/contact.php' ) ) {

      $classes[] = ' contact';
    }

    if ( is_page_template( 'page-templates/landing.php' ) ) {

      $classes[] = ' landing';
    }

    if ( is_page_template( 'page-templates/linkpage.php' ) ) {

      $classes[] = ' linkpage';
    }

    if ( !empty($classes) && !is_wp_error($classes) ) {

      $row_classes = join( " ", $classes );
      echo $row_classes;

    }
  }
}
