<?php
/**
 * Branding functions and setup
 *
 * @package hum-v7-core
 */

 /**
 * Displays the optional custom logo. Does nothing if custom logo is not available.
 *
 */
if ( ! function_exists( 'hum_site_logo' ) ) {

  function hum_site_logo() {

    if ( function_exists( 'the_custom_logo' ) ) {
      the_custom_logo();
    } else {
      return;
    }
  }
}


/**
 * Branding text
 *
 */
if ( ! function_exists( 'hum_branding_text' ) ) {

  function hum_branding_text() {

    echo '<a href="'. esc_url( home_url( '/' ) ) .'" class="site-branding__text" rel="home"'; hum_core_semantics( 'site-url' ); echo '>';

      // site-title & description
      $hum_base_description = get_bloginfo( 'description', 'display' );

      echo '<p'; hum_core_semantics( 'site-title' ); echo '>'; bloginfo( 'name' ); echo '</p>';
      echo '<p'; hum_core_semantics( 'site-description' ); echo '>'.$hum_base_description.'</p>';

    echo '</a>';
  }
}
