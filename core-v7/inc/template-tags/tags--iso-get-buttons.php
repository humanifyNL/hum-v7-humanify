<?php
/**
 * Hum Base buttons for isotope
 * $query -> insert valid wp_query - default: $wp_query
 * $taxonomy -> button taxonomy sorting - default: category
 *
 * @package hum-v7-core
 */


if ( !function_exists( 'hum_get_query_buttons_iso' ) ) {

  function hum_get_query_buttons_iso( $query, $taxonomy ) {

    // text input
    $button_all = 'Alles';

    // prep array for term_ids
    $queried_term_ids = array();

    // get list of post id's in query
    $queried_post_ids = wp_list_pluck( $query->posts, 'ID' );

    if ( $queried_post_ids && !is_wp_error( $queried_post_ids ) ) {

      foreach ( $queried_post_ids as $post_id ) {

        // get terms for each post_id
        $queried_terms = wp_get_post_terms( $post_id, $taxonomy );

        // put term_id in array
        if ( !empty($queried_terms) ) {

          $queried_term_ids[] = $queried_terms[0]->term_id;
          // filter duplicates out of array
          $queried_term_ids = array_unique($queried_term_ids, SORT_REGULAR);
        }
      }

      // build html
      echo '<div class="button-group button-group--iso">';

        echo '<button class="btn button--alt is-checked" data-filter="*">'.$button_all.'</button>';

        // get term objects
        $post_terms = get_terms( array(
          'taxonomy' => $taxonomy,
          'hide_empty' => true,
          'include' => $queried_term_ids,
        ) );

        foreach ( $post_terms as $post_term ) {

          $term_id = $post_term->term_id;
          $term_slug = $post_term->slug;
          $term_name = $post_term->name;

          // skip uncategorized
          if ( $term_id == 1 ) {
            continue;
          }

          echo '<button class="btn button--alt" data-filter=".'. $term_slug .'">'. $term_name .'</button>';
        }

      echo '</div>';
    }
  }
}
