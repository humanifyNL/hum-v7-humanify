<?php
/**
 * Custom template tags
 * Excerpt, post-summary, more-link
 *
 * @package hum-v7-core
 */

/**
 * Display post content with "more" link when applicable
 */
if ( ! function_exists( 'hum_post_more' ) ) {

	function hum_post_more() {

		$link_title = get_field( 'post_links_title', 'option');

		the_content( sprintf(

			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( $link_title.'<span class="screen-reader-text"> "%s"</span>', 'hum-base' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );
	}
}

if ( ! function_exists( 'hum_excerpt' ) ) {
/**
 * Displays the optional excerpt.
 * Wraps the excerpt in a div element.
 *
 * Create your own hum_excerpt() function to override in a child theme's funcions.php file.
 *
 * @param string $class Optional. Class string of the div element. Defaults to 'post-summary'.
 */

  function hum_excerpt( $class = 'block__text is-excerpt' ) {
  	$class = esc_attr( $class );

  	if ( ! is_search() ) {

			echo '<div class="'.$class.'">';
				the_excerpt();
			echo '</div><!--.'.$class.'-->';

		} elseif ( is_search() ) {

			echo '<div class="'.$class.' wrap">';
				the_excerpt();
			echo '</div><!--.'.$class.'-->';

		}
  }
}

if ( ! function_exists( 'hum_excerpt_single' ) ) {
	/**
	* Displays the optional excerpt (subtitle) below the entry title.
	* Wraps the excerpt in a div element.
	*
	* Create your own hum_excerpt_top() function to override in a child theme's funcions.php file.
	* @param string $class Optional. Class string of the div element. Defaults to 'post-summary'.
	*/
	function hum_excerpt_single( $class = 'block__text is-excerpt' ) {

		$class = esc_attr( $class );

		if ( !is_home() ) {

			echo '<p class="'.$class.'">';
			// Includes a fix for better plugin compatibility: https://github.com/WordPress/twentysixteen/issues/397
			$my_excerpt = get_the_excerpt();
			echo $my_excerpt;

			echo '</p>';
		}
	}
}

// HAND EXCERPT
/*
if ( ! function_exists( 'hum_excerpt_more' ) ) {

	// Changing excerpt more - only works where excerpt IS hand-crafted
	function hum_excerpt_more( $excerpt ) {

		global $acf_products_on;
		$excerpt_more = '';
		if( has_excerpt() && !is_admin() ) {

    	$excerpt_more = '<span class="block__footer"><a class=".block__link" href="' . get_permalink() . '" rel="nofollow">Lees verder</a></span>';
	  }
	  return $excerpt . $excerpt_more;
	}
	add_filter( 'get_the_excerpt', 'hum_excerpt_more' );
}*/

// AUTO EXCERPT
/*
if ( ! function_exists( 'hum_base_new_excerpt_more' ) && ! is_admin() ) {
/**
 * Add "continue reading" link for the automatically generated Excerpts. Replaces "[...]" (appended to automatically generated excerpts).
 *
 * This does not apply to explicit excerpts for the posts that you would enter manually into Excerpt text box.
 * Please also note, that Excerpt is not a Teaser (the part of a post that appears on the front page when you use the More tag).
 *
 * Note that if you have also placed the More tag and it was placed before the minimum count of words needed for Excerpt,
 * "continue reading" link for the Excerpt will not be displayed.
 *
 * If that happens, you can use the function below to adjust the Excerpt length. By default automatic Excerpt shows
 * the first 55 words of the post's content.
 *
 * @link https://codex.wordpress.org/Template_Tags/the_excerpt
 * @return string 'Continue reading' link
 */
/*
  function hum_base_new_excerpt_more() {

		global $acf_products_on;

		if ( !is_home() && !is_front_page() && !is_page_template( 'page-templates/product-links.php' ) && $acf_products_on == false ) {

			$link = sprintf( '<span class="block__footer"><a href="%1$s" class=".block__link">%2$s</a></span>',
			esc_url( get_permalink( get_the_ID() ) ),
			/* translators: %s: post title.*/
			/*sprintf( __( 'Lees meer<span class="screen-reader-text"> "%s"</span>', 'hum-base' ), esc_html( get_the_title( get_the_ID() ) ) )
			);

			return $link;
		}
  }

add_filter( 'excerpt_more', 'hum_base_new_excerpt_more' );
}*/



/* Add class to read more link
 * https://wordpress.stackexchange.com/questions/88352/wrap-more-link-in-div
 */
function wrap_readmore($more_link) {
  return '<div class="block__footer">'.$more_link.'</div>';
}
add_filter('the_content_more_link', 'wrap_readmore', 10, 1);


function add_morelink_class( $link, $text ) {

	$link_type = 'more-link ' . hum_button_class( 'post' );


  return str_replace(
    'more-link',
    $link_type,
    $link
  );
}
add_action( 'the_content_more_link', 'add_morelink_class', 10, 2 );

/* Control custom excerpt length (not more tag)
 * @link https://codex.wordpress.org/Template_Tags/the_excerpt
 */
/*function hum_base_custom_excerpt_length( $length ) {

  return 30;

}
add_filter( 'excerpt_length', 'hum_base_custom_excerpt_length', 999 );
*/
