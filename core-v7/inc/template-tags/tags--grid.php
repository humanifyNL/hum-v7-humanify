<?php
/**
* Grid class modifier
 *
 * ACF field: group_5f0990f32e941
 *
 * @package hum-v7-core
 */


if ( !function_exists( 'hum_grid' ) ) {

  function hum_grid() {

    $grid_selected = get_sub_field( 'grid_select' );

    if ( isset($grid_selected) ) {

      switch ( $grid_selected ) {

        case 'grid-4':
          $grid = 'grid--25';
          break;
        case 'grid-3':
          $grid = 'grid--33';
          break;
        case 'grid-2':
          $grid = 'grid--50';
          break;
      }

      return $grid;
    }
  }
}

/* ACF populate select field
 *
 * https://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 * fieldname = grid_select
 */

function acf_load_grid_select_field_choices( $field ) {

    // reset choices
    $field['choices'] = array(
      'grid-4' => '4 per rij',
      'grid-3' => '3 per rij',
      'grid-2' => '2 per rij',
    );

    // return the field
    return $field;

}
add_filter('acf/load_field/name=grid_select', 'acf_load_grid_select_field_choices');
