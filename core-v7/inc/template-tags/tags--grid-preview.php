<?php
/**
 * Grid preview class modifier
 *
 * ACF field: group_5f0990f32e941
 *
 * @package hum-v7-core
 */

if ( !function_exists( 'hum_grid_preview' ) ) {

  function hum_grid_preview() {

    $use_local = get_sub_field( 'grid_preview' );

    if ( !empty($use_local) ) {

      $grid_selected = $use_local;

    } elseif ( is_page() ) {

      // not set on page - do default for pages
      $grid_selected = get_field( 'grid_preview_page', 'option');

    } elseif ( is_single() ) {

      // use rel
      $grid_selected = get_field( 'grid_preview_rel', 'option');;

    } else {

      // not set - do default
      $grid_selected = get_field( 'grid_preview_post', 'option');

    }


    if ( !empty($grid_selected) ) {

      switch ( $grid_selected ) {

        case 'grid-4':
          $grid = 'grid--25';
          break;
        case 'grid-3':
          $grid = 'grid--33';
          break;
        case 'grid-2':
          $grid = 'grid--50';
          break;
        case 'grid-1':
          $grid = 'grid--100';
          break;

        default:
          $grid = 'grid--100';
      }

      return $grid;
    }
  }
}

/* ACF populate select field
 *
 * https://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 * fieldname = grid_preview & grid_preview_rel
 */

function acf_load_grid_preview_field_choices( $field ) {

    // reset choices
    $field['choices'] = array(
      'grid-4' => '4 per rij',
      'grid-3' => '3 per rij',
      'grid-2' => '2 per rij',
      'grid-1' => '1 per rij',
    );

    // return the field
    return $field;

}

add_filter('acf/load_field/name=grid_preview', 'acf_load_grid_preview_field_choices');
add_filter('acf/load_field/name=grid_preview_post', 'acf_load_grid_preview_field_choices');
add_filter('acf/load_field/name=grid_preview_rel', 'acf_load_grid_preview_field_choices');
add_filter('acf/load_field/name=grid_preview_page', 'acf_load_grid_preview_field_choices');
