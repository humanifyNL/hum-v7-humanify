<?php
/**
 * Custom post functions
 *
 * @package hum-v7-core
 */

/**
 * Custom Post Types
 *
 */
/*
function create_post_type() {

  register_post_type( 'projects',
    array(
      'labels' => array(
        'name'              => __( 'Projects' ),
        'singular_name'     => __( 'Project' ),
  			'all_items'         => __( 'All Projects' ),
  			'view_item'         => __( 'View Project' ),
  			'add_new_item'      => __( 'New Project' ),
  			'add_new'           => __( 'New Project' ),
  			'edit_item'         => __( 'Edit Project' ),
  			'update_item'       => __( 'Update Project' ),
  			'search_item'       => __( 'Search Project' ),
      ),
      'public'              => true,
      'has_archive'   	    => true,
			'hierarchical'        => false,
			'publicly_queryable'  => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
	 	 	'show_in_nav_menus'   => true,
	 		'show_in_admin_bar'   => true,
      'menu_icon'           => 'dashicons-analytics',
			'supports'            => array('title','thumbnail','editor','hum_base_postexcerpt'),
			'taxonomies'          => array( 'post_tag','category' ),
			'rewrite'             => array('slug' => 'projects'),
		)
  );

  register_post_type( 'jobs',
    array(
      'labels' => array(
        'name'              => __( 'Job offers' ),
        'singular_name'     => __( 'Job offer' ),
        'all_items'         => __( 'All Job offers' ),
        'view_item'         => __( 'View Job offer' ),
        'add_new_item'      => __( 'New Job offer' ),
        'add_new'           => __( 'New Job offer' ),
        'edit_item'         => __( 'Edit Job offer' ),
        'update_item'       => __( 'Update Job offer' ),
        'search_item'       => __( 'Search Job offers' ),
      ),
      'public'              => true,
      'has_archive'   	    => true,
      'hierarchical'        => false,
      'publicly_queryable'  => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'menu_icon'           => 'dashicons-id',
      'supports'            => array('title','thumbnail','editor','hum_base_postexcerpt'),
      //'taxonomies'        => array( 'post_tag' ),
      'rewrite'             => array('slug' => 'job-offers'),
    )
  );

  register_post_type( 'employees',
    array(
      'labels' => array(
        'name'              => __( 'Employees' ),
        'singular_name'     => __( 'Employees' ),
        'all_items'         => __( 'All Employees' ),
        'view_item'         => __( 'View Employee' ),
        'add_new_item'      => __( 'New Employee' ),
        'add_new'           => __( 'New Employee' ),
        'edit_item'         => __( 'Edit Employee' ),
        'update_item'       => __( 'Update Employee' ),
        'search_item'       => __( 'Search Employee' ),
      ),
      'public'              => true,
      'has_archive'   	    => true,
      'hierarchical'        => false,
      'publicly_queryable'  => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'menu_icon'           => 'dashicons-admin-users',
      'supports'            => array('title','thumbnail','editor','hum_base_postexcerpt'),
      //'taxonomies'        => array( 'post_tag' ),
      'rewrite'             => array('slug' => 'employees'),
    )
  );

  register_post_type( 'clients',
    array(
      'labels' => array(
        'name'              => __( 'Clients' ),
        'singular_name'     => __( 'Client' ),
        'all_items'         => __( 'All Clients' ),
        'view_item'         => __( 'View Client' ),
        'add_new_item'      => __( 'New Client' ),
        'add_new'           => __( 'New Client' ),
        'edit_item'         => __( 'Edit Client' ),
        'update_item'       => __( 'Update Client' ),
        'search_item'       => __( 'Search Client' ),
      ),
      'public'              => true,
      'has_archive'   	    => true,
      'hierarchical'        => false,
      'publicly_queryable'  => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'menu_icon'           => 'dashicons-groups',
      'supports'            => array('title','thumbnail','editor','hum_base_postexcerpt'),
      //'taxonomies'        => array( 'post_tag' ),
      'rewrite'             => array('slug' => 'clients'),
    )
  );

}
add_action( 'init', 'create_post_type' );
*/


/**
 * Custom taxonomies
 *
 */
/*
// Add new taxonomy, NOT hierarchical (like tags)
$labels = array(
	'name'                       => _x( 'WebTags', 'taxonomy general name'),
	'singular_name'              => _x( 'WebTag', 'taxonomy singular name' ),
	'search_items'               => __( 'Search Tags' ),
	'popular_items'              => __( 'Popular Tags' ),
	'all_items'                  => __( 'All WebTags' ),
	'parent_item'                => null,
	'parent_item_colon'          => null,
	'edit_item'                  => __( 'Edit Tag' ),
	'update_item'                => __( 'Update Tag' ),
	'add_new_item'               => __( 'Add New Tag' ),
	'new_item_name'              => __( 'New Tag Name' ),
	'separate_items_with_commas' => __( 'Separate Tags with commas' ),
	'add_or_remove_items'        => __( 'Add or remove Tags' ),
	'choose_from_most_used'      => __( 'Choose from the most used Tags'),
	'not_found'                  => __( 'No Tags found.' ),
	'menu_name'                  => __( 'WebTags' ),
);

$args = array(
	'hierarchical'          => false,
	'labels'                => $labels,
	'show_ui'               => true,
	'show_admin_column'     => true,
	'update_count_callback' => '_update_post_term_count',
	'query_var'             => true,
	'rewrite'               => array( 'slug' => 'tag-web' ),
);

register_taxonomy( 'tag-web', array('post'), $args );
*/



/**
 * Add custom post to Cat archives.
 *
 */
/*
function hum_category_set_post_types( $query ){

  if( $query->is_category() && $query->is_main_query() ){
    $query->set( 'post_type', array( 'post_web' ) );
  }
}
add_action( 'pre_get_posts', 'hum_category_set_post_types' );
*/
