<?php
/**
 * Filter functions.
 *
 * This file holds functions that are used for filtering.
 * https://github.com/samikeijonen/bemit/tree/master/inc
 *
 * @package hum-v7-core
 */

/**
 * Filters the CSS classes applied to a menu item's list item element.
 *
 * @param string[] $classes Array of the CSS classes that are applied to the menu item's `<li>` element.
 * @param WP_Post  $item    The current menu item.
 * @param stdClass $args    An object of wp_nav_menu() arguments.
 * @param int      $depth   Depth of menu item. Used for padding.
 */
function hum_base_nav_menu_css_class( $classes, $item, $args, $depth ) {

	// Get theme location, fallback for `default`.
	$theme_location = $args->theme_location ? $args->theme_location : 'default';

	// Reset all default classes and start adding custom classes to array.
	$_classes = [ 'menu__item' ];
	// Add theme location class.
	$_classes[] = 'menu__item--' . $theme_location;

	// Add a class if the menu item has children.
	if ( in_array( 'menu-item-has-children', $classes, true ) ) {
		$_classes[] = 'has-children';
	}
	// Add `is-top-level` example class using $depth parameter.
	if ( 0 === $depth ) {
		$_classes[] = 'is-top-level';
	}

	// Return custom classes.
	return $_classes;
}
add_filter( 'nav_menu_css_class', 'hum_base_nav_menu_css_class', 10, 4 );


/**
 * Filters the WP nav menu link attributes.
 *
 * @param array    $atts {
 *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
 *
 *     @type string $title  Title attribute.
 *     @type string $target Target attribute.
 *     @type string $rel    The rel attribute.
 *     @type string $href   The href attribute.
 * }
 * @param WP_Post  $item  The current menu item.
 * @param stdClass $args  An object of wp_nav_menu() arguments.
 * @param int      $depth Depth of menu item. Used for padding.
 * @return string  $attr
 */
function hum_base_nav_menu_link_attributes( $atts, $item, $args, $depth ) {

	// Get theme location, fallback for `default`.
	$theme_location = $args->theme_location ? $args->theme_location : 'default';

	// Start adding custom classes.
	$atts['class'] = 'menu__anchor menu__anchor--' . $theme_location;
	// Add `menu__anchor--button` when there is `button` class in `<li>` element.
	if ( in_array( 'button', $item->classes, true ) && 'primary' === $args->theme_location ) {
		$atts['class'] .= ' menu__anchor--button';
	}
	// Add `is-page-ancestor` class for current page ancestor (main)
	if ( in_array( 'current-page-ancestor', $item->classes, true ) ) {
		$atts['class'] .= ' is-page-ancestor';
	}
  // Add `is-menu-ancestor` class for current menu ancestors
  if ( in_array( 'current-menu-ancestor', $item->classes, true ) ) {
    $atts['class'] .= ' is-menu-ancestor';
  }
	// Add `is-active` class.
	if ( in_array( 'current-menu-item', $item->classes, true ) ) {
		$atts['class'] .= ' is-active';
	}
	// Add `is-top-level` example class using $depth parameter.
	if ( 0 === $depth ) {
		$atts['class'] .= ' is-top-level';
	}
	if ( in_array( 'yesmob', $item->classes, true ) && 'primary' === $args->theme_location ) {
		$atts['class'] .= ' yesmob';
	}
	if ( in_array( 'nomob', $item->classes, true ) && 'primary' === $args->theme_location ) {
		$atts['class'] .= ' nomob';
	}

	// Return custom classes.
	return $atts;
}
add_filter( 'nav_menu_link_attributes', 'hum_base_nav_menu_link_attributes', 10, 4 );


/**
 * Filters the list of CSS classes to include with each page item in the list.
 *
 * @see wp_list_pages()
 *
 * @param string[] $css_class    An array of CSS classes to be applied to each list item.
 * @param WP_Post  $page         Page data object.
 * @param int      $depth        Depth of page, used for padding.
 * @param array    $args         An array of arguments.
 * @param int      $current_page ID of the current page.
 */
function hum_base_page_css_class( $css_class, $page, $depth, $args, $current_page ) {

	$css_class = [ 'menu__item menu__item--sub-pages' ];

	if ( in_array( 'page_item_has_children', $css_class, true ) ) {
		$css_class[] = 'has-children';
	}
	if ( in_array( 'current_page_ancestor', $css_class, true ) ) {
		$css_class[] = 'menu__item--ancestor';
	}
	if ( 0 === $depth ) {
		$css_class[] = 'is-top-level';
	}

	return $css_class;

}
add_filter( 'page_css_class', 'hum_base_page_css_class', 10, 5 );


/**
 * Filters the HTML attributes applied to a page menu item's anchor element.
 *
 * @param array   $atts {
 *       The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
 *
 *     @type string $href The href attribute.
 * }
 * @param WP_Post $page         Page data object.
 * @param int     $depth        Depth of page, used for padding.
 * @param array   $args         An array of arguments.
 * @param int     $current_page ID of the current page.
 */
function hum_base_page_menu_link_attributes( $atts, $page, $depth, $args, $current_page ) {

	$atts['class'] = 'menu__anchor menu__anchor--sub-pages';

	if ( $current_page === $page->ID ) {
		$atts['class'] .= ' is-active';
	}
	if ( $args['has_children'] ) {
		$atts['class'] .= ' has-children';
	}
	if ( 0 === $depth ) {
		$atts['class'] .= ' is-top-level';
	}

	return $atts;
}
add_filter( 'page_menu_link_attributes', 'hum_base_page_menu_link_attributes', 10, 5 );


/**
 * Adds a custom class to the submenus in nav menus.
 *
 * @param string[] $classes Array of the CSS classes that are applied to the menu `<ul>` element.
 * @param stdClass $args    An object of `wp_nav_menu()` arguments.
 * @param int      $depth   Depth of menu item. Used for padding.
 */
function hum_base_nav_menu_submenu_css_class( $classes, $args, $depth ) {

	$classes = [ 'menu menu__sub-menu' ];
	return $classes;
}
add_filter( 'nav_menu_submenu_css_class', 'hum_base_nav_menu_submenu_css_class', 10, 3 );
