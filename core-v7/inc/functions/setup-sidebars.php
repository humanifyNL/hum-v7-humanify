<?php
/**
 * Hum Base sidebar registration
 *
 * @package hum-v7-core
 */

 /**
 * Register our main widget area, footer widget areas and the front page widget areas.
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

 function hum_core_widgets_init() {

 	register_sidebar( array(
 		'name'          => esc_html__( 'Sidebar', 'hum-base' ),
 		'id'            => 'sidebar-main',
 		'description'   => esc_html__( 'Wordt weergegeven op pagina&#39;s met sidebar template. Ook op: archief, zoeken, auteur, en 404 pagina&#39;s.' , 'hum-base' ),
 		'before_widget' => '<section id="%1$s" class="widget widget--sidebar %2$s">',
 		'after_widget'  => '</section>',
 		'before_title'  => '<h3 class="widget__title">',
 		'after_title'   => '</h3>',
 	) );

  /*
 	register_sidebar( array(
 		'name'          => esc_html__( 'Sidebar Static', 'hum-base' ),
 		'id'            => 'sidebar-static',
 		'description'   => esc_html__( 'Verschijnt op blog en berichten. Sticky positie' , 'hum-base' ),
 		'before_widget' => '<section id="%1$s" class="widget widget--sidebar %2$s">',
 		'after_widget'  => '</section>',
 		'before_title'  => '<h3 class="widget__title">',
 		'after_title'   => '</h3>',
 	) );
  */
 }
 add_action( 'widgets_init', 'hum_core_widgets_init' );
