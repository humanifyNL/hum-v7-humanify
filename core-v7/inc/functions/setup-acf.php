<?php
/**
 * ACF setup functions
 *
 * @package hum-v7-core
 */


/**
 * add 'row order' to ACF fields overview
 *
 */
function acf_field_group_columns($columns) {

  $columns['menu_order'] = __('Order');

  return $columns;
}

add_filter('manage_edit-acf-field-group_columns', 'acf_field_group_columns', 20);


function acf_field_group_columns_content($column, $post_id) {

  switch ($column) {

    case 'menu_order':
      global $post;
      echo '<strong>',$post->menu_order,'</strong>';
    break;
    default:
    break;
  } // end switch
}

add_action('manage_acf-field-group_posts_custom_column', 'acf_field_group_columns_content', 20, 2);



/**
 * acf filter for detecting post types and putting it in select field
 *
 */
function acf_load_posttype_field_choices( $field ) {

  // reset choices
  $field['choices'] = array();

	$post_types = get_post_types();

	if ( !empty( $post_types ) ) {

		if ( $post_types['post'] ) {

			$value = $post_types['post'];
			$label = ucfirst($post_types['post']);

			// append to choices
			$field['choices'][ $value ] = $label;
		}

		if ( isset($post_types['projects']) ) {

			$value = $post_types['projects'];
			$label = ucfirst($post_types['projects']);

			// append to choices
			$field['choices'][ $value ] = $label;

		}

		if ( isset($post_types['employees']) ) {

			$value = $post_types['employees'];
			$label = ucfirst($post_types['employees']);

			// append to choices
			$field['choices'][ $value ] = $label;

		}

		if ( isset($post_types['clients']) ) {

			$value = $post_types['clients'];
			$label = ucfirst($post_types['clients']);

			// append to choices
			$field['choices'][ $value ] = $label;

		}

		if ( isset($post_types['logos']) ) {

			$value = $post_types['logos'];
			$label = ucfirst($post_types['logos']);

			// append to choices
			$field['choices'][ $value ] = $label;

		}

		if ( isset($post_types['jobs']) ) {

			$value = $post_types['jobs'];
			$label = ucfirst($post_types['jobs']);

			// append to choices
			$field['choices'][ $value ] = $label;

		}

		// return the field
		return $field;
	}
}

add_filter('acf/load_field/name=pagelinks_post_type', 'acf_load_posttype_field_choices');
