<?php
/**
 * The template for footer
 * Contains the closing of the #content div and all content after inc wp_footer
 *
 * @package hum-v7-core
 */
?>

<footer id="colophon" class="site-footer" aria-labelledby="footer-header" itemscope="itemscope" itemtype="https://schema.org/WPFooter">

	<?php
	get_template_part( 'template-parts/site/footer/footer', 'flex' );

	get_template_part( 'template-parts/site/footer/footer', 'bottom' );

	get_template_part( 'template-parts/site/footer/footer', 'float' );
	?>

</footer>

</div><!--site-container-->
<?php

wp_footer();
?>

</body>
</html>
