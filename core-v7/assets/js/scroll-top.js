/**
 * Add class when scrolling
 *
 * @package hum-v7-core
 */

jQuery(document).ready(function($) {

  //var newLogo = $('.site-logo').attr('src');
  //var oldLogo = $('.site-logo').attr('src');

  $(window).on('load scroll', function() {
    var y = $(this).scrollTop();
    if ( y >= 40 ) {
      //$('.site-logo').attr('src',newLogo);
      $('.header--site').addClass('header--site__compact');
      $('.site-container').addClass('compact');

    } else {
      //$('.site-logo').attr('src',oldLogo);
      $('.header--site').removeClass('header--site__compact');
      $('.site-container').removeClass('compact');

    }
  });

});
