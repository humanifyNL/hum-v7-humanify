(function($){

  $('#frm-referrer').val(location.href);
  if ($('#terms_url').length ) {
    // put terms url in href
    $('#frm-accept-terms-url').attr('href', $('#terms_url').val() );
  } else {
    // remove terms checkbox
    $('.frm-accept-terms').hide();
    $('.frm-accept-terms input[name="frm-accept-terms"]').prop('checked', true);
    $('.frm-accept-terms input[name="frm-accept-terms"]').attr('checked', true);
  }

}(jQuery));
