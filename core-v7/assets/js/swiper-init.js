/**
 * Swiper init
 *
 * @package hum-v7-core
 */


jQuery(document).ready(function($) {
  //initialize swiper when document ready
  var $mySwiper = new Swiper ('.swiper-container', {
  	direction: 'horizontal',
  	loop: true,
  	effect: 'fade',
  	autoplay: {
  		delay: 5000,
  	},
  	speed: 600,

    /*
    // If we need pagination
    pagination: {
      el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
      el: '.swiper-scrollbar',
    },

    // http://idangero.us/swiper/api/

    */
  });

  var $mySwiper2 = new Swiper ('.block__img--slider', {
    direction: 'horizontal',
    loop: true,
    effect: 'flip',
    autoplay: {
      delay: 4000,
    },
    speed: 400,
  });
});
