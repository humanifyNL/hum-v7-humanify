/**
 * Add class when scrolling up
 *
 * @package hum-v7-core
 */

jQuery(document).ready(function($) {

  $(window).on('load scroll', function() {

    // when scrolled 0.8x past top
    var scrolltop = $(this).scrollTop();
    var bounds = $(window).height() * 0.75;

    // add .visible class
    if ( scrolltop >= bounds ) {
      $('.footer--float').addClass('visible');
    } else {
      $('.footer--float').removeClass('visible');
    }

    // when at the bottom, minus footer height
    var botbounds = scrolltop + $(window).height();
    var footerHeight = $('.site-footer').outerHeight();
    var bot = $(document).height() - footerHeight;

    if ( botbounds >= bot ) {
      $('.footer--float').addClass('at-bottom');
    } else {
      $('.footer--float').removeClass('at-bottom');
    }

  });

});
